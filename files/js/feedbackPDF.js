/**
 * To generate a feedback in pdf
 */
function feedbackPDF()
{

    var doc = new jsPDF();

    //logo UCA
    let imgUCA = new Image()
    imgUCA.src = 'images/logoUCA.png'
    doc.addImage(imgUCA,'png', 140, 5, 60, 8)

    //logo LINE
    let imgLINE = new Image()
    imgLINE.src = 'images/logoLINE.jpg'
    doc.addImage(imgLINE,'jpg', 10, 5, 65, 15)

    doc.setFontSize(9)
    doc.setFont('','normal')

    let xText = 15

    //address
    doc.text('Laboratoire d\'Innovation et Numérique pour l\'Education (LINE)\n' +
        'INSPE de Nice. Université Côte d\'Azur.\n' +
        '43, avenue Stephen Liégeard, 06100, Nice, France \n', xText, 30)

    //date
    let date = new Date()
    doc.text('Le ' + date.toLocaleDateString(), 170, 45)

    //object
    doc.setFont('','bold')
    doc.text('Objet :', xText, 55)
    doc.setFont('','normal')
    doc.text('Rétroactions sur l’analyse de l’activité CreaCube,', 27, 55)

    //personalized text
    let bodyText =  document.getElementById('body-text').value
    let splitText = doc.splitTextToSize(bodyText, 175);
    doc.text( splitText, xText, 70);

    let signature =  document.getElementById('signature').value
    let splitSignature = doc.splitTextToSize(signature, 175);

    //to adapt the position of elements in function of the body-text's size
    let space = splitText.length * 4.5;

    let widthChart = 210
    let heightChart = 80
    let xChart = -7
    let yChart = 65
    let spaceCharts = 100
    let spaceBetweenThanksSignatures = 10

    //add selected chart(s)
    if (document.getElementById('add-feedbackChart-true').checked && document.getElementById('add-boxPlot-true').checked)
    {
        if(space > 45 || splitSignature.length > 2)
        {
            doc.addImage(getFeedbackChartImg(),xChart, yChart + space + 5, widthChart, heightChart)

            doc.addPage();
            doc.addImage(getBoxPlotImg(), xChart, 20, widthChart, heightChart)

            spaceCharts = 110
            //thanks
            doc.text('Meilleures salutations,', xText, spaceCharts)
            //signature(s)
            doc.text( splitSignature, xText, spaceCharts + spaceBetweenThanksSignatures);

        } else {

            doc.addImage(getFeedbackChartImg(),xChart, yChart + space, widthChart, heightChart)
            doc.addImage(getBoxPlotImg(),xChart, yChart + space + 90, 180, 85)

            spaceCharts = 170
            //thanks
            doc.text('Meilleures salutations,', xText, yChart + space + spaceCharts)
            //signature(s)
            doc.text( splitSignature, xText, yChart + space + spaceCharts + spaceBetweenThanksSignatures);
        }
    }

    else if (document.getElementById('add-feedbackChart-true').checked)
    {
        doc.addImage(getFeedbackChartImg(),xChart, yChart + space, widthChart, heightChart)
        //thanks
        doc.text('Meilleures salutations,', xText, yChart + space + spaceCharts)
        //signature(s)
        doc.text( splitSignature, xText, yChart + space + spaceCharts + spaceBetweenThanksSignatures  );
    }

    else if (document.getElementById('add-boxPlot-true').checked)
    {
        doc.addImage(getBoxPlotImg(),xChart, yChart + space, widthChart, heightChart )
        //thanks
        doc.text('Meilleures salutations,', xText, yChart + space + spaceCharts)
        //signature(s)
        doc.text( splitSignature, xText, yChart + space + spaceCharts + spaceBetweenThanksSignatures );
    }
    else{

        //thanks
        doc.text('Meilleures salutations,', xText, yChart + space)
        //signature(s)
        doc.text( splitSignature, xText, yChart + space + spaceBetweenThanksSignatures);

    }

    doc.save('feedback.pdf');

}