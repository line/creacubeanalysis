

function makeBehaviorExcel(participations)
{
    var workSheetBehavior = [['idVideo','idParticipant','Age','AgeCategory', 'Gender', 'idTeam', 'idVideo', 'Total time','Total time (in second)', 'A1 time','A1 time (in second)','First Figure A1','Last Figure A1','A2 time','A2 time (in second)', 'First Figure A2', 'Last Figure A2', 'Perseverance Time on first family shape',  'Perseverance Time On Profile','First Intention','Final Solution','Profile' ]]

    let timesTotal= [];
    let timesA1= [];
    let timesA2= [];

    for (let i = 0 ;i < participations.length;i++)
    {
        let data = [participations[i].idVideo,participations[i].participants.idParticipant, participations[i].participants.age, participations[i].participants.ageCategory, participations[i].participants.gender, participations[i].idTeam, participations[i].idVideo]

        let times = allTimeCalculator(participations[i])
        let figures = getBehaviorFigure(participations[i])
        let profile = profileCalculator(participations[i])

        timesTotal.push(totalTimeCalculator(times))
        timesA1.push(A1TimeCalculator(times))
        timesA2.push(A2TimeCalculator(times))

        data.push(timeConverter(totalTimeCalculator(times)))
        data.push(timesTotal[i])


        data.push(timeConverter(A1TimeCalculator(times)))
        data.push(timesA1[i])
        data.push(figures[0])
        data.push(figures[1])

        data.push(timeConverter(A2TimeCalculator(times)))
        data.push(timesA2[i])
        data.push(figures[2])
        data.push(figures[3])

        //Final criteria


        if(profile === 'Non-intentional Creative'){debugger}

        data.push(getPerseveranceTime(participations[i]))

        data.push(getPerseveranceTime(participations[i], 'Profile',profile))

        if(figures[1] !== undefined && figures[1] !== "" && figures[2] !== undefined && figures[2] !== "" && figures[3] !== undefined && figures[3] !== ""  ){
            if(figures[1] === figures[2] && figures[2] && figures[2]!== "T03"){
                data.push("Conservative")
            } else if (figures[2]=== "T03" && figures[3]=== "T03") {
                data.push("")
            } else { data.push("Creative")}

            if(figures[1] === figures[3] && figures[2]!== "T03" ){
                data.push("Conservative")
            } else if (figures[2]=== "T03" && figures[3]=== "T03") {
                data.push("")
            } else { data.push("Creative")}

            data.push(profile)
        }


        workSheetBehavior.push(data)
    }


    createExcel(workSheetBehavior)
}


function createExcel(workSheetB)
{
    var wb = XLSX.utils.book_new();

    wb.Props = {
        Title: "ANR CreaMaker Behavior",
        Subject: "CreaCube",
        Author: "LINE",
    };


    wb.SheetNames.push("Behavior");

    wb.Sheets["Behavior"] = XLSX.utils.aoa_to_sheet(workSheetB);


    var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
        var view = new Uint8Array(buf);  //create uint8array as viewer
        for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
        return buf;
    }

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'ANR CreaMaker Behavior.xlsx');

}