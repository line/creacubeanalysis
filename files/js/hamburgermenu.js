$(function () {
	$('.hMenu-nav-link').on('click', function (m) {
		submit(m.currentTarget.id);
	});
});

function submit(id){
	var form = document.createElement("form");

	form.setAttribute("id", "selMenu");
	form.setAttribute("method", "POST");
	form.setAttribute("action", "/");

	var champCache = document.createElement("input");
	champCache.setAttribute("type", "hidden");
	champCache.setAttribute("id", "idMenuHidden");
	champCache.setAttribute("name", "idMenu");
	champCache.setAttribute("value", id);
	form.appendChild(champCache);

	document.body.appendChild(form);
	form.submit();
}
