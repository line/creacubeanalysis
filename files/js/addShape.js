
$("#done").hide()
$("#error").hide()

async function addShape()
{
    $("#done").hide()
    $("#error").hide()
debugger
    let newShape={}
    newShape.family = document.getElementById('family').value
    newShape.combination = document.getElementById('combination').value

    let combinationIsExisted = await waitForCombinationExists(newShape)

if(!combinationIsExisted)
{
    $.post(
        '/addShape',
        {
            shape: newShape
        },
        $("#done").html("La figure a été ajouté").show()
    );
} else {
    $("#error").html("La figure existe déjà").show()
}

}

function waitForCombinationExists(shape) {
    return new Promise((resolve => {
        getCombinationExists(shape, function (result) {
            resolve(result)
        })
    }))
}


function getCombinationExists(shapeToVerify, callback)
{
    $.get(
        '/getCombinationExists',
    {
        shape: shapeToVerify
    },
    callback
    )
}