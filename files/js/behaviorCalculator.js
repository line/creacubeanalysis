

function profileCalculator(participation)
{
    let figures = getBehaviorFigure(participation)
    let profile = ""

    if(figures[1] !== undefined && figures[1] !== "" && figures[2] !== undefined && figures[2] !== "" && figures[3] !== undefined && figures[3] !== ""){
        if(figures[1]===figures[2] && figures[2] === figures[3]){
            profile = "Conservative"
        } else if(figures[1]===figures[2] && figures[3] !== figures[2]){
            profile = "Unintentional Creative"
        } else if(figures[1] !== figures[2] && figures[3] === figures[1]){
            profile = "Unintentional Conservative"
        } else if(figures[2]=== "T03" && figures[3] ==="T03") {
            profile= "Passive"
        } else {profile = "Creative"}
    }

    return profile
}

function getBehaviorFigure(participation)
{

    let firstFigureA1 = ""
    let lastFigureA1 = ""
    let firstFigureA2 = ""
    let lastFigureA2 = ""

    let fluidity = fluidityCalculatorActivity(participation)
    let endA1 = fluidity.indexOf("T03")

    if (fluidity[fluidity.length - 1] === "T02"){
        firstFigureA1 = fluidity[0]
        let endA = fluidity.indexOf("T02")
        lastFigureA1 = fluidity[endA1 === -1 ? endA : (endA1-1)]
        endA1 === -1 ? firstFigureA2 = "" : firstFigureA2 = fluidity[endA1+1]
        endA1 === -1 ? firstFigureA2 = "" : firstFigureA2 = fluidity[endA]
        return [firstFigureA1, lastFigureA1, firstFigureA2,lastFigureA2]
    } else if (endA1 === fluidity.length - 1 )
    {
        firstFigureA1 = fluidity[0]
        lastFigureA1 = fluidity[endA1-1]
        return [firstFigureA1, lastFigureA1, firstFigureA2,lastFigureA2] }


    firstFigureA1 = fluidity[0]
    lastFigureA1 = fluidity[endA1-1]
    firstFigureA2 = fluidity[endA1+1]
    lastFigureA2 = fluidity[fluidity.length - 2]

    return [firstFigureA1, lastFigureA1, firstFigureA2,lastFigureA2]

}

function getPerseveranceTime(participation,type, profile)
{
    let fluidityAndTime = fluidityTimeCalculator(participation)
    let fluidity = fluidityAndTime[0]
    let time = fluidityAndTime[1]


    let endA1 = fluidity.indexOf("T03")

    let finalA1Figure = fluidity[endA1-1]

    fluidity = fluidity.slice(endA1 + 1)
    fluidity.pop()
    time = time.slice(endA1 + 1)
    time.pop()

    let perseveranceTime = 0
    let momentChange = 0


    if (type === 'Profile'){

        if(profile === "Unintentional Conservative")
        {
            for (let i = 0; i <= fluidity.length - 1; i++) {
                let f = fluidity[i];

                if (f === finalA1Figure) {
                    momentChange = time[i]
                    perseveranceTime = momentChange - time[0]
                    break
                }
            }
        } else if (profile === "Unintentional Creative")
        {
            let initFigure = fluidity[0]

            for (let i = 0; i <= fluidity.length - 1; i++) {
                let f = fluidity[i];

                if (f !== initFigure) {
                    momentChange = time[i]
                    perseveranceTime = momentChange - time[0]
                    break
                }
            }
        } else {
            perseveranceTime = 0
        }
    } else {
        let initFigure = fluidity[0]
        for (let i = 0; i <= fluidity.length - 1; i++){
            let f = fluidity[i];

            if(f[0] !== initFigure[0] || f[1] !== initFigure[1]) {
                momentChange = time[i]
                perseveranceTime = momentChange - time[0]
                break
            }
        }
    }

    if (perseveranceTime === 0) perseveranceTime = null
    return perseveranceTime
}

function getTeamData(participation)
{

    return $.get(
        '/getTeam',
        { idVideo : participation.idVideo,
            idTeam : participation.idTeam,
            idParticipant : participation.participants.idParticipant
        },
        'json'
    );
}