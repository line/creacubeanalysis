var allData = new Array()
var genderData = new Array()
var ageData = new Array()
var profiles = new Array()

/**
 * To display the feedback chart(s) in function if "display by activity" is checked
 */
function initBehaviorChart() {

    if(document.getElementById('passive-profile').checked)
    {
        profiles = ['Profile','Conservative','Creative', 'Unintentional Conservative', 'Unintentional Creative', 'Passive']
    } else profiles = ['Profile','Conservative','Creative', 'Unintentional Creative', 'Unintentional Conservative']

    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBehaviorChart);
    google.charts.setOnLoadCallback(drawBehaviorGenderChart);
    google.charts.setOnLoadCallback(drawBehaviorAgeChart);
    google.charts.setOnLoadCallback(drawBehaviorPercentageChart);
    google.charts.setOnLoadCallback(drawBehaviorPercentageGenderChart);
    google.charts.setOnLoadCallback(drawBehaviorPercentageAgeChart);
    //google.charts.setOnLoadCallback(drawBehaviPercentageorCoopChart);

}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
 function drawBehaviorChart() {

    let data = []

    allData = prepareData(currentParticipation)
    data.push(profiles)
    data.push(allData)

    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior',
        colors: [ '#D0CECE','#AFABAB','#767171', '#404040', '#0D0D0D' ],
        vAxis : {title : 'number of participants'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-all-div'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-all').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
function drawBehaviorGenderChart() {

    let data = []

    data.push(profiles)

    genderData = prepareDataGender(currentParticipation)

    data.push(genderData[0])
    data.push(genderData[1])


    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior with gender filter',
        colors: [  '#D0CECE','#AFABAB','#767171', '#404040', '#0D0D0D' ],
        vAxis : {title : 'number of participants'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-gender-div'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-gender').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
function drawBehaviorAgeChart() {

    let data = []
    data.push(profiles)

    ageData = prepareDataAge(currentParticipation)

    data.push(ageData[0])
    data.push(ageData[1])
    data.push(ageData[2])
    data.push(ageData[3])
    /*data.push(ageData[4])
    data.push(ageData[5])*/
    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior with age filter',
        colors: [  '#D0CECE','#AFABAB','#767171', '#404040', '#0D0D0D' ],
        vAxis : {title : 'number of participants'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-age-div'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-age').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
function drawBehaviorPercentageChart() {

    let data = []
    data.push(profiles)
    data.push(getPercent(allData))

    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior in percentage',
        colors: [  '#D0CECE','#AFABAB','#767171', '#404040', '#0D0D0D' ],
        vAxis : {title : 'participants percentage'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-all-div2'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-all2').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
function drawBehaviorPercentageGenderChart() {

    let data = []
    data.push(profiles)

    let genderPercentData = getPercent(genderData)

    data.push(genderPercentData[0])
    data.push(genderPercentData[1])


    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior in percentage with gender filter',
        colors: [  '#D0CECE','#AFABAB','#767171', '#404040', '#0D0D0D' ],
        vAxis : {title : 'participants percentage'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-gender-div2'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-gender2').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
function drawBehaviorPercentageAgeChart() {

    let data = []
    data.push(profiles)

    let agePercentData = getPercent(ageData)

    data.push(agePercentData[0])
    data.push(agePercentData[1])
    data.push(agePercentData[2])
    data.push(agePercentData[3])
    /*data.push(agePercentData[4])
    data.push(agePercentData[5])*/
    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior in percentage with age filter',
        colors: [  '#D0CECE','#AFABAB','#767171', '#404040', '#0D0D0D' ],
        vAxis : {title : 'participants percentage'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-age-div2'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-age2').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}


/**
* To calculate the data for the participant(s) added in the chart and to prepare the download
*/
function drawBehaviorCoopChart() {

    let data = []
    let profiles = ['Profile','Conservative','Creative', 'Non-intentional Conservative', 'Non-intentional Creative']
    data.push(profiles)

    let coopData = prepareDataCoop(currentParticipation)

    data.push(coopData[0])
    data.push(coopData[1])
    data.push(coopData[2])


    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaMaker Behavior with gender team filter',
        colors: [ '#06707B','#EB8B0C','#D9EB0D', '#8BDCE6', '#976BB3' ],
        vAxis : {title : 'participants percentage'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-coop-div'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        document.getElementById('save-img-coop').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueBehaviorCreaMaker.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}


function prepareData(participations)
{
    let counts = ['Profiles',0,0,0,0]
    if (document.getElementById('passive-profile').checked){
        counts = ['Profiles',0,0,0,0,0]
    }

    participations.forEach( participation => {
        let profile = profileCalculator(participation)
        switch (profile) {
            case profiles[1]:
                counts[1] += 1
                break;
            case profiles[2]:
                counts[2] += 1
                break;
            case profiles[3] :
                counts[3] += 1
                break;
            case profiles[4]:
                counts[4] += 1
                break;
            case profiles[5]:
                if (document.getElementById('passive-profile').checked){
                    counts[5] += 1
                }
                break;
        }
    })


    return counts //(getPercent(counts))
}

function prepareDataGender(participations)
{
    let data = []
    let countsF = ['Female',0,0,0,0]
    let countsH = ['Male',0,0,0,0]
    if (document.getElementById('passive-profile').checked){
        countsF = ['Female',0,0,0,0,0]
        countsH = ['Male',0,0,0,0,0]
    }

    participations.forEach( participation => {
        let profile = profileCalculator(participation)
        switch (profile) {
            case profiles[1]:
                if(participation.participants.gender === "F")countsF[1] += 1
                else countsH[1] += 1
                break;
            case profiles[2]:
                if(participation.participants.gender === "F")countsF[2] += 1
                else countsH[2] += 1
                break;
            case profiles[3] :
                if(participation.participants.gender === "F")countsF[3] += 1
                else countsH[3] += 1
                break;
            case profiles[4]:
                if(participation.participants.gender === "F")countsF[4] += 1
                else countsH[4] += 1
                break;
            case profiles[5]:
                if (document.getElementById('passive-profile').checked){
                    if(participation.participants.gender === "F")countsF[5] += 1
                    else countsH[5] += 1
                }
        }
    })

    data.push(countsF)
    data.push(countsH)
    return data
}

function prepareDataAge(participations)
{
    let data = []

    //let countsBeb = ['Young child',0,0,0,0]
    let countsEnf = ['Child',0,0,0,0]
    let countsAdo = ['Adolescent',0,0,0,0]
    let countsJad = ['Young Adult',0,0,0,0]
    let countsAdu = ['Adult',0,0,0,0]
    //let countsSen = ['Senior',0,0,0,0]
    if (document.getElementById('passive-profile').checked){
        //countsBeb = ['Young child',0,0,0,0,0]
        countsEnf = ['Child',0,0,0,0,0]
       countsAdo = ['Adolescent',0,0,0,0,0]
       countsJad = ['Young Adult',0,0,0,0,0]
       countsAdu = ['Adult',0,0,0,0,0]
       //countsSen = ['Senior',0,0,0,0,0]
    }

    participations.forEach( participation => {
        let profile = profileCalculator(participation)
        switch (profile) {
            case profiles[1]:
                /*if(participation.participants.ageCategory === "beb")countsBeb[1] += 1
                else*/ if (participation.participants.ageCategory === "enf") countsEnf[1] += 1
                else if (participation.participants.ageCategory === "ado") countsAdo[1] += 1
                else if (participation.participants.ageCategory === "jad") countsJad[1] += 1
                else if (participation.participants.ageCategory === "adu") countsAdu[1] += 1
                //else if (participation.participants.ageCategory === "sen") countsSen[1] += 1
                break;
            case profiles[2]:
                /*if(participation.participants.ageCategory === "beb")countsBeb[2] += 1
                else*/ if (participation.participants.ageCategory === "enf") countsEnf[2] += 1
                else if (participation.participants.ageCategory === "ado") countsAdo[2] += 1
                else if (participation.participants.ageCategory === "jad") countsJad[2] += 1
                else if (participation.participants.ageCategory === "adu") countsAdu[2] += 1
                //else if (participation.participants.ageCategory === "sen") countsSen[2] += 1
                break;
            case profiles[3] :
                /*if(participation.participants.ageCategory === "beb")countsBeb[3] += 1
                else*/ if (participation.participants.ageCategory === "enf") countsEnf[3] += 1
                else if (participation.participants.ageCategory === "ado") countsAdo[3] += 1
                else if (participation.participants.ageCategory === "jad") countsJad[3] += 1
                else if (participation.participants.ageCategory === "adu") countsAdu[3] += 1
                //else if (participation.participants.ageCategory === "sen") countsSen[3] += 1
                break;
            case profiles[4]:
                /*if(participation.participants.ageCategory === "beb")countsBeb[4] += 1
                else*/ if (participation.participants.ageCategory === "enf") countsEnf[4] += 1
                else if (participation.participants.ageCategory === "ado") countsAdo[4] += 1
                else if (participation.participants.ageCategory === "jad") countsJad[4] += 1
                else if (participation.participants.ageCategory === "adu") countsAdu[4] += 1
                //else if (participation.participants.ageCategory === "sen") countsSen[4] += 1
                break
            case profiles[5]:
                if (document.getElementById('passive-profile').checked){
                    /*if(participation.participants.ageCategory === "beb")countsBeb[5] += 1
                    else*/ if (participation.participants.ageCategory === "enf") countsEnf[5] += 1
                    else if (participation.participants.ageCategory === "ado") countsAdo[5] += 1
                    else if (participation.participants.ageCategory === "jad") countsJad[5] += 1
                    else if (participation.participants.ageCategory === "adu") countsAdu[5] += 1
                    //else if (participation.participants.ageCategory === "sen") countsSen[5] += 1
                }
        }
    })

    /*data.push(getPercent(countsBeb))
    data.push(getPercent(countsEnf))
    data.push(getPercent(countsAdo))
    data.push(getPercent(countsJad))
    data.push(getPercent(countsAdu))
    data.push(getPercent(countsSen))*/

    //data.push(countsBeb)
    data.push(countsEnf)
    data.push(countsAdo)
    data.push(countsJad)
    data.push(countsAdu)
    //data.push(countsSen)

    return data
}


function prepareDataCoop(participations)
{
    let data = []
    let countsFemaleTeam = ['Female team',0,0,0,0]
    let countsMaleTeam = ['Male team',0,0,0,0]
    let countsMixedTeam = ['Mixed team',0,0,0,0]


    participations.forEach( participation => {
        if (participation.idTeam !== "ind"){
                getTeamData(participation).then(function (team) {
                    let profile = profileCalculator(participation)
                    let gender = participation.participants.gender
                    let membersGender = []
                    debugger
                    team.forEach(member => membersGender.push(member.participants.gender))

                    switch (profile) {
                        case 'Conservative':
                            if(membersGender[0] === gender && (membersGender[1] === gender || membersGender.length === 1) )
                            {
                                gender === "F" || gender === "f" ? countsFemaleTeam[1] += 1 : countsMaleTeam[1] += 1
                            } else {countsMixedTeam[1] += 1}

                            break;
                        case 'Creative':
                            if(membersGender[0] === gender && (membersGender[1] === gender || membersGender.length === 1) )
                            {
                                gender === "F" || gender === "f" ? countsFemaleTeam[2] += 1 : countsMaleTeam[2] += 1
                            } else {countsMixedTeam[2] += 1}
                            break;
                        case 'Non-intentional Conservative' :
                            if(membersGender[0] === gender && (membersGender[1] === gender || membersGender.length === 1) )
                            {
                                gender === "F" || gender === "f" ? countsFemaleTeam[3] += 1 : countsMaleTeam[3] += 1
                            } else {countsMixedTeam[3] += 1}
                            break;
                        case 'Non-intentional Creative':
                            if(membersGender[0] === gender && (membersGender[1] === gender || membersGender.length === 1) )
                            {
                                gender === "F" || gender === "f" ? countsFemaleTeam[4] += 1 : countsMaleTeam[4] += 1
                            } else {countsMixedTeam[4] += 1}
                            break
                    }

                })
        }
    })

    data.push(getPercent(countsFemaleTeam))
    data.push(getPercent(countsMaleTeam))
    data.push(getPercent(countsMixedTeam))

    return data
}

function getPercent(data)
{
    if(typeof data[1] === "number")
    {

        let total = 0
        for (let x = 1; x < data.length; x++) {
            total += data[x]
        }
        for (let x = 1; x < data.length; x++) {
            data[x] = (data[x] * 100) / total
        }

    } else {
        for(let i=0; i< data.length; i++ )
        {
        let total = 0

            for (let x = 1; x < data[i].length; x++) {
                total += data[i][x]
            }
            for (let y = 1; y < data[i].length; y++) {
                data[i][y] = (data[i][y] * 100) / total
            }
        }
    }

    return data
}