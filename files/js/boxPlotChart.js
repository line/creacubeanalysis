
var boxPlotImg;


/**
 * To display the box plot chart
 */
function initBoxPlot() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawBoxPlot);
}

/**
 * To calculate the data for the participants added in the chart and to prepare the download
 */
function drawBoxPlot() {

    let array = new Array();

    //to verify if there is a list of participant and to initialize an empty chart
    if(allFluidityCount.length === 0 || allFlexibilityCount.length === 0 || allOriginalityCount.length === 0)
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'x');
        data.addColumn('number', 'serie0');

        data.addColumn({id:'max', type:'number', role:'interval'});
        data.addColumn({id:'min', type:'number', role:'interval'});
        data.addColumn({id:'firstQuartile', type:'number', role:'interval'});
        data.addColumn({id:'median', type:'number', role:'interval'});
        data.addColumn({id:'thirdQuartile', type:'number', role:'interval'});
        data.addRows(getBoxPlotValues(array));}

    else {

        //to add the fields
        allFluidityCount.unshift("Fluidité");
        allFlexibilityCount.unshift("Flexibilité");
        allOriginalityCount.unshift("Innovation");
        allCooperation.unshift("Cooperation")

        //array for the chart's data
        array = [];
        array.push(allFluidityCount);
        array.push(allFlexibilityCount);
        array.push(allOriginalityCount);
        array.push(allCooperation);


        var data = new google.visualization.DataTable();
        data.addColumn('string', 'x');

        //to initialize the value
        for (let i = 0; i < allFluidityCount.length - 1; i++)
        {
            data.addColumn('number', idParticipant[i])
        }

        data.addColumn({id:'max', type:'number', role:'interval'});
        data.addColumn({id:'min', type:'number', role:'interval'});
        data.addColumn({id:'firstQuartile', type:'number', role:'interval'});
        data.addColumn({id:'median', type:'number', role:'interval'});
        data.addColumn({id:'thirdQuartile', type:'number', role:'interval'});

        data.addRows(getBoxPlotValues(array));
    }

    var options = {
        title:'CreaCube',
        height: 500,
        legend: {position: 'none'},
        hAxis: {
            gridlines: {color: '#fff'}
        },
        lineWidth: 0,
        series: [{colors: '#06707B'}],
        intervals: {
            barWidth: 1,
            boxWidth: 1,
            lineWidth: 2,
            style: 'boxes'
        },
        interval: {
            max: {
                style: 'bars',
                fillOpacity: 1,
                color: '#777'
            },
            min: {
                style: 'bars',
                fillOpacity: 1,
                color: '#777'
            }
        }
    };

    var chart = new google.visualization.LineChart(document.getElementById('box-plot'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        boxPlotImg = chart.getImageURI();

        document.getElementById('save-img-boxPlot').onclick = function (){
            download(chart.getImageURI(), 'graphiqueCreaCube2.png', "image/png")

        }
    });

    chart.draw(data, options);
}


/**
 * Takes an array of input data and returns an
 * array of the input data with the box plot
 * interval data appended to each row.
 */
function getBoxPlotValues(array) {


    for (var i = 0; i < array.length; i++) {

        var arr = array[i].slice(1).sort(function (a, b) {
            return a - b;
        });


        var max = arr[arr.length - 1];
        var min = arr[0];
        var median = getMedian(arr);
        var indexOfMedian = getIndexOfMedian(arr);


        // First Quartile is the median from lowest to overall median.
        var firstQuartile = getMedian(arr.slice(0,indexOfMedian));

        // Third Quartile is the median from the overall median to the highest.
        var thirdQuartile = getMedian(arr.slice(indexOfMedian));

        array[i][arr.length + 1] = max;
        array[i][arr.length + 2] = min
        array[i][arr.length + 3] = firstQuartile;
        array[i][arr.length + 4] = median;
        array[i][arr.length + 5] = thirdQuartile;

    }

    return array;
}

function average(a) {

    var b = (a.length) -1,
        c = 0, i;
    for (i = 1; i < b; i++){
        c += a[i];
    }
    return c/b;
}

/**
 * To take an array and returns the median value
 * @param array
 * @returns {number|*}
 */
function getMedian(array) {
    var length = array.length;

    /* If the array is an even length the
     * median is the average of the two
     * middle-most values. Otherwise the
     * median is the middle-most value.
     */
    if (length % 2 === 0) {
        var midUpper = length / 2;
        var midLower = midUpper - 1;

        return (array[midUpper] + array[midLower]) / 2;
    } else {
        return array[Math.floor(length / 2)];
    }
}

/**
 * To take an array and returns the index of the median value
 * @param array
 * @returns {number|*}
 */
function getIndexOfMedian(array) {
    var length = array.length;

    /* If the array is an even length the
     * median is the average of the two
     * middle-most values. Otherwise the
     * median is the middle-most value.
     */
    if (length % 2 === 0) {
        var midUpper = length / 2;
        var midLower = midUpper - 1;

        return Math.ceil((midUpper + midLower) / 2);
    } else {
        return Math.floor(length / 2);
    }
}


/**
 * To calculate the fluidity, flexibility and innovation for all the participant in the list and to add them in the box plot when the button #add-all is click
 * @param participations
 */

async function addAll(participations){

    await calculateGlobalCreativity(participations)

    initBoxPlot();
}



function addAllBegin()
{
    getAll(function (participations){
        addAll(participations);
        currentParticipation = participations
        Object.assign(Participation, participations)
    })
}


function getBoxPlotImg()
{
    return boxPlotImg;
}