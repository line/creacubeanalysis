/**
 * To have the time of the START, the first T03 and the second T03 (in case of activity2 field is true)
 * @param participation
 * @returns {number[]}
 */

function allTimeCalculator(participation)
{

    let begin = 0
    let beginA2 = 0
    let numberA = 0
    let endA1 = 0
    let endA2 = 0

    let af01 = 0
    let af02 = 0
    let af03 = 0
    let af04 = 0
    let af05 = 0

    if (participation.participants.clicks.length > 0) {
        for (k in participation.participants.clicks) {
            for (cl of participation.participants.clicks[k].tclicks) {
                var t = (participation.participants.clicks[k].time).toString();
                if (cl == "START" || cl[0] == "T" || cl == "START1" || cl == "START2" || (cl[0] == "A" && cl[1]== "F")) {
                    var f = 0;
                    if (cl == "START" || cl == "START1")
                    {

                        begin = t;
                    }

                    if(cl[0] === "A" && cl[1] === "F" && cl[2] === "0")
                    {
                        switch (cl) {
                            case 'AF01': af01 = t
                                break;
                            case 'AF02': af02 = t
                                break;
                            case 'AF03': af03 = t
                                break;
                            case 'AF04': af04 = t
                                break;
                            case 'AF05': af05 = t
                                break;
                        }
                    }

                    if ((cl[0] == "T" && cl[1] == "0" && (cl[2] == "3" || cl[2] == "2"))) {

                        numberA ++

                        if(numberA === 1)
                        {

                            endA1 = t;
                            if (participation.activity2 === false)
                            {

                                endA2 = endA1
                                break;
                            }

                        } else if(numberA === 2)
                        {

                            endA2 = t;
                            break;
                        }

                    }

                    if (cl == "START2")
                    {

                        beginA2 = t;
                    }
                }
            }
        }
    }


    if(beginA2 === 0)
    {
        beginA2 = endA2
    }

    return [begin, endA1, beginA2, endA2, af01, af02, af03, af04, af05];
}

/**
 * To calculate the total time of the both activities for a participant
 * @param times
 * @returns {number}
 */
function totalTimeCalculator(times)
{
    let totalTime = (times[1]-times[0]) + (times[3]-times[2])
    /*if (times[3] !== 0 && times[0] < times[3] )
    {
        totalTime = (times[1] - times[0]) + (times[3] - times[2])
    }*/

    return totalTime
}

/**
 * To calculate the time of the A1 for a participant
 * @param times
 * @returns {number}
 */
function A1TimeCalculator(times)
{

    return times[1]-times[0]
}

/**
 * To calculate the time of the A2 for a participant
 * @param times
 * @returns {number}
 */
function A2TimeCalculator(times)
{

    return times[3]-times[2]
}

/**
 * To convert seconds in a string hour:min:sec
 * @param time
 * @returns {string}
 */
function timeConverter(time)
{
    var minutes = Math.floor(time / 60);
    var seconds = time - minutes * 60;
    var hours = Math.floor(time / 3600);
    time = time - hours * 3600;

    function str_pad_left(string,pad,length) {
        return (new Array(length+1).join(pad)+string).slice(-length);
    }

    var finalTime = str_pad_left(hours,'0',2)+':'+str_pad_left(minutes,'0',2)+':'+str_pad_left(seconds,'0',2);
    return finalTime
}

