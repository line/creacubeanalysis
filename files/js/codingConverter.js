/***
 *
 */
function converter(oldShape)
{
    let newShape = "";

    switch (oldShape) {
        case '01': newShape = "000"
            break;
        case '02': newShape = "010"
            break;
        case '03': newShape = "020"
            break;
        case '04': newShape = "030"
            break;
        case '05': newShape = "040"
            break;
        case '06': newShape = "021"
            break;
        case '07': newShape = "002"
            break;
        case '08': newShape = "042"
            break;
        case '09': newShape = "050"
            break;
        case '10': newShape = "022"
            break;
        case '11': newShape = "013"
            break;
        case '12': newShape = "024"
            break;
        case '13': newShape = "034"
            break;
        case '14': newShape = "051"
            break;
        case '15': newShape = "012"
            break;
        case '16': newShape = "X1"
            break;
        case '17': newShape = "XX"
            break;
        case '18': newShape = "014"
            break;
        case '19': newShape = "060"
            break;
    }

    return newShape

}

function converterForChart(oldShape)
{
    let newShape = 0;

    switch (oldShape) {
        case '000': newShape = 1
            break;
        case '002': newShape = 2
            break;
        case '010': newShape = 3
            break;
        case '011': newShape = 4
            break;
        case '012': newShape = 5
            break;
        case '013': newShape = 6
            break;
        case '014': newShape = 7
            break;
        case '015': newShape = 8
            break;
        case '020': newShape = 9
            break;
        case '021': newShape = 10
            break;
        case '022': newShape = 11
            break;
        case '024': newShape = 12
            break;
        case '025': newShape = 13
            break;
        case '030': newShape = 14
            break;
        case '034': newShape = 15
            break;
        case '040': newShape = 16
            break;
        case '042': newShape = 17
            break;
        case '044': newShape = 18
            break;
        case '045': newShape = 19
            break;
        case '050': newShape = 20
            break;
        case '051': newShape = 21
            break;
        case '055': newShape = 22
            break;
        case '056': newShape = 23
            break;
        case '060': newShape = 24
            break;
        case '061': newShape = 25
            break;
        case '064': newShape = 26
            break;
        case '065': newShape = 27
            break;
        case '066': newShape = 28
            break;
        case 'X1': newShape = 29
            break;
        case 'XX': newShape = 30
            break;


    }


    return newShape

}

function converterByFamily(oldShape)
{
    let newShape = 0;

    switch (oldShape) {
        case '000': newShape = "000"
            break;
        case '002': newShape = "002"
            break;
        case '010': newShape = "010"
            break;
        case '011': newShape = "010"
            break;
        case '012': newShape = "010"
            break;
        case '013': newShape = "010"
            break;
        case '014': newShape = "010"
            break;
        case '015': newShape = "010"
            break;
        case '020': newShape = "020"
            break;
        case '021': newShape = "020"
            break;
        case '022': newShape = "020"
            break;
        case '024': newShape = "020"
            break;
        case '025': newShape = "020"
            break;
        case '030': newShape = "030"
            break;
        case '034': newShape = "030"
            break;
        case '040': newShape = "040"
            break;
        case '042': newShape = "040"
            break;
        case '044': newShape = "040"
            break;
        case '045': newShape = "040"
            break;
        case '050': newShape = "050"
            break;
        case '051': newShape = "050"
            break;
        case '055': newShape = "050"
            break;
        case '056': newShape = "050"
            break;
        case '060': newShape = "060"
            break;
        case '061': newShape = "060"
            break;
        case '064': newShape = "060"
            break;
        case '065': newShape = "060"
            break;
        case '066': newShape = "060"
            break;
        case 'X1': newShape = "XX"
            break;
        case 'XX': newShape = "XX"
            break;


    }


    return newShape

}