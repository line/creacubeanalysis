var selected = []
var activities1 = []
var activities2 = []
/**
 * To display the feedback chart(s) in function if "display by activity" is checked
 */
function initFlowChart() {

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawFlowChartA1);

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawFlowChartA2);

}


function drawFlowChartA1() {
    var data = google.visualization.arrayToDataTable([
        ['Time', 'p01', 'p02'],
        ['2013',  1000,      400],
        ['2014',  1170,      460],
        ['2015',  660,       1120],
        ['2016',  1030,      540]
    ]);

    var options = {
        title: 'Activity 1',
        hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0},
        //orientation: 'vertical'
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_A1'));
    chart.draw(data, options);

}

function drawFlowChartA2() {
    var data = google.visualization.arrayToDataTable([
        ['Time', 'p01', 'p02'],
        [1,  1000,      400],
        [2,  1170,      460],
        [4,  660,       1120],
        [5,  1030,      540]
    ]);

    var options = {
        title: 'Activity 2',
        hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
        colors: [ '#06707B','#EB8B0C','#D9EB0D', '#8BDCE6', '#976BB3' ],
        vAxis: {minValue: 0},
        //orientation: 'vertical'
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_A2'));
    chart.draw(data, options);

}

/**
 * To get the participant selected and to add it in the chart if the button is clicked
 * @param value
 */
function changeSelection(value){

    $("#messages").hide();

    getParticipation(value[0], function (participation){
        addParticipant(participation);

    })
}

/**
 * To calculate information for a participant and send it to the chart when the button is clicked
 * @param participation
 */
function addParticipant(participation)
{
    //when the button add is clicked
    document.getElementById('add-participant').onclick = async function () {


        //to verify there is no space in the chart
        if (selected.length === 4) {
            $("#messages").html("Le graphique est complet").show();

        } else {

            participation.index = selected.length
            selected.push(participation)
            var clicks1 = new Array();
            var clicks2 = new Array();
            var clicksT03 = new Array();

            if (participation.participants.clicks.length > 0) {

                var activite = 1;
                var t0 = -1;

                for (k in participation.participants.clicks) {

                    for (cl of participation.participants.clicks[k].tclicks) {

                        if (cl == "START" || cl == "START1") {
                            t0 = participation.participants.clicks[k].time;
                        }

                        var t = participation.participants.clicks[k].time - t0;

                        if (cl == "START2" || cl[0] == "T" || (cl[0] == "F"&& cl[1] != "L")) {
                            var f = 0;

                            if (cl[0] === "F" && cl[1] !== "L") {
                                f = cl.substring(1,4)
                            } else if (cl == "FXX") {
                                f = "XX";
                            } else if (cl === "FX1"){
                                f = "X1";
                            } else if (cl[0] == "T" || cl == "START2") {
                                activite++;
                            }
                            if (activite == 1) {
                                clicks1.push({ "time" : t, "click" : f });
                            } else if (activite == 2) {
                                clicksT03.push({ "time" : t, "click" : f });
                            } else if (activite == 3) {
                                clicks2.push({ "time" : t, "click" : f });
                            }

                        }
                    }
                }


                if(clicks2.length === 0){
                    activities1 = clicks1
                    activities2 = clicksT03
                } else {
                    activities1 = clicks1
                    activities2 = clicksT03 + clicks2
                    console.log("start2")
                    console.log(activities2)
                }

            }

            //to add the participant in the chart and init the chart
            initFlowChart();

        }

    }
}


/**
 * To get information of a participant
 * @param id
 * @param callback
 */
function getParticipation(id, callback) {
    $.get(
        '/participation',
        { id : id },
        callback,
        'json'
    );
}