document.getElementById('close').addEventListener('click', function () {
    if (document.getElementById("areaEvent").children.length > 2)
    {
        document.getElementById("areaEvent").lastChild.remove()
        document.getElementById("areaEvent").lastChild.remove()
    }
    $("#trueF").hide()
    $("#falseF").hide()
    $("#emptyF").show()
    $("#functionalOrNot").hide();
})
document.getElementById('enregistrerEventBtn').addEventListener('click', function () {
    if (document.getElementById("areaEvent").children.length > 2)
    {
        document.getElementById("areaEvent").lastChild.remove()
        document.getElementById("areaEvent").lastChild.remove()
    }
    $("#trueF").hide()
    $("#falseF").hide()
    $("#emptyF").show()
    $("#functionalOrNot").hide();
})
document.getElementById('confirmerSupprimerEventBtn').addEventListener('click', function () {

    if (document.getElementById("areaEvent").children.length > 2)
    {
        document.getElementById("areaEvent").lastChild.remove()
        document.getElementById("areaEvent").lastChild.remove()
    }
    $("#trueF").hide()
    $("#falseF").hide()
    $("#emptyF").show()
    $("#functionalOrNot").hide();
})
document.getElementById('cancelActions').addEventListener('click', function () {

    if (document.getElementById("areaEvent").children.length > 2)
    {
        document.getElementById("areaEvent").lastChild.remove()
        document.getElementById("areaEvent").lastChild.remove()
    }
    $("#trueF").hide()
    $("#falseF").hide()
    $("#emptyF").show()
    $("#functionalOrNot").hide();
})

$("#trueF").hide()
$("#falseF").hide()
$("#functionalOrNot").hide();

function changeOK(video, OKType)
{
    $.post(
        '/modifyOK',
        {
            idVideo: video.idVideo,
            OKType: OKType,
            newValue: false
        }
    );

    $("#" + OKType + "_false").show()
    $("#" + OKType + "_true").hide()
}

function changeKO(video, OKType)
{
    $.post(
        '/modifyOK',
        {
            idVideo: video.idVideo,
            OKType: OKType,
            newValue: true
        }
    );

    $("#" + OKType + "_true").show()
    $("#" + OKType + "_false").hide()
}

function deleteVideo(video)
{
    if(confirm("Voulez-vous supprimer la vidéo " + video.idVideo + " ? Attention, elle sera définitivement supprimée du serveur !" ))
    {
        $.post(
            '/deleteVideo',
            {
                video: video,
            },
            window.location.href = "/"
        );
    }

}

function deleteAllEvents(video, participant)
{
    if(confirm("Voulez-vous supprimer tous les évènements du participant " + participant.idParticipant + " ? Attention, cela est irréversible !" ))
    {
        saveClicks(video.idVideo, participant.idParticipant, new Array())

        let deletedEventsP = getParticipation(participant.idParticipant)
        deletedEventsP.clicks = []
    }
}