var allFields = [['Participant', 'Fluidité' , 'Flexibilité', 'Innovation','Coopération', ]];
const allParticipation = new Array();
const chartIdParticipant = new Array();
const chartFluidityLength = new Array()
const chartFluidityLengthActivity = new Array();
const chartFluidityActivity = new Array();
const chartIdTeam = new Array();
var maxFluidityActivities = 1
var feedbackChartImg;

/**
 * To display the feedback chart(s) in function if "display by activity" is checked
 */
function initFeedbackChart() {

    if (document.getElementById('display-by-activity').checked) {
        $("#chart-div").hide();
        $("#save-img").hide();

        $("#chart-div1").show();
        $("#save-img1").show();
        $("#chart-div2").show();
        $("#save-img2").show();

        google.charts.load('current', {'packages':['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawChartActivity1);
        google.charts.load('current', {'packages':['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawChartActivity2);

    } else {
        $("#chart-div1").hide();
        $("#save-img1").hide();
        $("#chart-div2").hide();
        $("#save-img2").hide();

        $("#chart-div").show();
        $("#save-img").show();

        google.charts.load('current', {'packages':['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawChart);
    }

}

/**
 * To calculate the data for the participant(s) added in the chart and to prepare the download
 */
function drawChart() {

    let data = madeData(0)
    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaCube',
        colors: [ '#06707B','#EB8B0C','#D9EB0D', '#8BDCE6', '#976BB3' ],
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-div'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        feedbackChartImg = chart.getImageURI();
        document.getElementById('save-img').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueCreaCube1.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

function drawChartActivity1() {

    let data = madeData(1)
    let chartData = google.visualization.arrayToDataTable(data);

    if (chartFluidityLengthActivity.length !== 0 && document.getElementById('keep-size-axe').checked)
    {
        maxFluidityActivities = Math.max.apply(Math,chartFluidityLength)
    } else if (chartFluidityLengthActivity.length !== 0)
    {
        maxFluidityActivities = Math.max.apply(Math,chartFluidityLengthActivity)
    }

    //options to design the chart
    let options = {
        title: 'CreaCube Activité 1',
        colors: [ '#06707B','#EB8B0C','#D9EB0D', '#8BDCE6', '#976BB3' ],
        vAxis: {minValue : maxFluidityActivities}
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-div1'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        //feedbackChartImg = chart.getImageURI();
        document.getElementById('save-img1').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueCreaCubeActivité1.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}


function drawChartActivity2() {

    let data = madeData(2)
    let chartData = google.visualization.arrayToDataTable(data);

    //options to design the chart
    let options = {
        title: 'CreaCube Activité 2',
        colors: [ '#06707B','#EB8B0C','#D9EB0D', '#8BDCE6', '#976BB3' ],
        vAxis: {minValue : maxFluidityActivities}
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart-div2'));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {

        //feedbackChartImg = chart.getImageURI();
        document.getElementById('save-img2').onclick = function (){

            //to upload an image of the chart
            download(chart.getImageURI(), 'graphiqueCreaCube1.png', "image/png")
        }
    });

    chart.draw(chartData, options);
}

function madeData(activity)
{
    let data = [];
    let intermediateF = [];
    let fields = [];
    Object.assign(intermediateF, allFields[0]);
    fields.push(intermediateF)

    let chartParticipation = []



    //to verify if there is participant added in the chart or not
    if (allParticipation[0] !== undefined) {

        //the number of shape made in group of participant
        var totalFluidity = 0;
        //five percent of the totalFluidity
        var fivePercent = 0;
        //the number of shape made per participant
        var allFluidity = [];
        //the shape's name which appears less than 5%
        var originalShapes = [];
        //the name of the shape made by all the group
        var allFlexibility = [];

        //for all the participant added in the chart
        for (let i = 0; i < allParticipation.length; i++) {

            let intermediateP = []
            Object.assign(intermediateP,allParticipation[i]);
            chartParticipation.push(intermediateP)

            if (activity === 0){
                //to count the fluidity total for the participants in the chart
                totalFluidity += allParticipation[i][1].length;

                //to add participant's data in the chart
                let intermediate = [];
                Object.assign(intermediate, allParticipation[i]);
                intermediate[1] = allParticipation[i][1].length
                intermediate[4] = intermediate[4][0]
                data.push(intermediate);

            } else if (activity === 1){

                //to calculate the fluidity in function of the activity
                let fluidityAct1 = wantedActivityFluidityCalculator(chartFluidityActivity[i], 1)
                chartParticipation[i][1] = fluidityAct1

                //to count the fluidity total for the participants in the chart
                totalFluidity += allParticipation[i][1].length;

                //to add participant's data in the chart
                let intermediate = new Array()
                Object.assign(intermediate, allParticipation[i]);
                intermediate[1] = fluidityAct1.length;
                intermediate[2]= Object.keys(flexibilityCalculator(fluidityAct1)).length
                intermediate[4] = intermediate[4][1]
                data.push(intermediate);
                chartFluidityLengthActivity.push(fluidityAct1.length)

            } else if (activity === 2) {
                //to calculate the fluidity in function of the activity
                let fluidityAct2 = wantedActivityFluidityCalculator(chartFluidityActivity[i], 2)
                chartParticipation[i][1] = fluidityAct2
                //to count the fluidity total for the participants in the chart
                totalFluidity += allParticipation[i][1].length;

                //to add participant's data in the chart
                let intermediate = new Array()
                Object.assign(intermediate, allParticipation[i]);
                intermediate[1] = fluidityAct2.length;
                intermediate[2]= Object.keys(flexibilityCalculator(fluidityAct2)).length
                intermediate[4] = intermediate[4][2]
                data.push(intermediate);

            }
            //for all the participant's fluidity added in the chart
            for (let element of allParticipation[i][1]) {

                //to gather all the fluidity but per participant
                allFluidity.push(element);
            }

            //To display or not the id of participants
            if(document.getElementById('see-idParticipant').checked) {
                data[i][0] = chartIdTeam[i] + '-' + chartIdParticipant[i]
            }
        }

        //CREATIVITY CALCULATOR

        //to calculate five percent of the total fluidity
        fivePercent = totalFluidity * 0.05;

        //to calculate the flexibility of the group
        allFlexibility = flexibilityCalculator(allFluidity)

        //to list the original/innovative shapes of the group
        originalShapes = originalShapesCalculator(allFlexibility, fivePercent)

        //to attribute the innovation/originality's points per participant
        for (let i = 0; i < data.length; i++) {

            let participantInnovation = 0;
            originalShapes.forEach(shape => {
                if (chartParticipation[i][1].includes(shape.toString())) {
                    participantInnovation += 1;
                }
            });

            //to display or not the cooperation field int he chart
            if(document.getElementById('display-coop').checked === false) {
                data[i].pop()
                fields[0].splice(4,1)
            }

            //to adjust the data in the chart (remove the entire flexibility table and add the originality/innovation)
            data[i].splice(3, 1, participantInnovation);
        }
    } else {
        if(document.getElementById('display-coop').checked === false) {
            fields[0].splice(4,1)
            data.push(["Ajoutez des participants de la liste !",0,0,0])
        } else {
            data.push(["Ajoutez des participants de la liste !",0,0,0,0])
        }
    }

    document.getElementById('sort-id').checked ? data.sort().reverse().unshift(fields[0]) : data.reverse().unshift(fields[0]);
    return data
}

/**
 * To get the participant selected and to add it in the chart if the button is clicked
 * @param value
 */
function changeSelection(value){

    $("#messages").hide();

    getParticipation(value[0], function (participation){
        addParticipant(participation);

    })
}

/**
 * To calculate information for a participant and send it to the chart when the button is clicked
  * @param participation
 */
function addParticipant(participation)
{
    //when the button add is clicked
    document.getElementById('add-participant').onclick = async function () {

        //to calculate the fluidity and the flexibility for a participant
        var fluidity = fluidityCalculator(participation);
        var flexibility = flexibilityCalculator(fluidity);
        var cooperation = []


        //to verify there is no identical participant already added in the chart and to adapt the idTeam if one or two already exists
        if (chartIdParticipant.includes(participation.participants.idParticipant) || chartIdTeam.includes(participation.idTeam +"-"+ participation.participants.idParticipant)) {

            $("#messages").html("Le participant a déjà été ajouté au graphique").show();

        } else {

            if (participation.idTeam === "ind")
            {
                cooperation = [0,0,0];
            }
            else if (!chartIdTeam.includes(participation.idTeam))
            {
                //to wait for the result
                cooperation = await cooperationCalculator(participation)
            }
            else if (chartIdTeam.includes(participation.idTeam) && !(chartIdTeam.includes(participation.idTeam + "'"))) {
                cooperation = await cooperationCalculator(participation)
                participation.idTeam = participation.idTeam + "'";

            } else if (chartIdTeam.includes(participation.idTeam + "'")) {
                cooperation = await cooperationCalculator(participation)
                participation.idTeam = participation.idTeam + "''";
            }



            //to add the participant in the chart and init the chart

            allParticipation.push([participation.idTeam, fluidity, Object.keys(flexibility).length, 0, cooperation]);
            chartFluidityLength.push(fluidity.length);
            initFeedbackChart();
            chartFluidityActivity.push(fluidityCalculatorActivity(participation))
            chartIdParticipant.push(participation.participants.idParticipant)
            chartIdTeam.push(participation.idTeam)

        }

    };
}

/**
 * To get information of a participant
 * @param id
 * @param callback
 */
function getParticipation(id, callback) {
    $.get(
        '/participation',
        { id : id },
        callback,
        'json'
    );
}

/**
 *
 * @returns {*}
 */
function getFeedbackChartImg()
{
    return feedbackChartImg;
}