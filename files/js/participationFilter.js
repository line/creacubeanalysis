var currentParticipation = new Array();

$("#idTeam-div").hide()

/*
 * Application des filtres sélectionnés sur les participations affichées dans la liste
 */
function modifyListe(chartToInit) {

	currentParticipation = [];

	ageMin = document.getElementById("ageMin").value//parseInt(document.getElementById("ageMin").value,10);
	ageMax = document.getElementById("ageMax").value //parseInt(document.getElementById("ageMax").value, 10);

	bebe = document.getElementById("ageBeb").checked;
	enfant = document.getElementById("ageEnf").checked;
	ado = document.getElementById("ageAdo").checked;
	jadulte = document.getElementById("ageJAd").checked;
	adulte = document.getElementById("ageAdu").checked;
	senior = document.getElementById("ageSen").checked;

	genreF = document.getElementById("genreF").checked;
	genreM = document.getElementById("genreM").checked;

	modaliteI = document.getElementById("modaliteI").checked;
	modaliteC = document.getElementById("modaliteC").checked;
	modaliteB = document.getElementById("modaliteB").checked;
	modaliteT = document.getElementById("modaliteT").checked;

	document.getElementById("modaliteC").checked ? $("#idTeam-div").show(): $("#idTeam-div").hide();

	idTeam = document.getElementById("idTeam").checked

	var idContext = document.getElementById("idContext");

	input = idContext.value;

	callback = function(participations) {
		$("#liste").html("");
		participations.forEach(function(p) {
			$("#liste").append("<option value=\"" + p.participants.idParticipant + "\">" + 
				p.idVideo + " - " + p.participants.idParticipant + " - " +
				p.participants.nameParticipant +  " - " +
				p.idTeam + "</option>");
			currentParticipation.push(p);
		});
		if(chartToInit === "initBehaviorChart") initBehaviorChart()
		if(chartToInit === "initBoxPlotChart") addAll(currentParticipation)
	};

	$.post(
		'/listParticipations/',
		{ filtres:
			{
				ageMin: ageMin,
				ageMax: ageMax,
				bebe: bebe,
				enfant: enfant,
				ado: ado,
				jadulte: jadulte,
				adulte: adulte,
				senior: senior,
				genreF: genreF,
				genreM: genreM,
				modaliteI: modaliteI,
				modaliteC: modaliteC,
				modaliteB: modaliteB,
				modaliteT: modaliteT,
				idContext: input,
				idTeam: idTeam
			}
		},
		callback,
		'json'
	);

	return currentParticipation;
}


