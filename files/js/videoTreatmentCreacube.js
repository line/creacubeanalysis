$(document).ready(()=>{
	$("#eventMap").draggable({handle:  "#eventHeader"})
});

var videoMedia;

var video;

const champs = [
	"nameParticipant",
	"profession",
	"profileTech",
	"diversity",
	"leftHandedO",
	"leftHandedN",
	"cubeletsPriorKnowledge",
	"cubeletsPriorKnowledge",
	"age",
	"ageCategory",
	"genderF",
	"genderM",
	"pOK",
	"pKO",
	"commentaire"
];

/*
 * Récupération des données et affichage de la page
 */
function initTraitVideoCreaCube(v) {
	video = v;

	videoMedia = document.querySelector("video");

	videoMedia.addEventListener("timeupdate", update, false);

	video.participants.forEach(function(p) {
		ajoutEvtsDansListe({ idParticipant: p.idParticipant, clicks: p.clicks });
		activerSaisieMetadata(p.idParticipant, false);

		$("#resultatModifMetadata-" + p.idParticipant).hide();

		$("#modifMeta-" + p.idParticipant).click(function() {
			activerSaisieMetadata(idParticipationCourante(), true);
		});

		$("#validMeta-" + p.idParticipant).click(function() {
			var participationModifiee = validerMetadata(idParticipationCourante());
			saveParticipation(video.idVideo, participationModifiee);
		});

		$("#annulMeta-" + p.idParticipant).click(function() {
			afficherMetadata(getParticipation(idParticipationCourante()));
			activerSaisieMetadata(idParticipationCourante(), false);
		});

		$("#age-" + p.idParticipant).change(function() {
			var category = ageCategory($("#age-" + p.idParticipant).val());
			$("#ageCategory-" + p.idParticipant).val(category);
		});
	});


	$("#eventMap").on("shown.bs.modal", function(e) {
		videoMedia.pause();

		// En cas de sélection d'un événement pour modification, l'attribut data-start de cet
		// événement contient la position dans la vidéo en secondes
		ds = undefined;
		for (i = 0; i < e.relatedTarget.attributes.length; i++) {
			if (e.relatedTarget.attributes[i].name == "data-start") {
				ds = e.relatedTarget.attributes[i].value;
			}
		}
		
		// en cas d'ajout d'un événement l'attribut data-start de ce nouvel événement sera
		// la postion actuelle dans la vidéo arrondi à la seconde
		if (ds == undefined) {
			ds = Math.round(videoMedia.currentTime);
		}

		var tclicks = eventClicks(getParticipation(idParticipationCourante()), ds);
		var nbClicks = 0;
		typesEvents.forEach(function(t) {
			if (tclicks["click" + t] != "") {
				nbClicks++
			}
		});
		if (nbClicks == 0) {
			$("#supprimerEventBtn").hide();
		} else {
			$("#supprimerEventBtn").show();
		}
		$("#confirmerSupprimerEventBtn").hide();

		typesEvents.forEach(function(t) {
			$("#selEvent" + t).val(tclicks["click" + t]);
		});

		$("#posRange").val(ds);

	});


	typesEvents.forEach(function(t) {
		$("#empty" + t).click(function() {
			$("#selEvent" + t).val("");
		});
	});

	$("#enregistrerEventBtn").click(function() {
		var t = $("#posRange").val();
		tclicks = [];
		
		typesEvents.forEach(function(t) {
			var c = $("#selEvent" + t).val();
			if (c != "") {
				tclicks.push(c);
			}
		});
		
		if (tclicks.length === 0) {
			alert("La sélection d'un événement est obligatoire");
		} else {
			enregistrerClicks(video.idVideo, idParticipationCourante(), t, tclicks);
			$("#eventMap").modal('hide');
			seek(t);
		}
	});

	$("#supprimerEventBtn").click(function() {
		$("#supprimerEventBtn").hide();
		$("#confirmerSupprimerEventBtn").show();
	});

	$("#confirmerSupprimerEventBtn").click(function() {

		debugger
		$("#supprimerEventBtn").show();
		$("#confirmerSupprimerEventBtn").hide();

		var t = $("#posRange").val();

		supprimerClicks(video.idVideo, idParticipationCourante(), t);
		$("#eventMap").modal('hide');
		seek(t);
	});

}

/*
 * 
 */
function ajoutEvtsDansListe(params) {

	var idParticipant = params.idParticipant;
	var clicks = params.clicks;
	$("#listeEvents-" + idParticipant).html("");
	clicks.forEach(function(c) {
		cl = "";
		if (c.time == Math.round(videoMedia.currentTime)) {
			cl = " active";
		}
		$("#listeEvents-" + idParticipant).append(
				"<a class=\"list-group-item list-group-item-action" + cl + 
				"\" data-start=\"" + c.time + "\" href=\"#\" " +
				"data-toggle=\"modal\" data-target=\"#eventMap\">" +
				"<span class=\"eventTime badge badge-light\">" + c.time + "</span> " +
				"<span class=\"eventDetail\">" + c.tclicks + "</span>" +
				"</a>");
	});
}

// -------------------------------------------------------
// Fonctions de synchronisation de la vidéo avec les clics
// -------------------------------------------------------

/*
 * 
 */
function update() {
	var ct = Math.round(videoMedia.currentTime);
	$("#now").text(ct);
	setActif(ct);
}

/*
 * 
 */
function seek(e) {
	videoMedia.currentTime = e;
}

/*
 * 
 */
function setActif(ct) {
	var idParticipant = idParticipationCourante();
	var pe = procheEvent(idParticipant, ct);
	var actif = $("[data-start=\"" + pe + "\"]").addClass(function () {
		$("[data-start]").removeClass("active");
		return "active";
	});

	$("#listeEvents-" + idParticipant).scroll(actif);

}

function procheEvent(idP, ct) {
	var pe = -1;
	var participation = getParticipation(idP);
	for (c in participation.clicks) {
		if (participation.clicks[c].time <= ct) {
			pe = participation.clicks[c].time;
		} else {
			break;
		}
	}
	return pe;
}

//----------------------------------
//Fonctions pour la saisie des clics
//----------------------------------

function getShapeFamily(family,callback)
{

	$.get(
		'/getShapeFamily',
		{family : family},
		callback,
		'json'
	);
}

function waitForShapeFamily(family)
{
	return new Promise((resolve => {
		getShapeFamily(family, function(shape){
			resolve(shape)
		})
	}))
}

async function addclick(c) {

	if (document.getElementById("areaEvent").children.length > 2)
	{
		document.getElementById("areaEvent").lastChild.remove()
		document.getElementById("areaEvent").lastChild.remove()
	}

	let buttonT = $('#trueF')
	let buttonF = $('#falseF')

	if (typeEvent(c) === "F" && c !== "FXX" && c !== "FX1")
	{


		let shape = await waitForShapeFamily(c)

		let select = document.createElement("SELECT")
		select.id = "selectCombination"

		for (const combination of shape.combinations)
		{
			let option = document.createElement("OPTION")
			option.setAttribute("data-img_src","/images/combinations/"+ shape.family +"-"+ combination.order +".jpg")
			let textOption = document.createTextNode(combination.order)
			option.appendChild(textOption)
			select.appendChild(option)
			document.getElementById("areaEvent").appendChild(select)
		}

		function custom_template(obj){
			var data = $(obj.element).data();
			var text = $(obj.element).text();
			if(data && data['img_src']){
				img_src = data['img_src'];
				template = $("<div style=\"width:200px;height:85px;\"><img src=\"" + img_src + "\" style=\"width:200px;\" alt=\"" + text + "\" /><p style=\"font-weight: 0;font-size:0pt;text-align:center; visibility: hidden;\">" + text + "</p></div>");
				
				
				return template;
			}
		}
		var options = {
			'templateSelection': custom_template,
			'templateResult': custom_template,
		}
		$('#selectCombination').select2(options);
		$('.select2-container--default .select2-selection--single ').css({'height': '80px'});

		$("#selectCombination").change(function() {

			if($(this).val().length === 4)
			{
				$("#selEvent" + typeEvent(c)).val(shape.family +"-"+ $(this).val() + "-"+ "F")
				buttonT.hide()
				buttonF.show()
				$("#emptyF").hide()
				$("#functionalOrNot").hide();
			} else {
				$("#selEvent" + typeEvent(c)).val(shape.family )
				buttonT.hide()
				buttonF.hide()
				$("#emptyF").show()
				$("#functionalOrNot").show();
			}

		});

		//$("#selEvent" + typeEvent(c)).val(shape.family +"-"+ shape.combinations[0].order +"-"+ "F")
		$("#selEvent" + typeEvent(c)).val(shape.family )
		/*buttonT.hide()
		buttonF.show()
		$("#emptyF").hide()*/
		buttonT.hide()
		buttonF.hide()
		$("#emptyF").show()
		
		document.getElementById('trueF').addEventListener('click', function(event){
			buttonF.show()
			buttonT.hide()

			if($("#selEvent" + typeEvent(c)).val().length <= 6)
			{
				$("#selEvent" + typeEvent(c)).val($("#selEvent" + typeEvent(c)).val().substr(0,5) + "F")
			} else{
				$("#selEvent" + typeEvent(c)).val($("#selEvent" + typeEvent(c)).val().substr(0,10) + "F")
			}

		})
		document.getElementById('falseF').addEventListener('click', function(event){
			buttonT.show()
			buttonF.hide()

			if($("#selEvent" + typeEvent(c)).val().length <= 6)
			{
				$("#selEvent" + typeEvent(c)).val($("#selEvent" + typeEvent(c)).val().substr(0,5) + "T")
			} else{
				$("#selEvent" + typeEvent(c)).val($("#selEvent" + typeEvent(c)).val().substr(0,10) + "T")
			}
		})

			$("#functionalOrNot").show()
			document.getElementById('functionalOrNot').addEventListener('click', function(event) {

				$("#selEvent" + typeEvent(c)).val(shape.family + "-"+ "F")
				buttonT.hide()
				buttonF.show()
				$("#emptyF").hide()

			});


	} else {
		buttonT.hide();
		buttonF.hide();
		$("#emptyF").show();
		$("#selEvent" + typeEvent(c)).val(c);
		$("#functionalOrNot").hide();
	}
}

function copyEvents(video, participation)
{
	let divs = document.getElementsByClassName("copy-paste-buttons")

	for (let i=0; i< divs.length; i++){

		if (divs[i].children.length === 2)
		{
			divs[i].lastChild.remove()
		}
		if (divs[i].children.length < 2)
		{
			let button = document.createElement("BUTTON")
			button.className = "btn btn-primary"
			button.id ="paste"
			let textButton = document.createTextNode("Coller")
			button.appendChild(textButton)
			divs[i].appendChild(button)
			console.log(participation)
			console.log(button)
			console.log(document.getElementById("copy"))

			button.addEventListener("click",function () {
				paste(video.idVideo, participation.idParticipant, video.participants[i].idParticipant )
			})
		}
	}
}

function paste(idVideo, idParticipantCopied, idParticipant)
{
	let divs = document.getElementsByClassName("copy-paste-buttons")

	for (let i=0; i< divs.length; i++) {

		if (divs[i].children.length === 2) {
			divs[i].lastChild.remove()
		}
	}
	var clicksCopied = getParticipation(idParticipantCopied).clicks;

	var participationWanted = getParticipation(idParticipant)

	let intermediateClicks = []

	clicksCopied.forEach(c =>{
		let intermediate = {};
		Object.assign(intermediate,c);
		intermediateClicks.push(intermediate)
	}
	)
	participationWanted.clicks = intermediateClicks

	saveClicks(idVideo, idParticipant, participationWanted.clicks);

}

function eventClicks(participation, t) {

	tclicks = [];
	typesEvents.forEach(function(typ) {
		tclicks["click" + typ] = "";
	});

	participation.clicks.forEach(function(k) {
		if (k.time == t) {
			k.tclicks.forEach(function(c) {
				var typ = typeEvent(c);
				tclicks["click" + typ] = c;
			});
		}
	});

	return tclicks;
}

function enregistrerClicks(idVideo, idParticipant, t, tclicks) {

	var participation = getParticipation(idParticipant);
	var trouve = false;
	participation.clicks.forEach(function(c) {
		if (c.time == t) {
			c.tclicks = tclicks;
			trouve = true;
		}
	});
	if (!trouve) {
		participation.clicks.push({ "time" : +t, "tclicks" : tclicks });
		participation.clicks.sort(function(a, b) {
			return a.time - b.time;
		});
	}
	saveClicks(idVideo, idParticipant, participation.clicks);
}

function supprimerClicks(idVideo, idParticipant, t) {
	var participation = getParticipation(idParticipant);

	ind = -1;
	for (i = 0; i < participation.clicks.length; i++) {
		if (participation.clicks[i].time == t) {
			ind = i;
		}
	}
	if (ind != -1) {
		participation.clicks.splice(ind, 1);
		saveClicks(idVideo, idParticipant, participation.clicks);
	}
}

function saveClicks(idVideo, idParticipant, clicks) {
	$.post(
		'/saveClicks/',
		{
			idVideo: idVideo,
			idParticipant: idParticipant,
			clicks: clicks
		},
		ajoutEvtsDansListe,
		'json'
	);

}

// -------------------------------------
// Fonctions pour la saisie des metadata
// -------------------------------------
function afficherMetadata(participation) {
	$("#nameParticipant-" + participation.idParticipant).val(participation.nameParticipant);
	$("#profession-" + participation.idParticipant).val(participation.profession);
	$("#profileTech-" + participation.idParticipant).val(participation.profileTech);
	$("#diversity-" + participation.idParticipant).val(participation.diversity);
	if (participation.leftHanded) {
		$("#leftHandedO-" + participation.idParticipant).prop("checked", true);
	} else {
		$("#leftHandedN-" + participation.idParticipant).prop("checked", true);
	}
	$("#age-" + participation.idParticipant).val(participation.age);
	$("#ageCategory-" + participation.idParticipant).val(participation.ageCategory);
	$("#genderF-" + participation.idParticipant).prop("checked", false);
	$("#genderM-" + participation.idParticipant).prop("checked", false);
	if (participation.gender == "F") {
		$("#genderF-" + participation.idParticipant).prop("checked", true);
	}
	if (participation.gender == "M") {
		$("#genderM-" + participation.idParticipant).prop("checked", true);
	}
	if (participation.participationOK) {
		$("#pOK-" + participation.idParticipant).prop("checked", true);
	}
	if (participation.participationOK == false) {
		$("#pKO-" + participation.idParticipant).prop("checked", true);
	}
	$("#commentaire-" + participation.idParticipant).val(participation.commentaire);
}

function activerSaisieMetadata(idParticipant, activer) {
	for (c in champs) {
		$("#" + champs[c] + "-" + idParticipant).prop("disabled", !activer);
	}
	if (activer) {
		$("#resultatModifMetadata-" + idParticipant).text("");
		$("#resultatModifMetadata-" + idParticipant).hide();
		$("#modifMetadata-" + idParticipant).hide();
		$("#validMetadata-" + idParticipant).show();
	} else {
		$("#modifMetadata-" + idParticipant).show();
		$("#validMetadata-" + idParticipant).hide();
	}
}

function idParticipationCourante() {
	var pCour = $(".nav-link.active");
	if (pCour !== undefined) {
		pCour = pCour.attr("id");
		if (pCour !== undefined) {
			pCour = pCour.split('-')[0];
			return pCour;
		}
	}
	// TODO Erreur pas de participation courante (en théorie, ne peut pas se produire)
}
function getParticipation(idParticipant) {
	participation = {};
	video.participants.forEach(function(p) {{
		if (p.idParticipant == idParticipant) {
			participation = p;
			return;
		}
	}});
	// TODO Erreur participation courante pas dans video (en théorie, ne peut pas se produire)
	return participation;
}

function validerMetadata(idParticipant) {
	// TODO contrôles
	var participationModifiee = {};
	participationModifiee.idParticipant = idParticipant;
	participationModifiee.nameParticipant =
		$("#nameParticipant-" + idParticipant).val();
	participationModifiee.profession =
		$("#profession-" + idParticipant).val();
	participationModifiee.profileTech =
		$("#profileTech-" + idParticipant).val();
	participationModifiee.diversity =
		$("#diversity-" + idParticipant).val();
	if ($("#leftHandedO-" + idParticipant).prop("checked")) {
		participationModifiee.leftHanded = true;
	}
	if ($("#leftHandedN-" + idParticipant).prop("checked")) {
		participationModifiee.leftHanded = false;
	}
	participationModifiee.cubeletPriorKnowledge =
		$("#cubeletPriorKnowledge-" + idParticipant).val();
	participationModifiee.age =
		parseInt($("#age-" + idParticipant).val(),10);
	participationModifiee.ageCategory =
		$("#ageCategory-" + idParticipant).val();
	if ($("#genderF-" + idParticipant).prop("checked")) {
		participationModifiee.gender = "F";
	}
	if ($("#genderM-" + idParticipant).prop("checked")) {
		participationModifiee.gender = "M";
	}
	if ($("#pOK-" + idParticipant).prop("checked")) {
		participationModifiee.participationOK = true;
	}
	if ($("#pKO-" + idParticipant).prop("checked")) {
		participationModifiee.participationOK = false;
	}
	participationModifiee.commentaire =
		$("#commentaire-" + idParticipant).val();
	participationModifiee.clicks = getParticipation(idParticipant).clicks;
	return participationModifiee;
}

function saveParticipation(idVideo, participation) {
	callback = function(res) {
		// TODO message à adapter selon res
		$("#resultatModifMetadata-" + participation.idParticipant).text("Modification effectuée");
		$("#resultatModifMetadata-" + participation.idParticipant).show();
		activerSaisieMetadata(participation.idParticipant, false);
	};
	$.post(
		'/saveParticipation/',
		{
			idVideo: idVideo,
			participation: JSON.stringify(participation)
		},
		callback,
		'text'
	);
	
}
