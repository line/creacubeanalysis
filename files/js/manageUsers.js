function deleteUser(user)
{

        $.post(
            '/deleteUser/',
            {
                userId: user.userId,
            }
        );
    location.reload()
}

function addUser()
{

    let newUser={}
    newUser.userId = document.getElementById('id').value
    newUser.name = document.getElementById('name').value
    newUser.firstName = document.getElementById('firstName').value

        $.post(
            '/addUser/',
            {
                user: newUser
            }
        );

    location.reload()
}