


function initBehaviorStatistics()
{

    let perseveranceTime = []

    currentParticipation.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation)) )
    calculateGlobalStatistic('none', removeNull(perseveranceTime))

    calculateGlobalStatistic('conservative', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Conservative')))

    calculateGlobalStatistic('creative', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Creative')))

    calculateGlobalStatistic('ni_conservative', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Unintentional Conservative')))

    calculateGlobalStatistic('ni_creative', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Unintentional Creative')))


    getFilteredData({genreF: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('femme', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('femme_p', removeNull(perseveranceTime))
    })



    getFilteredData({genreM: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('homme', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('homme_p', removeNull(perseveranceTime))
    })

    getFilteredData({bebe: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('beb', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('beb_p', removeNull(perseveranceTime))
    })

    getFilteredData({enfant: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('enf', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('enf_p', removeNull(perseveranceTime))
    })

    getFilteredData({ado: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('ado', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('ado_p', removeNull(perseveranceTime))
    })

    getFilteredData({jadulte: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('jad', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('jad_p', removeNull(perseveranceTime))
    })

    getFilteredData({adulte: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('adu', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('adu_p', removeNull(perseveranceTime))
    })

    getFilteredData({senior: true, modaliteI: true}).then(function (participations){

        let perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation))  )
        calculateGlobalStatistic('sen', removeNull(perseveranceTime))

        perseveranceTime = []
        participations.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation, "Profile", profileCalculator(participation)))  )
        calculateGlobalStatistic('sen_p', removeNull(perseveranceTime))
    })



    perseveranceTime = []
    currentParticipation.forEach(participation => perseveranceTime.push(getPerseveranceTime(participation,"Profile", profileCalculator(participation))))
    calculateGlobalStatistic('none_p', removeNull(perseveranceTime))

    calculateGlobalStatistic('conservative_p', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Conservative', "Profile")))

    calculateGlobalStatistic('creative_p', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Creative', "Profile")))

    calculateGlobalStatistic('ni_conservative_p', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Unintentional Conservative', "Profile")))

    calculateGlobalStatistic('ni_creative_p', removeNull(filterProfileWithPerseveranceTime(currentParticipation, 'Unintentional Creative', "Profile")))

}


function getFilteredData(filter)
{

    return $.post(
        '/listParticipations/',
        { filtres: filter
        },
        'json'
    );
}

function filterProfileWithPerseveranceTime(participations, wantedProfile, perseveranceType)
{
    let filteredProfile = []

    participations.forEach(participation => {
        let profile = profileCalculator(participation)
        if (profile === wantedProfile) {


            if(perseveranceType === "Profile")
            {
                filteredProfile.push(getPerseveranceTime(participation, perseveranceType, profile))
            } else filteredProfile.push(getPerseveranceTime(participation))
        }
    })

    return filteredProfile
}

function removeNull(data)
{
    return data.filter(element => {return element !== null})

}