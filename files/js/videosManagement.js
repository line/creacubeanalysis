$("#wait").hide()

function initGestionVideos() {
	ajoutOptionsDansListes();

	$(".modifVideo").click(function(e) {
		idVideo = ""
		for (i = 0; i < e.currentTarget.attributes.length; i++) {
			if (e.currentTarget.attributes[i].name == "id") {
				idVideo = e.currentTarget.attributes[i].value.substring(3);
			}
		}
	});

	putLastId("idVideo", function (lastId){
		lastId = parseInt(lastId.substr(1,4))
		lastId++
		$("#idVideo_new").val(createId(lastId))
	})


	$("#newVideo").on("show.bs.modal", function(e) {
		$("#messages").hide();
		$("#done").hide();
		$("#fermer").hide();
	});

	$("#idTeamBin").prop("disabled", true);
	$("#idTeamTri").prop("disabled", true);
	$("#idTeamHistory").prop("disabled", true);
	$("#idTeamCloserness").prop("disabled", true);
	$("#idParticipant2").prop("disabled", true);
	$("#idParticipant3").prop("disabled", true);


	$("#nbP").on("change", function() {
		if ($("#nbParticipants1").prop("checked")) {
			$("#idTeamBin").prop("disabled", true);
			$("#idTeamBin").val("")
			$("#idTeamTri").prop("disabled", true);
			$("#idTeamTri").val("");
			$("#idTeamHistory").prop("disabled", true);
			$("#idTeamCloserness").prop("disabled", true);
			$("#idParticipant2").prop("disabled", true);
			$("#idParticipant3").prop("disabled", true);
			$("#idParticipant2").val("");
			$("#idParticipant3").val("");

			putLastId("idParticipant", function (lastId){
				lastId = parseInt(lastId.substr(1,4))
				lastId++
				$("#idParticipant1").val(createId(lastId))
			})
		}
		if ($("#nbParticipants2").prop("checked")) {
			$("#idTeamBin").prop("disabled", false);
			$("#idTeamTri").prop("disabled", true);
			$("#idTeamTri").val("");
			$("#idTeamHistory").prop("disabled", false);
			$("#idTeamCloserness").prop("disabled", false);
			$("#idParticipant2").prop("disabled", false);
			$("#idParticipant3").prop("disabled", true);
			$("#idParticipant3").val("");

			putLastIdTeam("2", function (lastId){
				lastId = parseInt(lastId.substr(3,4))
				lastId++
				$("#idTeamBin").val(createId(lastId))
			})

			putLastId("idParticipant", function (lastId){
				lastId = parseInt(lastId.substr(1,4))
				lastId++
				$("#idParticipant1").val(createId(lastId))
				lastId++
				$("#idParticipant2").val(createId(lastId))
			})
		}
		if ($("#nbParticipants3").prop("checked")) {
			$("#idTeamBin").prop("disabled", true);
			$("#idTeamBin").val("")
			$("#idTeamTri").prop("disabled", false);
			$("#idTeamHistory").prop("disabled", false);
			$("#idTeamCloserness").prop("disabled", false);
			$("#idParticipant2").prop("disabled", false);
			$("#idParticipant3").prop("disabled", false);

			putLastIdTeam("3", function (lastId){
				lastId = parseInt(lastId.substr(3,4))
				lastId++
				$("#idTeamTri").val(createId(lastId))
			})

			putLastId("idParticipant", function (lastId){
				lastId = parseInt(lastId.substr(1,4))
				lastId++
				$("#idParticipant1").val(createId(lastId))
				lastId++
				$("#idParticipant2").val(createId(lastId))
				lastId++
				$("#idParticipant3").val(createId(lastId))
			})
		}
	});

	$("#ajoutVideoBtn").click(function(e) {
		$("#newVideoForm").submit();
	});

	$("#newVideoForm").on('submit', function(event){
		event.preventDefault();
		let erreurs = [];
		if ($("#idVideo").val() == "") {
			erreurs.push("Vous devez saisir un identifiant pour la vidéo");
		} else {
			if (isNaN($("#idVideo").val())) {
				erreurs.push("L'identifiant de la vidéo doit être composé de 1 à 4 chiffres");
			}	
		}
		if ($('input[name=nbParticipants]:checked').val() === undefined) {
			erreurs.push("Vous devez sélectionner le nombre de participants");
		} else {
			if ($("#idParticipant1").val() == "") {
				erreurs.push("Vous devez saisir un identifiant pour le premier participant");
			} else {
				if (isNaN($("#idParticipant1").val())) {
					erreurs.push("L'identifiant du premier participant doit être composé de 1 à 4 chiffres");
				}	
			}
			if ($('input[name=nbParticipants]:checked').val() == '2' ||
					$('input[name=nbParticipants]:checked').val() == '3') {
				if ($("#idParticipant2").val() == "") {
					erreurs.push("Vous devez saisir un identifiant pour le deuxième participant");
				} else {
					if (isNaN($("#idParticipant2").val())) {
						erreurs.push("L'identifiant du deuxième participant doit être composé de 1 à 4 chiffres");
					}	
				}
			}
			if ($('input[name=nbParticipants]:checked').val() == '2') {
				if ($("#idTeamBin").val() == "") {
					erreurs.push("Vous devez saisir un identifiant pour le binôme");
				} else {
					if (isNaN($("#idTeamBin").val())) {
						erreurs.push("L'identifiant du binôme doit être composé de 1 à 4 chiffres");
					}	
				}
			}
			if ($('input[name=nbParticipants]:checked').val() == '3') {
				if ($("#idParticipant3").val() == "") {
					erreurs.push("Vous devez saisir un identifiant pour le troisième participant");
				} else {
					if (isNaN($("#idParticipant3").val())) {
						erreurs.push("L'identifiant du troisième participant doit être composé de 1 à 4 chiffres");
					}	
				}
				if ($("#idTeamTri").val() == "") {
					erreurs.push("Vous devez saisir un identifiant pour le trinôme");
				} else {
					if (isNaN($("#idTeamTri").val())) {
						erreurs.push("L'identifiant du trinôme doit être composé de 1 à 4 chiffres");
					}	
				}
			}
		}
		if ($("#upload").val() == "") {
			erreurs.push("Vous devez sélectionner un fichier vidéo");
		}
		if (erreurs.length > 0) {
			event.stopPropagation();
			let messages = "";
			for (e in erreurs) {
				messages += erreurs[e] + "<br>";
			}
			$("#messages").html(messages);
			$("#messages").show();
		} else {
			$("#messages").hide();
			$("#wait").show()
			var form_data = new FormData(this);
			$.ajax({
				url: "/addVideo",
				method : "POST",
				processData: false,
				contentType: false,
				data: form_data,
				dataType: "JSON",
			})
			.done(function(json) {
				$("#wait").hide()
				if (json.length == 0) {

					$("#done").html("Ajout de la vidéo réussi !");
					$("#done").show();
					$("#ajouterVideo").hide();
					$("#fermer").show();
				} else {
					let messages = "";
					for (e in json) {
						messages += json[e] + "<br>";
					}
					$("#messages").html(messages);
					$("#messages").show();
				}
			});
		}
	});

}

function ajoutOptionsDansListes() {
	$("#idTeamHistory").append("<option value =\"\" selected>Choisir une option</option>");
	teamHistory.forEach(function(h) {
		$("#idTeamHistory").append("<option value=\"" + h.code + "\">"
				+ h.libelle + "</option>");
	});
	$("#idTeamCloserness").append("<option value =\"\" selected>Choisir une option</option>");
	teamCloserness.forEach(function(c) {
		$("#idTeamCloserness").append("<option value=\"" + c.code + "\">"
				+ c.libelle + "</option>");
	});
}
