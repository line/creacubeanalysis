var axes = [
			{ axis : "Axe01-Instructions", ticks : 7, init: 6,
				titles: [
					"Opposite direction",
					"(default)",
					"Understanding vehicle",
					"Noticing red and black points",
					"Figuring out the autonomous movement",
					"Solving the problem without being sure",
					"Solving the problem being sure the solution was ok"
				]
			},
			{ axis : "Axe02-Figures", ticks : 6, init: 5,
				titles: [
					"Associating cubes by colour (e.g. red cube next to red point)",
					"(default)",
					"Connecting cubes",
					"Creating unsuccessful figure",
					"Creating potential successful figure",
					"Creating a successful figure"
				]
			},
			{ axis : "Axe03-Movement", ticks : 6, init: 6,
				titles: [
					"(default)",
					"Sudden movement",
					"Imbalance mouvement (P01)",
					"Moves non linearly (rotation P02)",
					"Moves linearly (P03, P04, P05)",
					"Moves from red to black"
				]
			},
			{ axis : "Axe04-Cubes", ticks : 5, init: 4,
				titles: [
					"Attribution of movement to external factors (magnetic fields, etc)",
					"(default)",
					"Noticing different colors",
					"Noticing magnetic connection",
					"Connecting four cubes"
				]
			},
			{ axis : "Axe05-DriveCube", ticks : 4, init: 3,
				titles: [
					"Using wheels through friction",
					"(default)",
					"Noticing wheels",
					"Using wheels on the floor properly (motor activated)"
				]
			},
			{ axis : "Axe06-BatteryCube", ticks : 3, init: 3,
				titles: [
					"(default)",
					"Noticing switch button",
					"Switch on button"
				]
			},
			{ axis : "Axe07-DistanceCube", ticks : 6, init: 5,
				titles: [
					"Moving the cubes through activating the sensor",
					"(default)",
					"Noticing two circles",
					"Noticing reaction from two circles",
					"Using distance sensor without understanding its behavior", 
					"Using distance sensor understanding its behavior "
				]
			},
			{ axis : "Axe08-InverseCube", ticks : 5, init: 5,
				titles: [
					"(default)",
					"Noticing no special feature",
					"Noticing the red cube is required for the system to move",
					"Using the red cube without understanding the inverse role",
					"Using the red cube understanding the inverse role"
				]
			}
		];

// Data
var data =
	[
		[ 2, 2, 3, 2, 2, 2, 1, 3 ],
		[ 1, 2, 2, 2, 2, 1, 1, 3 ],
		[ 0, 0, 0, 0, 0, 0, 1, 1 ],
		[ 0, 4, 5, 4, 0, 0, 1, 1 ]
	];

// Call function to draw the Radar chart
RadarChart.draw("#chart", axes, data);

