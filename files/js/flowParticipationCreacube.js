//{% include "participationFlowChart.html" %}

var selected = ["", "", "", ""];
newSelected = ["", "", "", ""];
var participants =[]
const marges = {top : 15, right : 20, bottom : 30, left : 40};
var x;
var y;

var figures = [
	{ "nom": "", "res": "white" },
	{ "nom": "F000", "res": "blue" },
	{ "nom": "F002", "res": "blue" },
	{ "nom": "F010", "res": "darkgreen" },
	{ "nom": "F011", "res": "darkgreen" },
	{ "nom": "F012", "res": "darkgreen" },
	{ "nom": "F013", "res": "darkgreen" },
	{ "nom": "F014", "res": "darkgreen" },
	{ "nom": "F015", "res": "darkgreen" },
	{ "nom": "F020", "res": "darkorange" },
	{ "nom": "F021", "res": "darkorange" },
	{ "nom": "F022", "res": "darkorange" },
	{ "nom": "F024", "res": "darkorange" },
	{ "nom": "F025", "res": "darkorange" },
	{ "nom": "F030", "res": "blueviolet" },
	{ "nom": "F034", "res": "blueviolet" },
	{ "nom": "F040", "res": "red" },
	{ "nom": "F042", "res": "red" },
	{ "nom": "F044", "res": "red" },
	{ "nom": "F045", "res": "red" },
	{ "nom": "F050", "res": "saddlebrown" },
	{ "nom": "F051", "res": "saddlebrown" },
	{ "nom": "F055", "res": "saddlebrown" },
	{ "nom": "F056", "res": "saddlebrown" },
	{ "nom": "F060", "res": "hotpink" },
	{ "nom": "F061", "res": "hotpink" },
	{ "nom": "F064", "res": "hotpink" },
	{ "nom": "F065", "res": "hotpink" },
	{ "nom": "F066", "res": "hotpink" },
	{ "nom": "FX1", "res": "black" },
	{ "nom": "FXX", "res": "black" },
];
var lastValue = 0
/*
 * Affichage initial de la page
 */
function initGraphiquesCreaCube() {
	// affichage des axes et des lignes verticales et horizontales
	drawStructure("#svg1", 1);
	drawStructure("#svg2", 2);
}

/* 
 * Changement de sélection
 */
function changementSelection(val) {
		modifySelection(val);
		displayParticipations();
		selected = newSelected;
}

/*
 * Affichage du cadre pour le graphe d'une activité
 */
function drawStructure(idSvg, activite) {


	// réinitialisation à vide du graphique
	width = Math.round($("#graphique").width()) - marges.left - marges.right;
	height = 261
	svg = d3.select(idSvg)
		.attr("width", width + marges.left + marges.right)
		.attr("height", height + marges.top + marges.bottom)
		.attr("transform", "translate(" + marges.left + "," + marges.top + ")");

///////////////////////////////////////////////////////////////////////
	//TO DO change the min and max x axe
///////////////////////////////////////////////////////////////////////
	x = d3.scale.linear()
		.domain(d3.extent([0, lastValue]))
		.range([marges.left, width]);

	xAxis = make_x_axis(x);

	y = d3.scale.linear()
		.domain([0,30])
		.range([height, marges.top]);

	yAxis = make_y_axis(y);

	// lignes verticales
	svg.append("g")
		.attr("class", "grid")
		.attr("transform", "translate(0," + height + ")")
		.call(make_x_axis(x).tickSize(-height, 0, 0).tickFormat(""));

	// axe des x
	svg.append("g")
		.attr("class","x axis")
		.attr("transform","translate(0," + height + ")")
		.call(xAxis)
		.append("text")
		.attr("x", width / 2)
		.attr("y", 20)
		.attr("dy","1.5em").
	style("text-anchor","end")
		.text("Temps (en secondes)");


	// lignes horizontales
	svg.append("g")
		.attr("class","grid")
		.attr("transform", "translate(" + marges.left + ",0)")
		.call(make_y_axis(y).tickSize(-(width - marges.left), 0, 0).tickFormat(""));

	// axe des y
	svg.append("g")
		.attr("class","y axis")
		.attr("transform","translate(" + marges.left + ",0)")
		.call(yAxis)
		.append("text")
		.attr("transform","rotate(-90)")
		.attr("y", -marges.left)
		.attr("dy",".71em")
		.style("text-anchor","end")
		.text("Figures Activité " + activite);
}

/*
 * Informations pour la construction de l'axe des x
 */
function make_x_axis(a) {
	return d3.svg.axis()
		.scale(a)
		.orient("bottom")
		.ticks(18)
}

/*
 * Informations pour la construction de l'axe des y
 */
function make_y_axis(a) {
	return d3.svg.axis()
		.scale(a)
		.orient("left")
		.ticks(20)
}


/*
 * Affichage de la courbe pour une participation et une activité
 */
function drawCourbe(idParticipant, activite, clicks, couleur) {
	col = "col" + couleur;

	var valueLine = d3.svg.line()
		.x(function(d) { return x(d.time); })
		.y(function(d) { return y(d.click); });

	g = d3.select("#svg" + activite)
		.append("g")
		.attr("id", "line" + activite + idParticipant)
		.attr("class", "line " + col);

	g.append("path")
		.attr("d", valueLine(clicks));


	g.selectAll("point")
		.data(clicks)
		.enter()
		.append("svg:circle")
		.attr("class", "point")
		.style("opacity", 1)
		.style("stroke", function(a) { return figures[a.click]["res"] })
		.attr("cx", function(a) { return x(a.time) })
		.attr("cy", function(a) { return y(a.click) })
		.attr("r", 4);

}

function modifySelection(selection) {
	newSelected = ["", "", "", ""];

	// Annulation des sélections au-delà de la quatrième
	if (selection.length > 4) {
		options = document.getElementById("liste").children;
		for (i = 4; i < selection.length; i++) {
			for (j = 0; j < options.length; j++) {
				if (j.value == selection[i]) {
					j.selected = false;
				}
			}
		}
	}

	// Reconduction des anciennes sélections
	// (pour conserver le rang dans la sélection donc la couleur de la courbe)
	for (i = 0; i < selection.length; i++) {
		for (j = 0; j < 4; j++) {
			if (selection[i] == selected[j]) {
				newSelected[j] = selected[j];
				break;
			}
		}
	}

	// Ajout des nouvelles sélections dans les emplacements disponibles
	for (i = 0; i < selection.length; i++) {
		fait = false;
		for (j = 0; j < 4; j++) {
			if (selection[i] == selected[j]) {
				fait = true;
				break;
			}
		}
		if (!fait) {
			for (j = 0; j < 4; j++) {
				if (newSelected[j] == "") {
					newSelected[j] = selection[i];
					break;
				}
			}
		}
	}
}

function displayParticipations() {
	// Affichage courbes et details de la sélection
	for (var i = 0; i < 4; i++) {
		var couleur = i + 1;
		if (selected[i] != "" && newSelected[i] != selected[i]) {
			// Suppression courbes et détails
			$("#line1" + selected[i]).remove();
			$("#line2" + selected[i]).remove();
			$("#details" + couleur + "h").html("");
			$("#details" + couleur + "b").html("");
		}
		if (newSelected[i] != "" && newSelected[i] != selected[i]) {
			displayParticipation(newSelected[i], couleur);
		}
	}
}

function displayParticipation(idParticipant, couleur) {
	getParticipation(idParticipant, function(participation) {
		var clicks1 = new Array();
		var clicks2 = new Array();
		var clicks3 = new Array();
		if (participation.participants.clicks.length > 0) {
			var idParticipant = participation.participants.idParticipant;
			var activite = 1;
			var t0 = -1;
			var ok;
			for (k in participation.participants.clicks) {
				for (cl of participation.participants.clicks[k].tclicks) {
					if (cl == "START" || cl == "START1") {
						t0 = participation.participants.clicks[k].time;
						clicks1.push({ "time" : 0, "click" : 0 });
					}
					var t = participation.participants.clicks[k].time - t0;
					if (cl == "START2" || cl[0] == "T" || (cl[0] == "F"&& cl[1] != "L")) {
						var f = 0;

						if (cl[0] === "F" && cl[1] !== "L") {
							f = cl.substring(1,4)
							f = converterForChart(f)
						} else if (cl[0] == "T" || cl == "START2") {
							activite++;
						}
						if (activite == 1) {
							clicks1.push({ "time" : t, "click" : f });
						} else if (activite == 2) {
							clicks2.push({ "time" : t, "click" : f });
						} else if (activite == 3) {
							clicks3.push({ "time" : t, "click" : f });
						}


					}
				}
			}

			if(clicks3.length === 0){
				let newLastValue = clicks2[clicks2.length -1].time
				if(lastValue < newLastValue)
				{
					lastValue = newLastValue
					d3.selectAll("g > *").remove()

					initGraphiquesCreaCube()
					drawCourbe(participation.participants.idParticipant, 1, clicks1, couleur);
					drawCourbe(participation.participants.idParticipant, 2, clicks3, couleur);
				} else {
					drawCourbe(participation.participants.idParticipant, 1, clicks1, couleur);
					drawCourbe(participation.participants.idParticipant, 2, clicks2, couleur);
				}

			} else {
				clicks3 = clicks2.concat(clicks3)

				let newLastValue = clicks3[clicks3.length -1].time
				if(lastValue < newLastValue)
				{
					lastValue = newLastValue
					d3.selectAll("g > *").remove()

					initGraphiquesCreaCube()
					drawCourbe(participation.participants.idParticipant, 1, clicks1, couleur);
					drawCourbe(participation.participants.idParticipant, 2, clicks3, couleur);
				} else {
					drawCourbe(participation.participants.idParticipant, 1, clicks1, couleur);
					drawCourbe(participation.participants.idParticipant, 2, clicks3, couleur);
				}

			}
		}

		displayDetails(participation, couleur);
	});
}


/*
 * Affichage des informations essentielles d'une participation dans une vignette 
 * encadrée de la même couleur que les deux courbes associées
 */
function displayDetails(participation, couleur) {
	$("#details" + couleur + "h").html("<h4>" + participation.participants.idParticipant + " - "
			+ participation.participants.nameParticipant + "<h4>");
	$("#details" + couleur + "b").html(
		"<p><span class=\"title\">Date: </span>" + participation.dateJMA + "<br>" +
		"<span class=\"title\">Genre: </span>" + participation.participants.gender + "<br>" +
		"<span class=\"title\">Age: </span>" + participation.participants.age + "<br>" +
		"<span class=\"title\">Nb participants: </span>" + participation.nbParticipants + "</p>"
	);
}

function getParticipation(id, callback) {
	$.get(
		'/participation',
		{ id : id },
		callback,
		'json'
		);
}

function putShapeFamilyColor(){

	for (let i =1; i<figures.length-2; i++)
	{
		let td = document.getElementById(figures[i].nom)
		console.log(td)
		td.bgColor = figures[i].res
	}

}