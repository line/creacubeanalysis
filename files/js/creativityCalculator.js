//the count of made shape per participant
var allFluidityCount = new Array();
//the count of different shape made per participant
var allFlexibilityCount = new Array();
//the count of originality/innovation per participant
var allOriginalityCount = new Array();
//the count of made shape per participant in A1
var A1FluidityCount = new Array()
//the count of different shape made per participant in A1
var A1FlexibilityCount = new Array();
//the count of originality/innovation per participant in A1
var A1OriginalityCount = new Array();
//the count of made shape per participant in A2
var A2FluidityCount = new Array()
//the count of different shape made per participant in A2
var A2FlexibilityCount = new Array();
//the count of originality/innovation per participant in A2
var A2OriginalityCount = new Array();
//the count of cooperation point per participant
var allCooperation = new Array()
//all the idParticipant added
var idParticipant = new Array();
//all the participant in the db
var Participation = new Array();

var ShapesByFamilyCount = new Array()
var A1ShapesByFamilyCount = new Array()
var A2ShapesByFamilyCount = new Array()

var OriginalShapes = new Array();

/**
 * To calculate the fluidity of a participant
 * @param participation
 * @returns {fluidity[]}
 */
function fluidityCalculator(participation)
{
    let fluidity = [];


    if(participation.participants.clicks === null)
    {
        console.log(participation.participants.idParticipant)
    }


    if (participation.participants.clicks.length > 0) {
        for (k in participation.participants.clicks) {
            for (cl of participation.participants.clicks[k].tclicks) {
                if (cl === "FXX")
                {
                    fluidity.push("XX")
                }
                else if (cl === "FX1")
                {
                    fluidity.push("X1")
                }
                else if ((cl[0] === "F" && cl[1] !== "L" && cl.length < 4)) {

                    let f = 0;
                    f = cl.substr(1,3);
                    fluidity.push(f);
                }
                 else if (cl[0] === "F" && cl[1] !== "L")
                {
                    let f = 0;
                    f = cl.substring(1,4)
                    fluidity.push(f)
                }
            }
        }
    }

    return fluidity;
}

/**
 * To calculate the fluidity of a participant
 * @param participation
 * @returns {any[][]}
 */
function fluidityTimeCalculator(participation)
{
    let fluidity = new Array();
    let time = new Array();


    if (participation.participants.clicks.length > 0) {
        for (k in participation.participants.clicks) {
            for (cl of participation.participants.clicks[k].tclicks) {
                var t = (participation.participants.clicks[k].time).toString();
                if (cl[0] == "T" || (cl[0] == "F" && cl[1] != "L")) {
                    var f = 0;

                    if (cl === "FXX")
                    {
                        time.push(t)
                        fluidity.push("XX")
                    }
                    else if (cl === "FX1")
                    {
                        time.push(t)
                        fluidity.push("X1")
                    }
                    else if ((cl[0] === "F" && cl[1] !== "L" && cl.length < 4)) {

                        time.push(t)
                        f = cl.substr(1,3);
                        fluidity.push(f);
                    }
                    else if (cl[0] === "F" && cl[1] !== "L")
                    {

                        time.push(t)
                        f = cl.substring(1,4)
                        fluidity.push(f)
                    }

                    else if (cl[0] == "T" && cl[1] == "0" && cl[2] == "3") {
                        time.push(t)
                        fluidity.push("T03");
                    }
                }
            }
        }
    }

    return [fluidity, time];
}

function fluidityCalculatorActivity(participation)
{
    let fluidity = new Array();


    if (participation.participants.clicks.length > 0) {
        for (k in participation.participants.clicks) {
            for (cl of participation.participants.clicks[k].tclicks) {
                if (cl[0] == "T" || (cl[0] == "F" && cl[1] != "L")) {
                    var f = 0;

                    if (cl === "FXX")
                    {
                        fluidity.push("XX")
                    }
                    else if (cl === "FX1")
                    {
                        fluidity.push("X1")
                    }
                    else if ((cl[0] === "F" && cl[1] !== "L" && cl.length < 4)) {

                        f = cl.substr(1,3);
                        fluidity.push(f);
                    }
                     else if (cl[0] === "F" && cl[1] !== "L")
                    {

                        f = cl.substring(1,4)
                        fluidity.push(f)
                    }

                     else if (cl[0] === "T" && cl[1] === "0" && cl[2] === "3") {
                        fluidity.push("T03");
                    }
                     else if (cl[0] === "T" && cl[1] === "0" && cl[2] === "2"){
                        fluidity.push("T02");
                        break;
                    }
                }
            }
        }
    }

    return fluidity;
}

function wantedActivityFluidityCalculator(fluidity, wantedActivity)
{
    let wantedActivityFluidity = []

    if (wantedActivity === 1) {
        for(let i = 0; i<fluidity.length; i++)
        {
            if (fluidity[i] === "T03") {
                break
            }
            wantedActivityFluidity.push(fluidity[i])
        }
    } else if (wantedActivity === 2) {

        let beginActivity = (fluidity.indexOf("T03")) + 1

        for (let i = beginActivity; i<fluidity.length; i++)
        {
            if (fluidity[i] === "T03") {
                break
            }
            wantedActivityFluidity.push(fluidity[i])
        }
    } else {
        wantedActivityFluidity = fluidity
    }

    return wantedActivityFluidity;
}

/**
 * To calculate the flexibility for a participant
 * @param fluidity
 * @returns {{}}
 */
function flexibilityCalculator(fluidity)
{
    var flexibility = {};
    for (var i = 0; i < fluidity.length; i++) {
        var num = fluidity[i];

        flexibility[num] = flexibility[num] ? flexibility[num] + 1 : 1;
    }
    return flexibility;
}

/**
 * To calculate the innovative/original shape(s) in a list of made shapes
 * @param allFlexibility
 * @param fivePercent
 * @returns {[]}
 */
function originalShapesCalculator(allFlexibility, fivePercent)
{
    let originalShapes = [];

    for (let [index, element] of Object.entries(allFlexibility)) {
        if (element <= fivePercent) {
            originalShapes.push(index);
        }
    }

    return originalShapes;
}


function activityCalculator(participations, wantedActivity)
{
    var FluidityCount = new Array();
    var FlexibilityCount = new Array();
    var OriginalityCount = new Array();

    //list of the name of mad shape per participant
    var allFluidityShapePerParticipant = new Array();
    //list of the name of mad shape per participant in A1
    var activityFluidityShapePerParticipant = new Array();
    //list of the name of mad shape bor all the group
    var activityFluidity = new Array();
    //the name and the occurrence of different shape made by all the group
    var activityFlexibility = new Array();
    //the name and the occurrence of different shape made per participant
    var allFlexibilityPerParticipant = new Array();
    //the name and the occurrence of different shape made per participant
    var activityFlexibilityPerParticipant = new Array();

    //to calculate the fluidity per participant
    participations.forEach(participation => idParticipant.push(participation.participants.idParticipant))
    participations.forEach(participation => allFluidityShapePerParticipant.push(fluidityCalculatorActivity(participation)));

    allFluidityShapePerParticipant.forEach(fluidity => activityFluidityShapePerParticipant.push(wantedActivityFluidityCalculator(fluidity,wantedActivity)))

    //to count the fluidity per participant
    activityFluidityShapePerParticipant.forEach(fluidity => FluidityCount.push(fluidity.length))

    //to calculate the flexibility per participant
    allFluidityShapePerParticipant.forEach(fluidity => allFlexibilityPerParticipant.push(flexibilityCalculator(fluidity)))
    //to calculate the flexibility per participant in a activity
    activityFluidityShapePerParticipant.forEach(fluidity => activityFlexibilityPerParticipant.push(flexibilityCalculator(fluidity)))
    //to have the list of all the made shapes
    allFluidityShapePerParticipant.forEach(fluidity => fluidity.forEach(shape => activityFluidity.push(shape)))

    //CREATIVITY CALCULATOR

    //the number of shape made in group of participant
    let totalFluidity = activityFluidity.length;
    //to calculate five percent of the total fluidity
    let fivePercent = totalFluidity * 0.05;

    //to calculate the flexibility of the group
    activityFlexibility = flexibilityCalculator(activityFluidity)

    //to count the flexibility per participant
    activityFlexibilityPerParticipant.forEach(flexibility => FlexibilityCount.push(Object.keys(flexibility).length))

    let F000 = 0
    let F010 = 0
    let F020 = 0
    let F030 = 0
    let F040 = 0
    let F050 = 0
    let F060 = 0
    let FXX = 0

    for (const fluidity of activityFluidityShapePerParticipant){
        for (const shape of fluidity){
            converterByFamily(shape)
            switch (shape){
                case "000": F000++
                    break;
                case "010": F010++
                    break;
                case "020": F020++
                    break;
                case "030": F030++
                    break;
                case "040": F040++
                    break;
                case "050": F050++
                    break;
                case "060": F060++
                    break;
                case "XX": FXX++
                    break;
            }
        }
    }

    //to list the original/innovative shapes of the group
    var originalShapes = originalShapesCalculator(activityFlexibility, fivePercent)

    //to attribute the innovation/originality's points per participant
    for (const fluidity of activityFluidityShapePerParticipant)
    {
        let participantInnovation = 0;
        originalShapes.forEach(shape => {if(fluidity.includes(shape))
        {
            participantInnovation += 1
        }})
        OriginalityCount.push(participantInnovation);
    }

    if(wantedActivity === 1)
    {
        A1ShapesByFamilyCount = new Array()
        A1FluidityCount = FluidityCount;
        A1FlexibilityCount = FlexibilityCount;
        A1OriginalityCount = OriginalityCount;
        A1ShapesByFamilyCount.push(F000,F010,F020,F030,F040,F050,F060,FXX)
    } else if(wantedActivity === 2)
    {
        A2ShapesByFamilyCount = new Array()
        A2FluidityCount = FluidityCount;
        A2FlexibilityCount = FlexibilityCount;
        A2OriginalityCount = OriginalityCount;
        A2ShapesByFamilyCount.push(F000,F010,F020,F030,F040,F050,F060,FXX)
    }


}

/**
 * calculate allFluidityCount, allFlexibilityCount, AllOriginalityCount, allCooperation and OriginalShapes
 * @param participations
 * @returns {Promise<void>}
 */
async function calculateGlobalCreativity(participations) {

    allFluidityCount = new Array();
    allFlexibilityCount = new Array();
    allOriginalityCount = new Array();
    allCooperation = new Array();
    OriginalShapes = new Array();
    idParticipant = new Array();
    ShapesByFamilyCount = new Array()

    participations.forEach(participation => idParticipant.push(participation.participants.idParticipant))


    //list of the name of mad shape per participant
    var allFluidityShapePerParticipant = new Array();
    //list of the name of mad shape bor all the group
    var allFluidity = new Array();
    //the name and the occurrence of different shape made by all the group
    var allFlexibility = new Array();
    //the name and the occurrence of different shape made per participant
    var allFlexibilityPerParticipant = new Array();

    //to calculate the fluidity per participant
    participations.forEach(participation => allFluidityShapePerParticipant.push(fluidityCalculator(participation)));

    //to count the fluidity per participant
    allFluidityShapePerParticipant.forEach(fluidity => allFluidityCount.push(fluidity.length))

    //to calculate the flexibility per participant
    allFluidityShapePerParticipant.forEach(fluidity => allFlexibilityPerParticipant.push(flexibilityCalculator(fluidity)))
    //to have the list of all the made shapes
    allFluidityShapePerParticipant.forEach(fluidity => fluidity.forEach(shape => allFluidity.push(shape)))


    //CREATIVITY CALCULATOR

    //the number of shape made in group of participant
    let totalFluidity = allFluidity.length;
    //to calculate five percent of the total fluidity
    let fivePercent = totalFluidity * 0.05;

    //to calculate the flexibility of the group
    allFlexibility = flexibilityCalculator(allFluidity)

    //to count the flexibility per participant
    allFlexibilityPerParticipant.forEach(flexibility => allFlexibilityCount.push(Object.keys(flexibility).length))

    let F000 = 0
    let F010 = 0
    let F020 = 0
    let F030 = 0
    let F040 = 0
    let F050 = 0
    let F060 = 0
    let FXX = 0

    for (const fluidity of allFluidityShapePerParticipant){
        for (const shape of fluidity){
        converterByFamily(shape)
            switch (shape){
                case "000": F000++
                    break;
                case "010": F010++
                    break;
                case "020": F020++
                    break;
                case "030": F030++
                break;
                case "040": F040++
                    break;
                case "050": F050++
                    break;
                case "060": F060++
                    break;
                case "XX": FXX++
                    break;
            }
        }
    }
    ShapesByFamilyCount.push(F000,F010,F020,F030,F040,F050,F060,FXX)

    //to list the original/innovative shapes of the group
    OriginalShapes = originalShapesCalculator(allFlexibility, fivePercent)

    //to attribute the innovation/originality's points per participant
    for (const fluidity of allFluidityShapePerParticipant)
    {
        let participantInnovation = 0;
        OriginalShapes.forEach(shape => {if(fluidity.includes(shape))
        {
            participantInnovation += 1
        }})
        allOriginalityCount.push(participantInnovation);
    }

    for(let i=0; i<participations.length; i++ )
    {
        let nbp = parseInt(participations[i].nbParticipants)
        if( nbp > 1) {
            let cooperation = await cooperationCalculator(participations[i])
            allCooperation.push(cooperation[0])
        }else {
            allCooperation.push(0)
        }
    }

}

/**
 * To get all the participant in the list
 * @param callback
 */
function getAll(callback) {
    $.post(
        '/listParticipations/',
        { filtres:
                {
                    bebe: null,
                    enfant: null,
                    ado: null,
                    jadulte: null,
                    adulte: null,
                    senior: null,
                    genreF: null,
                    genreM: null,
                    modaliteI: null,
                    modaliteC: null,
                    modaliteB: null,
                    modaliteT: null,
                    idContext: null,
                    idTeam: null
                }
        },
        callback,
        'json'
    );
}

