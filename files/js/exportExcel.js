/**
 * To make the data for the excel
 * @param participations
 */



async function makeExcel(participations)
{
    var wsParticipants = [['idVideo','idParticipant','Age','AgeCategory', 'Gender', 'idTeam', 'idVideo', 'Total time','Total time (in second)', 'Total fluidity', 'Total flexibility', 'Total innovation', 'A1 time','A1 time (in second)','A1 fluidity', 'A1 flexibility', 'A1 innovation','A2 time','A2 time (in second)','A2 fluidity', 'A2 flexibility', 'A2 innovation', 'Cooperation']]
    await calculateGlobalCreativity(participations)
    var wsOriginalShapes= [['OriginalShapes']]
    activityCalculator(participations,1)
    activityCalculator(participations,2)

    let timesTotal= [];
    let timesA1= [];
    let timesA2= [];


    for (let i = 0 ;i < participations.length;i++)
    {
        let data = [participations[i].idVideo,participations[i].participants.idParticipant, participations[i].participants.age, participations[i].participants.ageCategory, participations[i].participants.gender, participations[i].idTeam, participations[i].idVideo]

        let times = allTimeCalculator(participations[i])

        timesTotal.push(totalTimeCalculator(times))
        timesA1.push(A1TimeCalculator(times))
        timesA2.push(A2TimeCalculator(times))

        data.push(timeConverter(totalTimeCalculator(times)))
        data.push(timesTotal[i])
        data.push(allFluidityCount[i])
        data.push(allFlexibilityCount[i])
        data.push(allOriginalityCount[i])

        data.push(timeConverter(A1TimeCalculator(times)))
        data.push(timesA1[i])
        data.push(A1FluidityCount[i])
        data.push(A1FlexibilityCount[i])
        data.push(A1OriginalityCount[i])

        data.push(timeConverter(A2TimeCalculator(times)))
        data.push(timesA2[i])
        data.push(A2FluidityCount[i])
        data.push(A2FlexibilityCount[i])
        data.push(A2OriginalityCount[i])

        let pushed = true;

        if(document.getElementById('display-only-good-data').checked === true) {

            if(totalTimeCalculator(times) <= 0)
            {
                pushed = false;
            }

        }

        if(document.getElementById('display-only-good-data-A1').checked === true) {

            if(A1TimeCalculator(times) <= 0)
            {
                pushed = false;
            }

        }

        if(document.getElementById('display-only-good-data-A2').checked === true) {

            if(A2TimeCalculator(times) <= 0)
            {
                pushed = false;
            }

        }

        if (pushed === true)
        {
            wsParticipants.push(data)
        }

        data.push(allCooperation[i])



        wsParticipants.push(data)
    }

    for (let i =0; i<OriginalShapes.length;i++)
    {
        let data = [OriginalShapes[i]]
        wsOriginalShapes.push(data)
    }

    var wsGroup = [["Criterion" ,"Min value", "First quartile", "Median", "Third quartile", "Max Value", "Average", "Standard Deviation", "Variance", "NB participant"]]


    //Fluidity calculations
    wsGroup.push(['Fluidity', math.min(allFluidityCount), math.quantileSeq(allFluidityCount, 0.25), math.median(allFluidityCount), math.quantileSeq(allFluidityCount, 0.75), math.max(allFluidityCount), math.mean(allFluidityCount), math.std(allFluidityCount),math.variance(allFluidityCount), allFluidityCount.length])
    wsGroup.push(['Fluidity A1', math.min(A1FluidityCount), math.quantileSeq(A1FluidityCount, 0.25), math.median(A1FluidityCount), math.quantileSeq(A1FluidityCount, 0.75), math.max(A1FluidityCount), math.mean(A1FluidityCount), math.std(A1FluidityCount),math.variance(A1FluidityCount), A1FluidityCount.length])
    wsGroup.push(['Fluidity A2', math.min(A2FluidityCount), math.quantileSeq(A2FluidityCount, 0.25), math.median(A2FluidityCount), math.quantileSeq(A2FluidityCount, 0.75), math.max(A2FluidityCount), math.mean(A2FluidityCount), math.std(A2FluidityCount),math.variance(A2FluidityCount), A2FluidityCount.length])
    //Flexibility calculations
    wsGroup.push(['Flexibility', math.min(allFlexibilityCount), math.quantileSeq(allFlexibilityCount, 0.25), math.median(allFlexibilityCount), math.quantileSeq(allFlexibilityCount, 0.75), math.max(allFlexibilityCount), math.mean(allFlexibilityCount), math.std(allFlexibilityCount),math.variance(allFlexibilityCount),allFlexibilityCount.length])
    wsGroup.push(['Flexibility A1', math.min(A1FlexibilityCount), math.quantileSeq(A1FlexibilityCount, 0.25), math.median(A1FlexibilityCount), math.quantileSeq(A1FlexibilityCount, 0.75), math.max(A1FlexibilityCount), math.mean(A1FlexibilityCount), math.std(A1FlexibilityCount),math.variance(A1FlexibilityCount), A1FlexibilityCount.length])
    wsGroup.push(['Flexibility A2', math.min(A2FlexibilityCount), math.quantileSeq(A2FlexibilityCount, 0.25), math.median(A2FlexibilityCount), math.quantileSeq(A2FlexibilityCount, 0.75), math.max(A2FlexibilityCount), math.mean(A2FlexibilityCount), math.std(A2FlexibilityCount),math.variance(A2FlexibilityCount), A2FlexibilityCount.length])
    //Originality calculations
    wsGroup.push(['Originality', math.min(allOriginalityCount), math.quantileSeq(allOriginalityCount, 0.25), math.median(allOriginalityCount), math.quantileSeq(allOriginalityCount, 0.75), math.max(allOriginalityCount), math.mean(allOriginalityCount), math.std(allOriginalityCount),math.variance(allOriginalityCount), allOriginalityCount.length])
    wsGroup.push(['Originality A1', math.min(A1OriginalityCount), math.quantileSeq(A1OriginalityCount, 0.25), math.median(A1OriginalityCount), math.quantileSeq(A1OriginalityCount, 0.75), math.max(A1OriginalityCount), math.mean(A1OriginalityCount), math.std(A1OriginalityCount),math.variance(A1OriginalityCount), A1OriginalityCount.length])
    wsGroup.push(['Originality A2', math.min(A2OriginalityCount), math.quantileSeq(A2OriginalityCount, 0.25), math.median(A2OriginalityCount), math.quantileSeq(A2OriginalityCount, 0.75), math.max(A2OriginalityCount), math.mean(A2OriginalityCount), math.std(A2OriginalityCount),math.variance(A2OriginalityCount), A2OriginalityCount.length])
    //Times calculations
    wsGroup.push(['Time', math.min(timesTotal), math.quantileSeq(timesTotal, 0.25), math.median(timesTotal), math.quantileSeq(timesTotal, 0.75), math.max(timesTotal), math.mean(timesTotal), math.std(timesTotal),math.variance(timesTotal), timesTotal.length])
    wsGroup.push(['Time A1', math.min(timesA1), math.quantileSeq(timesA1, 0.25), math.median(timesA1), math.quantileSeq(timesA1, 0.75), math.max(timesA1), math.mean(timesA1), math.std(timesA1),math.variance(timesA1), timesA1.length])
    wsGroup.push(['Time A2', math.min(timesA2), math.quantileSeq(timesA2, 0.25), math.median(timesA2), math.quantileSeq(timesA2, 0.75), math.max(timesA2), math.mean(timesA2), math.std(timesA2),math.variance(timesA2), timesA2.length])

    //Cooperation calculations

    wsGroup.push(['Cooperation', math.min(allCooperation), math.quantileSeq(allCooperation, 0.25), math.median(allCooperation), math.quantileSeq(allCooperation, 0.75), math.max(allCooperation), math.mean(allCooperation), math.std(allCooperation),math.variance(allCooperation), allCooperation.length])


    excel(wsParticipants, wsOriginalShapes, wsGroup)

}

/**
 * To make the excel and download it
 * @param participants
 * @param originalShapes
 * @param group
 */
function excel(participants, originalShapes, group)
{
    var wb = XLSX.utils.book_new();

    wb.Props = {
        Title: "ANR CreaMaker",
        Subject: "CreaCube",
        Author: "LINE",
        //CreatedDate: new Date(2017,12,19)
    };


    wb.SheetNames.push("Participants");
    wb.SheetNames.push("OriginalShapes");
    wb.SheetNames.push("Group");


    wb.Sheets["Participants"] = XLSX.utils.aoa_to_sheet(participants);
    wb.Sheets["OriginalShapes"] = XLSX.utils.aoa_to_sheet(originalShapes);
    wb.Sheets["Group"] = XLSX.utils.aoa_to_sheet(group);

    var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
        var view = new Uint8Array(buf);  //create uint8array as viewer
        for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
        return buf;
    }

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'ANR CreaMaker.xlsx');

}
