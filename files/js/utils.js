const typesEvents = ["AF", "AS", "B", "BU", "E", "F", "FL", "P", "U", "T"];

const ageCategories = [
	{ "nom": "beb", "libelle": "Bébé", ageDebut: 0.0, ageFin: 6.0 },
	{ "nom": "enf", "libelle": "Enfant", ageDebut: 7.0, ageFin: 12.0 },
	{ "nom": "ado", "libelle": "Adolescent", ageDebut: 13.0, ageFin: 18.0 },
	{ "nom": "jad", "libelle": "Jeune adulte", ageDebut: 19.0, ageFin: 29.0 },
	{ "nom": "adu", "libelle": "Adulte", ageDebut: 30.0, ageFin: 59.0 },
	{ "nom": "sen", "libelle": "Senior", ageDebut: 60.0, ageFin: 99.0 }
];

const teamHistory = [
	{ "code": 0, "libelle": "Nous ne nous connaissons pas" },
	{ "code": 1, "libelle": "Moins d'une semaine" },
	{ "code": 2, "libelle": "Moins d'un mois" },
	{ "code": 3, "libelle": "Moins d'un an" },
	{ "code": 4, "libelle": "Plus d'un an" }
];

const teamCloserness = [
	{ "code": 0, "libelle": "Sans avis" },
	{ "code": 1, "libelle": "Aucune amitié ou proximité" },
	{ "code": 2, "libelle": "Nous avons une certaine proximité ou amité" },
	{ "code": 3, "libelle": "Nous sommes proches ou amis" },
	{ "code": 4, "libelle": "Je n'apprécie pas mon binôme" }
];

typeEvent = function(c) {
	var typeEvent = "";
	if (c == "START1" || c == "START2") {
		typeEvent = "T";
	} else if (/^FX/g.test(c)) {
		typeEvent = "F";
	} else if (!(/^FL/g.test(c)) && (/^F/g.test(c)) && c.length>4 ){
		typeEvent = c.substr(0,1);
	}
	else {
		let re = /[0-9]/g;
		typeEvent = c.replace(re, "");
	}
	return typeEvent;
}

ageCategory = function(age) {
	const parsed = parseFloat(age);
	var category = "";
	
	if (!isNaN(parsed)) {
		ageCategories.forEach(function(ac) {
			if (parsed >= ac.ageDebut && parsed <= ac.ageFin) {
				category = ac.nom;
			}
		});
	}

	return category;
}