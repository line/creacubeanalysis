
async function globalStatistic(participations)
{
    await calculateGlobalCreativity(participations)

    //To calculate all the statics for the fluidity
    calculateGlobalStatistic("fluidity", allFluidityCount)

    //To calculate all the statics for the flexibility
    calculateGlobalStatistic("flexibility", allFlexibilityCount)

    //To calculate all the statics for the originality
    calculateGlobalStatistic("originality", allOriginalityCount)

    let timesTotal= [];
    let timesA1= [];
    let timesA2= [];
    let ages= [];

    for (let i = 0 ;i < participations.length;i++){

        let times = allTimeCalculator(participations[i])

        timesTotal.push(totalTimeCalculator(times))
        timesA1.push(A1TimeCalculator(times))
        timesA2.push(A2TimeCalculator(times))

        if (participations[i].participants.age !== null)
        {
            ages.push(participations[i].participants.age)
        }

    }

    //To calculate all the statics for the global time
    calculateGlobalStatistic("total_time", timesTotal)

    await activityCalculator(participations,1)

    //To calculate all the statics for the a1 fluidity
    calculateGlobalStatistic("fluidity_a1", A1FluidityCount)

    //To calculate all the statics for the a1 flexibility
    calculateGlobalStatistic("flexibility_a1", A1FlexibilityCount)

    //To calculate all the statics for the a1 originality
    calculateGlobalStatistic("originality_a1", A1OriginalityCount)

    //To calculate all the statics for the a1 time
    calculateGlobalStatistic("time_a1", timesA1)

    await activityCalculator(participations,2)

    //To calculate all the statics for the a1 fluidity
    calculateGlobalStatistic("fluidity_a2", A2FluidityCount)

    //To calculate all the statics for the a1 flexibility
    calculateGlobalStatistic("flexibility_a2", A2FlexibilityCount)

    //To calculate all the statics for the a1 originality
    calculateGlobalStatistic("originality_a2", A2OriginalityCount)

    //To calculate all the statics for the a1 time
    calculateGlobalStatistic("time_a2", timesA2)

    calculateGlobalStatistic("cooperation", allCooperation)

    calculateGlobalStatistic("age", ages)

}

function addAllBeginTable()
{
    getAll(function (participations) {

        globalStatistic(participations).then(r => participantStatistic(participations) )

        currentParticipation = participations

        initPieChart()
        Object.assign(Participation, participations)
    })
}

function calculateGlobalStatistic(html_id, const_name)
{

    if( const_name.length === 0){

        $("#" + html_id + "_min").html("0")
        $("#" + html_id + "_quantile1").html("0")
        $("#" + html_id + "_median").html("0")
        $("#" + html_id + "_quantile3").html("0")
        $("#" + html_id + "_max").html("0")
        $("#" + html_id + "_average").html("0")
        $("#" + html_id + "_sd").html("0")
        $("#" + html_id + "_variance").html("0")
        $("#" + html_id + "_nbP").html("0")

    } else {
        $("#" + html_id + "_min").html(math.min(const_name).toString())
        $("#" + html_id + "_quantile1").html(math.quantileSeq(const_name, 0.25).toString())
        $("#" + html_id + "_median").html(math.median(const_name).toString())
        $("#" + html_id + "_quantile3").html(math.quantileSeq(const_name, 0.75).toString())
        $("#" + html_id + "_max").html(math.max(const_name).toString())
        $("#" + html_id + "_average").html(math.mean(const_name).toFixed(2).toString())
        $("#" + html_id + "_sd").html(math.std(const_name).toFixed(2).toString())
        $("#" + html_id + "_variance").html(math.variance(const_name).toFixed(2).toString())
        $("#" + html_id + "_nbP").html((const_name.length).toString())
    }

}

function participantStatistic(participations)
{

    let globalTable = document.getElementById("global_data_table")

    while (globalTable.firstChild){
        globalTable.removeChild(globalTable.firstChild)
    }

    let A1Table = document.getElementById("a1_data_table")

    while (A1Table.firstChild){
        A1Table.removeChild(A1Table.firstChild)
    }

    let A2Table = document.getElementById("a2_data_table")

    while (A2Table.firstChild){
        A2Table.removeChild(A2Table.firstChild)
    }


    for (let i = 0 ;i < participations.length;i++){

        let globalTR = document.createElement("TR")
        let A1TR = document.createElement("TR")
        let A2TR = document.createElement("TR")

        globalTable.appendChild(globalTR)

        let times = allTimeCalculator(participations[i])

        createHTMLElement("TD", "col-1 global_table_statistic", "id_video_" + participations[i].participants.idParticipant, participations[i].idVideo, globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "id_participant_" + participations[i].participants.idParticipant, participations[i].participants.idParticipant, globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "id_team_" + participations[i].participants.idParticipant, participations[i].idTeam, globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "age_" + participations[i].participants.idParticipant, participations[i].participants.age, globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "age_category" + participations[i].participants.idParticipant, participations[i].participants.ageCategory, globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "gender_" + participations[i].participants.idParticipant, participations[i].participants.gender, globalTR)

        createHTMLElement("TD", "col-1 global_table_statistic", "fluidity_" + participations[i].participants.idParticipant, allFluidityCount[i], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "flexibility_" + participations[i].participants.idParticipant, allFlexibilityCount[i], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "originality_" + participations[i].participants.idParticipant, allOriginalityCount[i], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "time_" + participations[i].participants.idParticipant, totalTimeCalculator(times), globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "timeaf01_" + participations[i].participants.idParticipant, times[4], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "timeaf02_" + participations[i].participants.idParticipant, times[5], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "timeaf03_" + participations[i].participants.idParticipant, times[6], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "timeaf04_" + participations[i].participants.idParticipant, times[7], globalTR)
        createHTMLElement("TD", "col-1 global_table_statistic", "timeaf05_" + participations[i].participants.idParticipant, times[8], globalTR)


        A1Table.appendChild(A1TR)

        createHTMLElement("TD", "col-1", "a1_id_video_" + participations[i].participants.idParticipant, participations[i].idVideo, A1TR)
        createHTMLElement("TD", "col-2", "a1_id_participant_" + participations[i].participants.idParticipant, participations[i].participants.idParticipant, A1TR)
        createHTMLElement("TD", "col-1", "a1_id_team_" + participations[i].participants.idParticipant, participations[i].idTeam, A1TR)
        createHTMLElement("TD", "col-1", "a1_age_" + participations[i].participants.idParticipant, participations[i].participants.age, A1TR)
        createHTMLElement("TD", "col-2", "a1_age_category" + participations[i].participants.idParticipant, participations[i].participants.ageCategory, A1TR)
        createHTMLElement("TD", "col-1", "a1_gender_" + participations[i].participants.idParticipant, participations[i].participants.gender, A1TR)

        createHTMLElement("TD", "col-1", "a1_fluidity_" + participations[i].participants.idParticipant, A1FluidityCount[i], A1TR)
        createHTMLElement("TD", "col-1", "a1_flexibility_" + participations[i].participants.idParticipant, A1FlexibilityCount[i], A1TR)
        createHTMLElement("TD", "col-1", "a1_originality_" + participations[i].participants.idParticipant, A1OriginalityCount[i], A1TR)
        createHTMLElement("TD", "col-1", "a1_time_" + participations[i].participants.idParticipant, A1TimeCalculator(times), A1TR)

        A2Table.appendChild(A2TR)

        createHTMLElement("TD", "col-1", "a2_id_video_" + participations[i].participants.idParticipant, participations[i].idVideo, A2TR)
        createHTMLElement("TD", "col-2", "a2_id_participant_" + participations[i].participants.idParticipant, participations[i].participants.idParticipant, A2TR)
        createHTMLElement("TD", "col-1", "a2_id_team_" + participations[i].participants.idParticipant, participations[i].idTeam, A2TR)
        createHTMLElement("TD", "col-1", "a2_age_" + participations[i].participants.idParticipant, participations[i].participants.age, A2TR)
        createHTMLElement("TD", "col-2", "a2_age_category" + participations[i].participants.idParticipant, participations[i].participants.ageCategory, A2TR)
        createHTMLElement("TD", "col-1", "a2_gender_" + participations[i].participants.idParticipant, participations[i].participants.gender, A2TR)

        createHTMLElement("TD", "col-1", "a2_fluidity_" + participations[i].participants.idParticipant, A2FluidityCount[i], A2TR)
        createHTMLElement("TD", "col-1", "a2_flexibility_" + participations[i].participants.idParticipant, A2FlexibilityCount[i], A2TR)
        createHTMLElement("TD", "col-1", "a2_originality_" + participations[i].participants.idParticipant, A2OriginalityCount[i], A2TR)
        createHTMLElement("TD", "col-1", "a2_time_" + participations[i].participants.idParticipant, A2TimeCalculator(times), A2TR)

    }

}

function createHTMLElement(tag, className, id, value, parent)
{

    let newChild = document.createElement(tag)
    newChild.className = className
    newChild.id = id
    parent.appendChild(newChild)
    $("#" + id).html(value)

}

function initPieChart(){

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawPieChart);

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawPieChart2);

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawPieChartGlobal);

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawPieChartA1);

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawPieChartA2);

}

function drawPieChart(){

    let beb = 0
    let enf = 0
    let ado = 0
    let jad = 0
    let adu = 0
    let sen = 0
    for(let i=0; i<currentParticipation.length; i++){

        switch (currentParticipation[i].participants.ageCategory){
            case 'beb':
                beb++
                break;
            case 'enf':
                enf++
                break;
            case 'ado':
                ado++
                break;
            case 'jad':
                jad++
                break;
            case 'adu':
                adu++
                break;
            case 'sen':
                sen++
                break;
        }

    }
    var data = google.visualization.arrayToDataTable([
        ['AgeCategory', 'Number of participant'],
        ['beb', beb],
        ['enf', enf],
        ['ado', ado],
        ['jad', jad],
        ['adu', adu],
        ['sen', sen]
    ]);

    var options = {
        title: 'Age category'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);

}

function drawPieChart2(){

    let ind = 0
    let bin = 0
    let tri = 0
    for(let i=0; i<currentParticipation.length; i++){

        switch (currentParticipation[i].nbParticipants){
            case '1':
                ind++
                break;
            case '2':
                bin++
                break;
            case '3':
                tri++
                break;
        }

    }
    var data = google.visualization.arrayToDataTable([
        ['Team', 'Number of participant'],
        ['ind', ind],
        ['bin', bin],
        ['tri', tri]
    ]);

    var options = {
        title: 'Number of participants'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

    chart.draw(data, options);

}

function drawPieChartGlobal()
{
    var data = google.visualization.arrayToDataTable([
        ['Shape Family', 'Number of shape'],
        ['000', ShapesByFamilyCount[0]],
        ['010', ShapesByFamilyCount[1]],
        ['020', ShapesByFamilyCount[2]],
        ['030', ShapesByFamilyCount[3]],
        ['040', ShapesByFamilyCount[4]],
        ['050', ShapesByFamilyCount[5]],
        ['060', ShapesByFamilyCount[6]],
        ['Other', ShapesByFamilyCount[7]]
    ]);

    var options = {
        title: 'Shape family'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechartGlobal'));

    chart.draw(data, options);

}

function drawPieChartA1()
{
    var data = google.visualization.arrayToDataTable([
        ['Shape Family', 'Number of shape'],
        ['000', A1ShapesByFamilyCount[0]],
        ['010', A1ShapesByFamilyCount[1]],
        ['020', A1ShapesByFamilyCount[2]],
        ['030', A1ShapesByFamilyCount[3]],
        ['040', A1ShapesByFamilyCount[4]],
        ['050', A1ShapesByFamilyCount[5]],
        ['060', A1ShapesByFamilyCount[6]],
        ['Other', A1ShapesByFamilyCount[7]]
    ]);

    var options = {
        title: 'Shape family'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechartA1'));

    chart.draw(data, options);
}
function drawPieChartA2()
{
    var data = google.visualization.arrayToDataTable([
        ['Shape Family', 'Number of shape'],
        ['000', A2ShapesByFamilyCount[0]],
        ['010', A2ShapesByFamilyCount[1]],
        ['020', A2ShapesByFamilyCount[2]],
        ['030', A2ShapesByFamilyCount[3]],
        ['040', A2ShapesByFamilyCount[4]],
        ['050', A2ShapesByFamilyCount[5]],
        ['060', A2ShapesByFamilyCount[6]],
        ['Other', A2ShapesByFamilyCount[7]]
    ]);

    var options = {
        title: 'Shape family'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechartA2'));

    chart.draw(data, options);
}