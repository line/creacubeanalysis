/**
 * To get the information of the participation in a same team
 * @param participation that we need to get the team
 * @param callback
 */
function getTeam(participation,callback)
{
    $.get(
        '/getTeam',
        { idVideo : participation.idVideo,
            idTeam : participation.idTeam,
            idParticipant : participation.participants.idParticipant
        },
        callback,
        'json'
    );

    //let team = []
//
    //Participation.forEach(participant => {if (participation.idVideo === participant.idVideo && participation.idTeam === participant.idTeam)
    //{
    //    team.push(participant)
//
    //}
    //})
//
//
    //console.log("Voici la team")
    //console.log(team)
    //return team
}//

/**
 * To create the promise to get a team
 * @param participation
 * @returns {Promise<unknown>}
 */
function waitForTeam(participation)
{
    return new Promise((resolve => {
        getTeam(participation, function(participationTeam){
            resolve(participationTeam)
        })
    }))
}

/**
 * To calculate the cooperation of a participant
 * @param participation
 * @returns {Promise<[]>}
 */
async function cooperationCalculator(participation) {

    //to calculate the fluidity of the participant
    let fluidityTimeParticipant = fluidityTimeCalculator(participation);
    let fluidityTimeTeam = [];
    let cooperation = [];

    //to wait for the recuperation of the team info
    let participationTeam = []//await waitForTeam(participation)

    Participation.forEach(participant => {if (participation.idVideo === participant.idVideo && participation.idTeam === participant.idTeam && participation.participants.idParticipant !== participant.participants.idParticipant)
    {
        participationTeam.push(participant)
    }
    })

    for( let i=0; i<=2; i++)
    {
        let fluidityTimeParticipantByActivity = wantedActivityFluidityTime(fluidityTimeParticipant, i)

        for (let a = 0; a < participationTeam.length; a++) {
            fluidityTimeTeam[a] = wantedActivityFluidityTime(fluidityTimeCalculator(participationTeam[a]), i)
        }

        cooperation.push(checkCooperation(fluidityTimeParticipantByActivity,fluidityTimeTeam))
    }
    // to calculate the fluidity with the time for each participant and the team (necessary in case of trinomial)

    //to determine for each shapes made by the participant if there are cooperation

    return cooperation;
}

function checkCooperation(fluidityTimeParticipant, fluidityTimeTeam)
{
    let cooperation = 0;
    let delay = 2;

    for (const [key, value] of Object.entries(fluidityTimeParticipant)) {

        if (value !== "T03")
        {

            //to verify if the shape is made by the other participant of the team in the same time (with 3sec of difference)
            for (let i = 0; i < fluidityTimeTeam.length; i++) {


                let verifyToAddOnce = false;

                for (let a=0; a<=delay; a++)
                {
                    if((fluidityTimeTeam[i].hasOwnProperty((parseInt(key) - a).toString()) || fluidityTimeTeam[i].hasOwnProperty((parseInt(key) + a).toString()))
                        && (fluidityTimeTeam[i][(parseInt(key) - a).toString()] === value ||fluidityTimeTeam[i][(parseInt(key) + a).toString()] === value))
                    {
                        cooperation += 1;
                        verifyToAddOnce = true;
                        break
                    }
                }

                if(verifyToAddOnce === true)
                {
                    break
                }
                
            }
        }
    }

    return cooperation
}

function wantedActivityFluidityTime(fluidityTime, wantedActivity)
{

    var wantedActivityFluidity = {};

    if (wantedActivity === 1 && !Array.isArray(fluidityTime)) {

        fluidityTime = Object.entries(fluidityTime)

        for(const [key, value] of fluidityTime)
        {
            if (value === "T03") {
                break
            }
            wantedActivityFluidity[key] = value
        }
    } else if (wantedActivity === 2 && !Array.isArray(fluidityTime)) {

        fluidityTime = Object.entries(fluidityTime)

        let beginActivity = 1

            for (const [key, value] of fluidityTime) {
                if (value === "T03") {
                    break;
                }
                beginActivity++
            }
            fluidityTime = fluidityTime.slice(beginActivity)

        for (const [key, value] of fluidityTime)
        {
            if (value === "T03") {
                break
            }
            wantedActivityFluidity[key] = value
        }
    } else {
        wantedActivityFluidity = fluidityTime
    }

    return wantedActivityFluidity;
}

/**
 * to calculate the fluidity of participant with time of the made shapes
 * @param participation
 * @returns {{}}
 */
function fluidityTimeCalculator(participation)
{
    let fluidityTime = {};

    if (participation.participants.clicks.length > 0) {
        for (k in participation.participants.clicks) {
            for (cl of participation.participants.clicks[k].tclicks) {
                var t = (participation.participants.clicks[k].time).toString();
                if (cl == "START" || cl[0] == "T" || (cl[0] == "F" && cl[1] != "L" || cl == "START1" || cl == "START2")) {
                    var f = 0;

                    if (cl === "FXX")
                    {
                        fluidityTime[t] = "XX"
                    }
                    else if (cl === "FX1")
                    {
                        fluidityTime[t] = "X1"
                    }
                   else if ((cl[0] === "F" && cl[1] !== "L" && cl.length < 4)) {

                        f = cl.substr(1,3);
                        fluidityTime.push(f);
                    }
                    else if (cl[0] === "F" && cl[1] !== "L")
                    {
                        f = cl.substring(1,4)
                        fluidityTime[t] = f;
                    }
                    else if (cl[0] == "T" && cl[1] == "0" && cl[2] == "3") {
                        fluidityTime[t] = "T03";
                    }
                }
            }
        }
    }
    return fluidityTime;
}