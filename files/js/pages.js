
function updateVideoList(nbPage) {

    debugger

    callback = function(videos) {
        debugger
        $("#list_videos").html("");
        videos.forEach(function(v) {
            let newTR = document.createElement("tr")
            $("#list_videos").append(newTR);
            v.videoOK ? $("<td class=\"col-1\"><img src=\"/images/circle-check-2x-green.png\">" + v.idVideo + "</td>").appendTo(newTR) : $("<td class=\"col-1\"><img src=\"/images/circle-x-2x-red.png\"> " + v.idVideo + "</td>").appendTo(newTR);
            $("<td class=\"col-2\">"+ v.videoName +"</td>").appendTo(newTR)
            $("<td class=\"col-1\">"+ v.idTeam +"</td>").appendTo(newTR)
            let participationTD = document.createElement("td")
            participationTD.className = "col-3"
            let commentTD = document.createElement("td")
            commentTD.className = "col-3"
            for( let participant of v.participants) {
                participant.participationOK ? $("<img src=\"/images/circle-check-2x-green.png\">").appendTo(participationTD) : $("<img src=\"/images/circle-x-2x-red.png\">").appendTo(participationTD)
                participationTD.append(participant.idParticipant +"-"+ participant.nameParticipant +"-"+ participant.gender +"-"+ participant.age)
                $("<br>").appendTo(participationTD)
                participant.commentaire !== "" && participant.commentaire !== null ? commentTD.append(participant.idParticipant + "-" + participant.commentaire) : null

            }
            $(participationTD).appendTo(newTR)
            $("<td class=\"col-1\">"+ v.dateJMA +"</td>").appendTo(newTR)

            v.commentaire !== "" && v.commentaire !== null ? commentTD.prepend(v.commentaire): null
            $(commentTD).appendTo(newTR)

            $("<td class=\"col-1 text-center\"><a href=\"videoTreatment.html?id=" + v.idVideo +"\"\n" +
                "               target=\"_blank\" class=\"btn modifVideo\">\n" +
                "                <img src=\"/images/open-iconic/pencil-2x.png\">\n" +
                "            </a>").appendTo(newTR)

        });
    };

    $.get(
        '/getUpdatedPage',
        { page:nbPage-1},
        callback,
        'json'
    );

}


