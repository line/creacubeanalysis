var cfg = {
	radiusTick: 2,
	radiusSommet: 5,
	w: 600,
	h: 600,
	factorLegend: .85,
	levels: 3,
	maxValue: 0,
	radians: 2 * Math.PI,
	opacityArea: 0.5,
	ToRight: 5,
	TranslateX: 150,
	TranslateY: 50,
	ExtraWidthX: 200,
	ExtraWidthY: 100,
	maxLg: 0,
	color: d3.scale.category10()
};

var RadarChart = {
	draw: function(id, axes, data) {
		cfg.maxLg = Math.max(cfg.maxLg, d3.max(axes, function(o) {
			return 1 + (o.ticks - o.init) / o.init; }));
		var allAxis = (axes.map(function(i, j) { return i.axis }));
		var total = allAxis.length;
		var radius = Math.min(cfg.w, cfg.h) / 2;
		var Format = d3.format('%');

		var g = d3.select(id)
			.append("svg")
			.attr("width", cfg.w + cfg.ExtraWidthX)
			.attr("height", cfg.h + cfg.ExtraWidthY)
			.append("g")
			.attr("transform", "translate(" + cfg.TranslateX + ", " + cfg.TranslateY + ")");

		var tooltip;

		var axis = g.selectAll(".axis")
			.data(allAxis)
			.enter()
			.append("g")
			.attr("class", "axis");
	
		axis.append("line")
			.attr("x1", radius)
			.attr("y1", radius)
			.attr("x2", function(d, i) {
				return radius * (1 - Math.sin(i * cfg.radians / total)); })
			.attr("y2", function(d, i) {
				return radius * (1 - Math.cos(i * cfg.radians / total)); })
			.attr("class", "line")
			.style("stroke", "grey")
			.style("stroke-width", "1px");

		axis.append("text")
			.attr("class", "legend")
			.text(function(d) { return d })
			.style("font-family", "sans-serif")
			.style("font-size", "11px")
			.attr("text-anchor", "middle")
			.attr("dy", "1.5em")
			.attr("transform", function(d, i) { return "translate(0, -10)" })
			.attr("x", function(d, i){
				return radius * (1 - cfg.factorLegend * Math.sin(i * cfg.radians / total))
					- 60 * Math.sin(i * cfg.radians / total); })
			.attr("y", function(d, i){
				return radius * (1 - Math.cos(i * cfg.radians / total))
					- 20 * Math.cos(i * cfg.radians / total); });

		var ticks = [];
		for (i = 0; i < axes.length; i++) {
			for (j = 0; j < axes[i].ticks; j++) {
				var fraction = j / (axes[i].init - 1) / cfg.maxLg;
				var x = radius * (1 - fraction * Math.sin(i * cfg.radians / total));
				var y = radius * (1 - fraction * Math.cos(i * cfg.radians / total));
				var init = false;
				if (axes[i].init == j + 1) {
					init = true;
				}
				var title = axes[i].titles[j];
				ticks.push({ axe: i, tick: j, x: x, y: y, init: init, title: title });
			}
		}

		var initTicks = [];
		var sommets = "";
		for (i = 0; i < ticks.length; i++) {
			if (ticks[i].init) {
				initTicks.push(ticks[i]);
				sommets += ticks[i].x + ", " + ticks[i].y + " ";
			}
		}

		g.selectAll(".areaInit")
			.data([sommets])
			.enter()
			.append("polygon")
			.attr("class", "areaInit")
			.style("stroke-width", "2px")
			.style("stroke", "grey")
			.attr("points", sommets)
			.style("fill", "grey")
			.style("fill-opacity", cfg.opacityArea)
			.append("svg:title")
			.text("Etape 0");

		g.selectAll(".nodeInit")
			.data(initTicks)
			.enter()
			.append("svg:circle")
			.attr("class", "nodeInit")
			.attr('r', cfg.radiusSommet)
			.attr("alt", function(j) { return j.title; })
			.attr("cx", function(j) { return j.x; })
			.attr("cy", function(j, i) { return j.y; })
			.attr("data-id", function(j) { return j.axis })
			.style("fill", "grey")
			.style("fill-opacity", .9)
			.append("svg:title")
			.text(function(j) { return j.title });

		// Tooltip
		tooltip = g.append('text')
			.style('opacity', 0)
			.style('font-family', 'sans-serif')
			.style('font-size', '13px');

		var polygonTicks = [];
		var polygones = [];
		for (i = 0; i < data.length; i++) {
			var pTicks = [];
			var sommets = "";
			for (j = 0; j < data[i].length; j++) {
				for (k = 0; k < ticks.length; k++) {
					if (ticks[k].axe == j && ticks[k].tick == data[i][j]) {
						pTicks.push(ticks[k]);
						sommets += ticks[k].x + ", " + ticks[k].y + " ";
					}
				}
			}
			polygonTicks.push(pTicks);
			polygones.push(sommets);
		}
		g.selectAll(".area")
			.data(polygones)
			.enter()
			.append("polygon")
			.attr("class", "area")
			.style("stroke-width", "2px")
			.style("stroke", function(j, i) { return cfg.color(i); })
			.attr("points", function(j) { return j; })
			.style("fill", function(j, i) { return cfg.color(i); })
			.style("fill-opacity", cfg.opacityArea)
			.append("svg:title")
			.text(function(j, i) { return "Etape " + (i + 1); });

		for (i = 0; i < data.length; i++) {
			g.selectAll(".node" + i)
				.data(polygonTicks[i])
				.enter()
				.append("svg:circle")
				.attr("class", "node" + i)
				.attr('r', cfg.radiusSommet)
				.attr("alt", function(j) { return j.title; })
				.attr("cx", function(j) { return j.x; })
				.attr("cy", function(j, i) { return j.y; })
				.attr("data-id", function(j) { return j.axis })
				.style("fill", cfg.color(i))
				.style("fill-opacity", .9);
		}

		g.selectAll(".tick")
			.data(ticks)
			.enter()
			.append("svg:circle")
			.attr("class", "tick")
			.attr('r', cfg.radiusTick)
			.attr("alt", function(j) { return j.title; })
			.attr("cx", function(j) { return j.x; })
			.attr("cy", function(j) { return j.y; })
			.attr("data-id", function(j) { return j.axis })
			.style("fill", "black")
			.style("fill-opacity", .9)
			.append("svg:title")
			.text(function(j) { return j.title });

	}

};