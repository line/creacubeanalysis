const sprintf = require('sprintf-js').sprintf;
const mongo = require('mongodb').MongoClient ;
const url = "mongodb://localhost" ;
const dbName = "Creacube" ;
const collInName = "Participations";
const collOutName = "Videos";

mongo.connect (url, function (error, client) {
	if (error)
		throw error ;
	console.log("Connecté à la base de données") ;

	const db = client.db(dbName);
	const collIn = db.collection(collInName);
	const collOut = db.collection(collOutName);

	/*
	collIn.find()
		.sort( { videoName: 1 } )
		.forEach(function(d) {
			console.log(d.videoName + "|" + d.idParticipant + "|" + d.idTeam);
		});
	*/

	var idVideo = -1;
	var videoName = "";
	var listeParticipants = [];
	collIn.find()
	.sort( { idParticipant: 1 } )
	.forEach(function(d) {
		console.log("Participation " + d.idParticipant + " - " + d.videoName +
				" - idVideo = " + idVideo + ", videoName = " + videoName + ", " + listeParticipants.length);
		if (d.videoName == "" || d.videoName != videoName) {
			if (idVideo > -1) {
				console.log("ajoutVideo(" + idVideo + ", " + videoName + ", " + listeParticipants.length);
				ajoutVideo(collOut, idVideo, videoName, listeParticipants);
			}
			idVideo++;
			videoName = d.videoName;
			listeParticipants = [];
		}
		listeParticipants.push(d);
		
	}).then(function() {
		console.log("ajoutVideo(" + idVideo + ", " + videoName + ", " + listeParticipants.length);
		ajoutVideo(collOut, idVideo, videoName, listeParticipants);
	});


	/*
	*/
});

function ajoutVideo(collOut, idVideo, videoName, listeParticipants) {
//	console.log("ajoutVideo(" + idVideo + ", " + videoName + ", " + JSON.stringify(listeParticipants));
	var nbParticipants = 0;
	var idTeam = "";
	var idTeamHistory = "";
	var idTeamCloserness = "";
	var idContext = "";
	var date_yyyymmdd = "";
	var videoUrl = "";
	var participants = [];
	var first = true;

	listeParticipants.forEach(function(p) {
		if (first) {
			nbParticipants = p.nbParticipants;
			idTeam = p.idTeam;
			if (idTeam !== undefined &&
					idTeam.length > 3 &&
					idTeam.substring(0, 3) == "bin") {
				idTeam = sprintf("bin%04d", idTeam.substring(3));
			}
			idTeamHistory = p.idTeamHistory;
			idTeamCloserness = p.idCloserness;
			idContext = p.idContext;
			date_yyyymmdd = p.date_yyyymmdd;
			videoUrl = p.videoUrl;
			first = false;
		} else {
			if (p.nbParticipants != nbParticipants) {
				console.log("participant " + p.idParticipant + ", nbParticipants = " +
						p.nbParticipants + " différent de " + nbParticipants);
			}
			if (p.idTeam != idTeam) {
				console.log("participant " + p.idParticipant + ", idTeam = " +
						p.idTeam + " différent de " + idTeam);
			}
			if (p.idTeamHistory != idTeamHistory) {
				console.log("participant " + p.idParticipant + ", idTeamHistory = " +
						p.idTeamHistory + " différent de " + idTeamHistory);
			}
			if (p.idTeamCloserness != idTeamCloserness) {
				console.log("participant " + p.idParticipant + ", idTeamCloserness = " +
						p.idTeamCloserness + " différent de " + idTeamCloserness);
			}
			if (p.idContext != idContext) {
				console.log("participant " + p.idParticipant + ", idContext = " +
						p.idContext + " différent de " + idContext);
			}
			if (p.date_yyyymmdd != date_yyyymmdd) {
				console.log("participant " + p.idParticipant + ", date_yyyymmdd = " +
						p.date_yyyymmdd + " différent de " + date_yyyymmdd);
			}
			if (p.videoUrl != videoUrl) {
				console.log("participant " + p.idParticipant + ", videoUrl = " +
						p.videoUrl + " différent de " + videoUrl);
			}
		}
		participants.push(
			{
				"idParticipant": "p0" + p.idParticipant.substring(1),
				"nameParticipant": p.nameParticipant,
				"age": p.age,
				"ageCategory": p.ageCategory,
				"gender": p.gender,
				"profession": p.profession,
				"profileTech": p.profileTech,
				"diversity": p.diversity,
				"leftHanded": p.leftHanded,
				"participationOK": true,
				"commentaire": "",
				"clicks": inArray(p.clicks, p.idParticipant)
			});
	});

	var video = {
		"idVideo": sprintf("v%04d", idVideo),
		"nbParticipants": nbParticipants,
		"idTeam": idTeam,
		"idTeamHistory": idTeamHistory,
		"idTeamCloserness": idTeamCloserness,
		"idContext": idContext,
		"date_yyyymmdd": date_yyyymmdd,
		"videoUrl": videoUrl,
		"videoName": videoName,
		"videoOK": true,
		"commentaire": "",
		"participants": participants 
		}

	collOut.insertOne(video).then(function(wr) {
		console.log("Enregistrement vidéo : " + JSON.stringify(video) +
				" - résultat = " + JSON.stringify(wr));
	});
}

function liste(listeParticipants) {
	var sep = "";
	var result = "[ ";
	listeParticipants.forEach(p =>
		{
			idTeam = p.idTeam;
			if (idTeam !== undefined &&
					idTeam.length > 3 &&
					idTeam.substring(0, 3) == "bin") {
				idTeam = sprintf("bin%04d", idTeam.substring(3));
			}
			result = result + sep +
				"p0" + p.idParticipant.substring(1) +
				"( " + idTeam + " )";
			sep = ", "
		}
	);
	result += " ]";
	return result;
}

function inArray(cs, idParticipant) {
//	console.log("inArray participant " + idParticipant);
	var clicks = [];
		if (cs != null) {
		cs.forEach(function(c) {
			clicks.push({
				"time": c.time,
				"tclicks": [c.click]
			});
		});
	}
//	console.log("inArray participant " + idParticipant + " - clicks = " + clicks);
	return clicks;
}
