const fs = require('fs'); // Module intégré Node.js pour la gestion du système de fichiers
const path = require('path'); // Module intégré Node.js pour la gestion des chemins des fichiers
const sprintf = require('sprintf-js').sprintf;
const async = require('async');
let cas_user;


//Paramétrage général de l'application
properties = JSON.parse(fs.readFileSync(__dirname + '/../model/properties.json'));

// Fonctions utilitaires
const Utils = require(__dirname + '/../model/Utils.js');
const utils = new Utils();

//Journalisation des événements
const log4js = require('log4js');
log4js.configure({
	appenders: {
		creacube: {
			type: 'file',
			filename: properties.logPath + 'creacube.log',
			maxLogSize: 512 * 1024,
			layout: {
				type: 'pattern',
				pattern: '%d %p %m | %f %l%n'
			}
		},
		console: {
			type: 'stdout',
			layout: {
				type: 'pattern',
				pattern: '%d %p %m | %f %l%n'
			}
		}
	},
	categories: {
		default: { "appenders": [ 'creacube', 'console' ], "level": properties.logLevel }
	}
});
var logger = log4js.getLogger();
//log4js.shutdown(function() {
//	logger.info("Arrêt du serveur");
//});

//Authentification avec CAS v3
const CasAuthentication = require('node-cas-authentication');
var cas = new CasAuthentication({
	cas_url: properties.casUrl,
	service_url: properties.protocol + '://' + properties.host + ':' + properties.port,
	cas_version: '3.0',
	renew: false,
	//is_dev_mode: false for deployment mode
	is_dev_mode: true,
	dev_mode_user: '',
	dev_mode_info: {},
	session_name: 'cas_user',
	session_info: 'cas_userinfo',
	destroy_session: false,
	return_to: '/login'
});

const express = require ('express'); // TODO
var app = express();

const https = require(properties.protocol); // Module intégré Node.js pour la gestion du HTTPS
if(https === require('https')) {
	var privateKey  = fs.readFileSync(properties.privateKey, 'utf8');
	var certificate = fs.readFileSync(properties.certificate, 'utf8');
	var credentials = {key: privateKey, cert: certificate};
	var httpsServer = https.createServer(credentials, app);
} else {
	var httpsServer = https.createServer(app);
}


// Gestion de la session
// En développement, la session est stockée en mémoire
// En production, la session est stockée dans le système de fichiers
const session = require('express-session');
const MemoryStore = require('session-memory-store')(session);
const FileStore = require('session-file-store')(session);
if (properties.host == 'localhost') {
	var store = new MemoryStore();
} else {
	var fileStoreOptions = {};
	var store = new FileStore(fileStoreOptions)
}

// Moteur de template Nunjucks
const nunjucks = require('nunjucks');
nunjucks.configure(['views/'], {
	autoescape: true, 
	express: app
});

// Upload des vidéos
var multer = require('multer');
var upload = multer({ dest: __dirname + '/../upload/' });

// TODO
const archiver = require('archiver');

const url = require('url'); // Module intégré Node.js pour parser les URL

const bodyParser = require('body-parser'); // TODO

var multer  = require('multer')
var upload = multer({ dest: 'upload/' });

// Routage des requêtes
app.use(session( {
		secret: "pompomgirl",
		resave: false,
		saveUninitialized: true,
		cookie: { secure : true },
		store: store
		} ))
	.use(bodyParser.json())
	.use(bodyParser.urlencoded( { extended: true } ))
	.use(express.static("files/"))
	.use(express.static("zip/")) // Ajout
	.use(express.static(properties.videosFolder))
	.get('/login', cas.bounce, function(req, res) {
		logger.info("GET /login - " + req.session.cas_user);
		loginResponse(req, res, logger);
		})
	.get('/logout', cas.block, cas.logout)
	.get('/archive', cas.bounce, function (req, res) { // Ajout
		logger.info("GET /archive - " + req.session.cas_user);
		getZip(req, res, logger);
		})
	.get('/videoTreatment.html', cas.block, function (req, res) {
		logger.info("GET /videoTreatment.html - " + req.session.cas_user);
		getTreatVideo(req, res, logger);
		})
	.get('/', cas.bounce, function (req, res) {
		logger.info("GET / - " + req.session.cas_user);
		cas_user = req.session.cas_user
		get(req, res, logger);
		})
	.get('/zip/*', cas.block, function (req, res) {
		logger.info("GET /zip - " + req.session.cas_user);
		// TODO Le blocage n'est pas effectif pour les utilisateurs non connectés
		file = '';
		s = req.url.split('/');
		if (s.length > 0) {
				file = s[s.length - 1];
				logger.info('wanted zip = ' + file + ' - ' + req.session.cas_user);
				if (file.endsWith('.zip')) {
					var zipPath = '/datas/creamaker/creacube/www/zip/' + file;
					res.sendFile(zipPath, {'root': ''});

				}}

		})
	.post('/',cas.block, function (req, res) {
		logger.info("POST / - " + req.session.cas_user);
		post(req, res, logger);
		})
	.post('/addVideo', cas.block, upload.single('upload'), function (req, res) {
		// TOD vérifier extended: false
		logger.info("POST /addVideo - " + req.session.cas_user);
		postAddVideo(req, res, logger);
		})
	.get('/participation', cas.block, function (req, res) {
		// TOD vérifier extended: false
		logger.info("POST /participation - " + req.session.cas_user);
		getParticipation(req, res, logger);
		})
	.get('/getTeam',cas.block, function(req,res){
		logger.info("GET / Team - "+ req.session.cas_user);
		getTeam(req, res, logger)
	})
	.get('/getShapeFamily',cas.block, function(req,res){
		logger.info("GET / Shape - "+ req.session.cas_user);
		getShapeFamily(req, res, logger)
	})
	.get('/getCombinationExists',cas.block, function(req,res){
		logger.info("GET / Shape - "+ req.session.cas_user);
		getCombinationExists(req, res, logger)
	})
	.get('/getLastId',cas.block, function(req,res){
		logger.info("GET / Team - "+ req.session.cas_user);
		getLastId(req, res, logger)
	})
	.get('/getUpdatedPage',cas.block, function(req,res){
		logger.info("GET / Videos - "+ req.session.cas_user);
		getUpdatedPage(req, res, logger)
	})
	.post('/saveClicks', cas.block, function (req, res) {
		logger.info("POST /saveClicks - " + req.session.cas_user);
		saveClicks(req, res, logger);
		})
	.post('/saveParticipation', cas.block, function (req, res) {
		logger.info("POST /saveParticipation - " + req.session.cas_user);
		saveParticipation(req, res, logger);
		})
	.post('/saveLastId', cas.block, function (req, res) {
		logger.info("POST /saveLastId - " + req.session.cas_user);
		saveLastId(req, res, logger);
	})
	.post('/modifyOK', cas.block, function(req,res) {
		logger.info("POST /modifyOK - " + req.session.cas_user);
		saveOK(req, res, logger);
	})
	.post('/deleteUser', cas.block, function (req, res) {
		logger.info("POST /deleteUser - " + req.session.cas_user);
		deleteUser(req, res, logger);
	})
	.post('/addUser', cas.block, function (req, res) {
		logger.info("POST /addUser - " + req.session.cas_user);
		addUser(req, res, logger);
	})
	.post('/addShape', cas.block, function (req, res) {
		logger.info("POST /addShape - " + req.session.cas_user);
		addShape(req, res, logger);
	})
	.post('/listParticipations', cas.block, function (req, res) {
		logger.info("POST /listParticipations - " + req.session.cas_user);
		listParticipations(req, res, logger);
		})
	.post('/deleteVideo', cas.block, function (req, res) {
		logger.info("POST /deleteVideo - " + req.session.cas_user);
		deleteVideoConfirmed(req, res, logger);})
	.use(function (req, res, next) {
		logger.debug("unplanned route - " + req.session.cas_user + " - originalUrl : " + req.originalUrl);
		res.setHeader('Content-Type', 'text/plain;charset=UTF-8');
		res.status(404).send("This page doesn't exist.");
		});

// Lancement du serveur HTTPS
httpsServer.listen(properties.port, function() {
	logger.info('The server is running on the port ' + properties.port);
});

//Paramétrage de la base de données
const dbProperties = JSON.parse(fs.readFileSync(__dirname + '/../persistence/dbProperties.json'));

// Persistence en base de données
var DAO = require(__dirname + '/../persistence/MongoDAO.js');
var dao =  new DAO(dbProperties);

// Données métier
var Video = require(__dirname + '/../model/Video.js');
var Participation = require(__dirname + '/../model/Participation.js');

/*
 * TODO
 */
function loginResponse(req, res, logger) {
	var userId = req.session.cas_user;
	dao.userExists(userId, function(existe) {
		if (existe) {
			var referer = req.headers.referer;
			var query = decodeURIComponent(url.parse(referer).query);
			var serviceUrl = '/';
			var params = query.split('&');
			for (par in params) {
				p = params[par].split('=');
				if (p[0] == 'service') {
					serviceUrl = p[1];
				}
			}
			logger.info("Succeeded connection - " + userId);
			res.redirect(302, serviceUrl);
		} else {
			logger.info("Connection failed- " + userId);
			res.send("You aren't authorized to access this page");
			res.end();
		}
		return;
	});
}

/*
 * TODO
 */
function get(req, res, logger) {
	var params = {};
	var idMenu = "";
	if (typeof req.body != "undefined") {
		for (var p in req.body) {
			params[p] = req.body[p];
			if (p == "idMenu") {
				idMenu = req.body[p];
			}
		}
	}
	
	var content = "content" + idMenu + ".html";
	var context = {
		host: req.hostname,
		session: req.session,
		properties: properties,
		params: params,
		content: content
		};

	switch (idMenu) {
	case "VideosMenu":

		dao.countVideos(logger,cas_user, function (pages){

			context.nbPage = pages

		});

		dao.listLastIds(logger, cas_user, function(lastIds){
			context.lastIds = lastIds;
		});


		dao.listVideos({}, {},100, 0,logger, cas_user, function(videos) {
			videos.forEach(function(v) {
				v.dateJMA = utils.dateJMA(v.date_yyyymmdd);
			/*dao.removeAll(videos, logger, cas_user)

			videos.forEach(function(v) {
				dao.addVideo(v.idVideo, v, logger, cas_user, function(video){
					//logger.info(video)
				})*/
			});
			context.videos = videos;
			renderContent(res,context);
		});
		break;
	case "ChartsMenu":

		dao.listShapes(logger, cas_user, function (shapes) {
			context.shapes = shapes
		})

		dao.listParticipations({}, {} ,logger, cas_user, function(participations) {
			context.participations = participations;
			renderContent(res, context);	
		});
		break;
	case "ResultsMenu":
		let jsonVideos = []; 
		dao.listVideosWithResults({}, {}, logger, cas_user, function(videos) {
			videos.forEach(function(v) {
				v.dateJMA = utils.dateJMA(v.date_yyyymmdd);
				jsonVideos.push(JSON.stringify(v, null, 4));
			});
			const Participation = require(__dirname + '/../model/Participation.js');
			context.listeFields = new Participation().getFields();
			context.videos = videos;
			context.jsonVideos = jsonVideos;
			renderContent(res, context);	
		});
		break;
	case "FeedbackMenu":
		dao.listParticipations({}, {}, logger, cas_user, function(participations){
			context.participations = participations;
			renderContent(res, context)
		});
		break;
		case "StatisticMenu":
			dao.listParticipations({}, {}, logger, cas_user, function(participations){
				context.participations = participations;
				renderContent(res, context)
			});
			break;
		case "BehaviorMenu":
			dao.listParticipations({}, {}, logger, cas_user, function(participations){
				context.participations = participations;
				renderContent(res, context)
			});
			break;
		case "UserMenu":
			dao.getUser(cas_user,function (user){
				if(user.admin !== undefined)
				{
					context.hostStatus = user.admin
				}
		})
			dao.listUsers( logger, cas_user, function(users) {
				context.users = users;
				renderContent(res, context);
			});
			break;
		default:
		renderContent(res, context);	
		break;
	}
}

function renderContent(res, context) {
	res.setHeader('Content-Type', 'text/html;charset=UTF-8');
	res.render('index.html', context);	
}

/*
 * TODO
 */
function getTreatVideo(req, res, logger) {
	var id = '';
	for (var p in req.query) {
		if (p == "id") {
			id = req.query[p];
		}
	}
	if (id == 'new') {
		id = '';
	}
	dao.selectVideoById(id, logger, cas_user, function(video) {
		video.dateJMA = utils.dateJMA(video.date_yyyymmdd);
		var context = {
			host: req.hostname,
			session: req.session,
			properties: properties,
			content: "videoTreatment.html",
			video: video,
			videoWidth: 640,
			videoHeight: 480
		};
		res.setHeader('Content-Type', 'text/html;charset=UTF-8');
		res.render('videoTreatment.html', context);
	});
}

/*
 * TODO
 */
function getZip(req, res, logger) { // Ajout
	dao.listParticipations({}, {}, logger, cas_user, function(participations) {
		var now = new Date();
		var dateAMJhm = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() +
				'-' + now.getHours() + '-' + now.getMinutes();
		zipFile = 'creacube-' + dateAMJhm + '.zip';
		var output = fs.createWriteStream('zip/' + zipFile);
		var archive = archiver('zip', {
			zlib: { level: 9 } // Sets the compression level.
		});
		 
		// listen for all archive data to be written
		// 'close' event is fired only when a file descriptor is involved
		output.on('close', function() {
			var zipUrl = "/zip/" + zipFile;
			var context = {
					host: req.hostname,
					session: req.session,
					properties: properties,
					zipUrl: zipUrl,
					zipFile: zipFile
				};
				res.setHeader('Content-Type', 'text/html;charset=UTF-8');
				res.render('archive.html', context);	
		});
		 
		// This event is fired when the data source is drained no matter what was the data source.
		// It is not part of this library but rather from the NodeJS Stream API.
		// @see: https://nodejs.org/api/stream.html#stream_event_end
		output.on('end', function() {
			console.log('Data has been drained');
		});
		
		// good practice to catch warnings (ie stat failures and other non-blocking errors)
		archive.on('warning', function(err) {
			if (err.code === 'ENOENT') {
			// log warning
				console.log(err);
			} else {
				// throw error
				throw err;
			}
		});

		// good practice to catch this error explicitly
		archive.on('error', function(err) {
		  throw err;
		});

		// pipe archive date to the file
		archive.pipe(output);

		for (p in participations) {
			archive.append(JSON.stringify(participations[p]), { name: participations[p].participants.idParticipant + '.json' });
		}

		// Finalize the archive
		archive.finalize();
	});
}

/*
 * TODO
 */
function post(req, res, logger) {
	get(req, res, logger);
}

/*
 * TODO
 */
function getParticipation(req, res, logger) {
	if (typeof req.query.id != "undefined") {
		dao.selectParticipationById(req.query.id, logger, cas_user, function(participation) {
			res.json(participation);
		});
	}
}

function getTeam(req, res, logger)
{
	dao.findTeam(req.query.idVideo, req.query.idTeam, req.query.idParticipant,  logger, cas_user, function(participationTeam) {
		res.json(participationTeam);
	});
}

function getLastId(req, res, logger) {
	if (req.query.idType === "idVideo") {
		dao.getLastIdVideo(req.query.idType, logger, cas_user, function (idVideo) {
			res.json(idVideo);
		});
	} else if (req.query.idType === "idParticipant") {
		dao.getLastIdParticipant(req.query.idType, logger, cas_user, function (idParticipant) {
			res.json(idParticipant);
		});

	} else if (req.query.nbParticipants != null) {
		dao.getLastIdTeam(req.query.nbParticipants, logger, cas_user, function (idTeam) {
			res.json(idTeam);
		});
	}
}

function getUpdatedPage(req,res,logger) {
	let skip = (req.query.page) * 100
	dao.listVideos({},{},100,skip, logger,cas_user, function(videos) {
		videos.forEach(function(v) {
			v.dateJMA = utils.dateJMA(v.date_yyyymmdd);})
		res.send(videos);})
}

/*
 * TODO
 */
function postAddVideo(req, res, logger) {
	let erreurs = [];

	let idVideo = sprintf("v%04d", parseInt(req.body.idVideo));
	let nbParticipants = req.body.nbParticipants;
	let idTeamBin = "";
	let idTeamTri = "";
	let idTeam = "";
	let idParticipant1 = sprintf("p%04d", parseInt(req.body.idParticipant1));
	let idParticipant2 = "";
	let idParticipant3 = "";
	if (nbParticipants == 1) {
		idTeam = "ind";
	}
	if (nbParticipants > 1) {
		idParticipant2 = sprintf("p%04d", parseInt(req.body.idParticipant2));
	}
	if (nbParticipants == 2) {
		idTeamBin = sprintf("bin%04d", parseInt(req.body.idTeamBin));
		idTeam = idTeamBin;
	}
	if (nbParticipants == 3) {
		idTeamTri = sprintf("tri%04d", parseInt(req.body.idTeamTri));
		idTeam = idTeamTri;
		idParticipant3 = sprintf("p%04d", parseInt(req.body.idParticipant3));
	}
	let idTeamHistory = req.body.idTeamHistory;
	let idTeamCloserness = req.body.idTeamCloserness;
	let idContext = req.body.idContext;
	let date = req.body.date;
	if (date !== undefined && date != "") {
		date = date.substr(6, 4) + date.substr(3, 2) + date.substr(0, 2);
	}
	let videoOK = (req.body.videoOK === 'true');
	let activity1 = (req.body.activity1 === 'true');
	let activity2 = (req.body.activity2 === 'true');
	let activity3 = (req.body.activity3 === 'true');
	let commentaire = req.body.commentaire;
	let participants = [];

	async.series([
		function(callback) {
			dao.checkAvailableId("v", idVideo, logger, cas_user, function(dispo) {
				if (!dispo) {
					erreurs.push("L'identifiant de vidéo est déjà utilisé");
				}
				callback();
			});
		},
		function(callback) {
			if (nbParticipants == 2) {
				dao.checkAvailableId("bin", idTeamBin, logger, cas_user, function(dispo) {
					if (!dispo) {
						erreurs.push("L'identifiant de binôme est déjà utilisé");
					}
					callback();
				});
			} else {
				callback();
			}
		},
		function(callback) {
			if (nbParticipants == 3) {
				dao.checkAvailableId("tri", idTeamTri, logger, cas_user, function(dispo) {
					if (!dispo) {
						erreurs.push("L'identifiant de trinôme est déjà utilisé");
					}
					callback();
				});
			} else {
				callback();
			}
		},
		function(callback) {
			dao.checkAvailableId("p", idParticipant1, logger, cas_user, function(dispo) {
				if (!dispo) {
					erreurs.push("L'identifiant du premier participant est déjà utilisé");
				}
				callback();
			});
		},
		function(callback) {
			if (nbParticipants > 1) {
				dao.checkAvailableId("p", idParticipant2, logger, cas_user, function(dispo) {
					if (!dispo) {
						erreurs.push("L'identifiant du deuxième participant est déjà utilisé");
					}
					callback();
				});
			} else {
				callback();
			}
		},
		function(callback) {
			if (nbParticipants > 2) {
				dao.checkAvailableId("p", idParticipant3, logger, cas_user, function(dispo) {
					if (!dispo) {
						erreurs.push("L'identifiant du troisième participant est déjà utilisé");
					}
					callback();
				});
			} else {
				callback();
			}
		}
	],
	function() {
		if (erreurs.length > 0 ) {
			res.send(erreurs);
		} else {
			//var extension = req.file.originalname.substring(req.file.originalname.length - 4);
			var extension =".mp4"
			var videoName = idVideo + "-" + idParticipant1;
			if (nbParticipants == 2) {
				videoName += "-" + idParticipant2 + "-" + idTeamBin;
				idTeam = idTeamBin;
			}
			if (nbParticipants == 3) {
				videoName += "-" + idParticipant2 + "-" + idParticipant3 + "-" + idTeamTri ;
				idTeam = idTeamTri;
			}
			videoName += extension
			var newPath = properties.videosFolder + "/" + videoName ;


			fs.copyFile(req.file.path, newPath, function(err){
				if (err) {
					console.log("Erreur déplacement vidéo");
					console.log(err);
					res.send([ "La vidéo n'a pas été chargée" ]);
				} else {
					participants.push(new Participation([], "", idParticipant1, ""));
					if (nbParticipants > 1) {
						participants.push(new Participation([], "", idParticipant2, ""));
					}
					if (nbParticipants == 3) {
						participants.push(new Participation([], "", idParticipant3, ""));
					}
					
					video = new Video(
						idVideo,
						nbParticipants,
						idTeam,
						idTeamHistory,
						idTeamCloserness,
						idContext,
						date,
						"", //videoUrl
						videoName,
						videoOK,
						activity1,
						activity2,
						activity3,
						commentaire,
						participants
					);
					dao.addVideo(req.body.idVideo, video,
							logger, cas_user, function() {
						res.send(erreurs);
		//				req.body.idMenu = "MVideos";
		//				get(req, res, logger);
					})
				}
			});
		
		}
	});

}


function saveClicks(req, res, logger) {

	if (req.body.clicks === undefined){
		req.body.clicks = []
	}
	dao.saveClicks(req.body.idVideo, req.body.idParticipant, req.body.clicks,
			logger, cas_user, function(clicks) {
					idParticipant = req.body.idParticipant;
			res.send({ idParticipant: idParticipant, clicks: clicks });
	});
}

function saveParticipation(req, res, logger) {
	dao.saveParticipation(req.body.idVideo,
		JSON.parse(req.body.participation),
		logger,
		cas_user,
		function(resultat) {
			res.send(resultat);
		}
	);
}

function listParticipations(req, res, logger) {
	dao.listParticipations(
		dao.makeFiltres(req.body.filtres),
		{},
		logger,
		cas_user,
		function(participations) {
			res.send(participations);
		});
}

function saveLastId(req, res, logger) {
	dao.saveLastId(
		req.body.lastId,
		logger,
		cas_user,
		function(resultat) {
			res.send(resultat);
		}
	);
}

function getShapeFamily(req, res, logger) {
	dao.getShapeFamily(
		req.query.family,
		logger,
		cas_user,
		function(combinations) {
			res.send(combinations);
		})
}

function saveOK(req, res, logger) {

	dao.saveOK(req.body.idVideo,
		req.body.OKType,
		req.body.newValue,
		logger,
		cas_user)

}

function deleteUser(req, res, logger) {
	dao.deleteUser(
		req.body.userId,
		logger,
		cas_user,
		function(resultat) {
			res.send(resultat);
		}
	);
}

function addUser(req, res, logger) {
	dao.addUser(
		req.body.user,
		logger,
		cas_user,
		function(resultat) {
			res.send(resultat);
		}
	);
}

function addShape(req, res, logger) {
	dao.addShape(
		req.body.shape,
		logger,
		req.session.cas_user,
		function(resultat) {
			res.send(resultat);
		}
	);
}

function getCombinationExists(req, res, logger) {
	dao.combinationExists(
		req.query.shape,
		function(resultat) {
			res.send(resultat);
		}
	);
}

function deleteVideoConfirmed(req, res, logger){

	dao.deleteVideo(
		req.body.video.idVideo,
		logger,
		req.session.cas_user,
		function(result){
			var pathVideo = properties.videosFolder + "/" + req.body.video.videoName;

			fs.unlink(pathVideo,(err) => {
				if (err) throw err;
				console.log('Fichier supprimé !');
			});
		})
}
