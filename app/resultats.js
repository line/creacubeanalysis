const path = require('path');
const express = require ('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const fs = require('fs');
const MemoryStore = require('session-memory-store')(session);
const async = require('async');

properties = JSON.parse(fs.readFileSync(__dirname + '/../model/properties.json'));
const dbProperties = JSON.parse(fs.readFileSync(__dirname + '/../persistence/dbProperties.json'));

//Journalisation des événements
const log4js = require('log4js');
log4js.configure({
	appenders: {
		file: {
			type: 'file',
			filename: properties.logPath + '/results.log',
			maxLogSize: 512 * 1024,
			layout: {
				type: 'pattern',
				pattern: '%d %p %m | %f %l%n'
			}
		},
		console: {
			type: 'stdout',
			layout: {
				type: 'pattern',
				pattern: '%d %p %m | %f %l%n'
			}
		}
	},
	categories: {
		default: { appenders: [ 'file', 'console' ], level: properties.logLevel, enableCallStack: true }
	}
});
var logger = log4js.getLogger();

var DAO = require(__dirname + '/../persistence/MongoDAO.js');
var dao =  new DAO(dbProperties);

var app = express();

var env = nunjucks.configure(['views/'], {
	autoescape: true, 
	express: app
});

var store = new MemoryStore();

app.use(session( {
		secret: "pompomgirl",
		resave: false,
		saveUninitialized: true,
		cookie: { secure : true },
		store: store
		} ))
	.use(bodyParser.json())
	.use(bodyParser.urlencoded( { extended: true } ))
	.use(express.static("files/"))
	.get('/', function (req, res) {
		get(req, res);
		})
	.use(function (req, res, next) {
		res.setHeader('Content-Type', 'text/plain;charset=UTF-8');
		res.status(404).send("Cette page n'existe pas.");
		})
	.listen(80, function() {
		console.log('Le serveur tourne sur le port 80');
	});

function get(req, res) {
	var params = {};
	var content = "contentResultsMenu.html";
	var context = {
		host: req.hostname,
		session: req.session,
		properties: properties,
		params: params,
		content: content
		};
	dao.listVideosWithResults({}, {}, logger, "", function(videos) {
		const Participation = require(__dirname + '/../model/Participation.js');
		context.listeFields = new Participation().getFields();
		context.videos = videos;
		res.setHeader('Content-Type', 'text/html;charset=UTF-8');
		res.render('index.html', context);	
	});
}
