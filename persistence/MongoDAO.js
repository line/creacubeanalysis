const mongo = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const { promisify } = require('util');

var MongoDAO = function(dbProperties) {
	this.dbProperties = dbProperties;
	var url = this.dbProperties.url;
	var dbName = this.dbProperties.dbName;

	var collection = this.dbProperties.videosCollection;

	// Requêtes sur la table Users
	this.listUsers = function(logger, userId, callback) {
		var collection = this.dbProperties.usersCollection;
		var User = require(__dirname + '/../model/Users.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var users = [];
			coll.find()
				.sort( { userId: 1 } )
				.forEach(function (d) {
					users.push(new User(d.userId, d.name, d.firstName, d.admin));
				})
				.then(function() {
					callback(users);
				});
		});
	}

	this.getUser = function(userId, callback) {
		var collection = this.dbProperties.usersCollection;
		var User = require(__dirname + '/../model/Users.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var user;
			coll.find( { userId: userId } )
				.forEach(function (d) {
					user = new User(d.userId, d.name, d.firstName, d.admin);
				})
				.then(function() {
					callback(user);
				});
		});
	}

	this.userExists = function(userId, callback) {
		var collection = this.dbProperties.usersCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var existe = false;
			coll.find( { userId: userId } )
				.forEach(function (d) {
					existe = true;
				})
				.then(function() {
					callback(existe);
				});
		});
	}

	this.deleteUser = function(idUser, logger, userId, callback) {
		logger.info("deleteUser(" + JSON.stringify(idUser) + ") - " + userId);
		var collection = this.dbProperties.usersCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.deleteOne(
				{ "userId": idUser }
			).then(function(wr) {
				logger.info("deleteUser(" + JSON.stringify(idUser) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(wr.result);
			}).catch(function(error) {
				logger.error("deleteUser(" + JSON.stringify(idUser) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.addUser = function(user, logger, userId, callback) {
		logger.info("addUser(" + JSON.stringify(user.userId) + ") - " + userId);
		var collection = this.dbProperties.usersCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.insertOne(
				{ "userId": user.userId,
					"name": user.name,
					"firstName": user.firstName,
					"admin": false},
			).then(function(wr) {
				logger.info("addUser(" + JSON.stringify(user) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(wr.result);
			}).catch(function(error) {
				logger.error("AddUser(" + JSON.stringify(user) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	// Requêtes sur la table LastId
	this.listLastIds = function(logger, userId, callback) {
		logger.info("listLastIds - " + userId);
		var collection = this.dbProperties.lastIdCollection;
		var LastId = require(__dirname + '/../model/LastId.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var lastIds = [];
			coll.find()
				.forEach(function (d) {
					lastIds.push(new LastId(d.nameId, d.id));
				})
				.then(function() {
					callback(lastIds);
				});
		});
	}

	this.countVideos = function (logger, userId, callback){
		logger.info("counVideos - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.find({}).count().then(function(c) {
				let nbPage = c/100
				let remainder = c%100
				remainder !== 0 ? nbPage = (Math.floor(c/100)) + 1 : nbPage = nbPage;
				let pages =[]
				for(let i= 1; i <= nbPage;i++ ){
					pages.push(i)
				}
				callback(pages);
			});
		})
	}


	// Requêtes sur la table Shapes
	this.listShapes = function(logger, userId, callback) {
		logger.info("listShapes - " + userId);
		var collection = this.dbProperties.shapesCollection;
		var Shape = require(__dirname + '/../model/Shapes.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var shapes = [];
			coll.find()
				.forEach(function (d) {
					shapes.push(new Shape(d.family, d.combinations));
				})
				.then(function() {
					callback(shapes);
				});
		});
	}

	this.addShape = function(shape, logger, userId, callback) {
		logger.info("addShape(" + JSON.stringify(shape) + ") - " + userId);
		var collection = this.dbProperties.shapesCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.updateOne(
				{ "family": shape.family},
				{ $push: {combinations: {"order": shape.combination} }},
				{"upsert" : false}
			).then(function(wr) {
				logger.info("addShape(" + JSON.stringify(shape) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(wr.result);
			}).catch(function(error) {
				logger.error("addShape(" + JSON.stringify(shape) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.combinationExists = function(shape, callback) {
		var collection = this.dbProperties.shapesCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var existe = false;
			coll.find( { "family": shape.family,
				combinations: {"order": shape.combination}
			} )
				.forEach(function (d) {
					existe = true;
				})
				.then(function() {
					callback(existe);
				});
		});
	}

	// Requêtes sur la table Shapes
	this.getShapeFamily = function(family, logger, userId, callback) {
		logger.info("getShapeFamily - " +family + " - " + userId);
		var collection = this.dbProperties.shapesCollection;
		var Shape = require(__dirname + '/../model/Shapes.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var shape = {};
			coll.find({
				"family": family
			})
				.forEach(function (d) {
					shape = new Shape(d.family, d.combinations);
				})
				.then(function() {
					callback(shape);
				});
		});
	}

	// Requêtes sur la table Videos
	this.listVideos = function(filtre, ordre,limit, skip, logger, userId, callback) {
		logger.info("listVideos(" + JSON.stringify(filtre) + ", " +
			JSON.stringify(ordre) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		var Video = require(__dirname + '/../model/Video.js');
		var Participation = require(__dirname + '/../model/Participation.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var videos = [];
			coll.find().limit(limit).skip(skip)
				.sort( { idVideo: 1 } )
				.forEach(function (d) {
					participants = [];
					for (p in d.participants) {
						var participation = d.participants[p];
						if(participation.clicks === null)
						{
							participation.clicks = []
							var DAO = require(__dirname + '/../persistence/MongoDAO.js');
							var dao =  new DAO(dbProperties);
							dao.saveClicks(d.idVideo, participation.idParticipant, [],logger, userId, function(result){})
						}
						participants.push(new Participation(
							participation.clicks,
							participation.dataFilename,
							participation.idParticipant,
							participation.nameParticipant,
							parseInt(participation.age, 10),
							participation.ageCategory,
							participation.gender,
							participation.profession,
							participation.profileTech,
							participation.diversity,
							participation.leftHanded,
							participation.cubeletsPriorKnowledge,
							participation.participationOK,
							participation.commentaire
						));
					}
					videos.push(new Video(
						d.idVideo,
						d.nbParticipants,
						d.idTeam,
						d.idTeamHistory,
						d.idTeamCloserness,
						d.idContext,
						d.date_yyyymmdd,
						d.videoUrl,
						d.videoName,
						d.videoOK,
						d.activity1,
						d.activity2,
						d.activity3,
						d.commentaire,
						participants
					));
				})
				.then(function() {
					callback(videos);
				});
		});
	}



	this.removeAll = function(videos,logger, userId, callback) {
		var collection = this.dbProperties.videosCollection;
		var Video = require(__dirname + '/../model/Video.js');
		var Participation = require(__dirname + '/../model/Participation.js');
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error;
			const db = client.db(dbName);
			const coll = db.collection(collection);

			coll.remove(
				{ idVideo: {$lt : 'v1000'} })
				.then(function(wr) {
					callback(wr);
				})

		});
	};

	this.listVideosWithResults = function(filtre, ordre, logger, userId, callback) {
		logger.info("listVideosWithResults(" + JSON.stringify(filtre) + ", " +
			JSON.stringify(ordre) + ") - " + userId);

		this.listVideos(filtre, ordre,0,0, logger, userId, function(videos) {
			for (v in videos) {
				var video = videos[v];
				for (p in video.participants) {
					var participation = video.participants[p];
					participation.results = participation.getResults();
				}
			}
			callback(videos);
		});
	}

	this.selectVideoById = function(idVideo, logger, userId, callback) {
		logger.info("videoSelectionById(" + idVideo + ") - " + userId);
		var Video = require(__dirname + '/../model/Video.js');
		var Participation = require(__dirname + '/../model/Participation.js');
		var video;
		if (idVideo == '') {
			// TODO possible de faire mieux pour la création d'un objet vide ?
			callback(new Video());
		} else {
			var collection = this.dbProperties.videosCollection;
			mongo.connect (this.dbProperties.url, function (error, client) {
				if (error) throw error ;
				const db = client.db(dbName);
				const coll = db.collection(collection);
				coll.find( { "idVideo": idVideo } )
					.forEach(function (d) {
						// TODO possible de faire mieux pour convertir un JSON en objet 
						// avec le prototype de l'objet ?
						participants = [];
						d.participants.forEach(function(p) {
							participation = new Participation(
								p.clicks,
								p.dataFilename,
								p.idParticipant,
								p.nameParticipant,
								p.age,
								p.ageCategory,
								p.gender,
								p.profession,
								p.profileTech,
								p.diversity,
								p.leftHanded,
								p.cubeletsPriorKnowledge,
								p.participationOK,
								p.commentaire
							);
							participants.push(participation);
						});
						video = new Video(
							d.idVideo,
							d.nbParticipants,
							d.idTeam,
							d.idTeamHistory,
							d.idTeamCloserness,
							d.idContext,
							d.date_yyyymmdd,
							d.videoUrl,
							d.videoName,
							d.videoOK,
							d.activity1,
							d.activity2,
							d.activity3,
							d.commentaire,
							participants
						);
					})
					.then(function() {
						callback(video);
					});
			});
		}
	};

	// Requêtes sur la table Participations
	this.makeFiltres = function(filtres) {
		var filtre = {};

		if (filtres.ageMin != 'undefined' && filtres.ageMin != "" && filtres.ageMin != "NaN" && filtres.ageMin != undefined)
		{
			filtre.ageMin = {$gte: parseInt(filtres.ageMin, 10)};
		}

		if (filtres.ageMax != 'undefined' && filtres.ageMax != "" && filtres.ageMax != "NaN" && filtres.ageMax != undefined)
		{
			filtre.ageMax = {$lte: parseInt(filtres.ageMax, 10)};
		}


		var ageCategories = [];
		if (filtres.bebe != 'undefined' && filtres.bebe == 'true') {
			ageCategories.push('beb');
		}
		if (filtres.enfant != 'undefined' && filtres.enfant == 'true') {
			ageCategories.push('enf');
		}
		if (filtres.ado != 'undefined' && filtres.ado == 'true') {
			ageCategories.push('ado');
		}
		if (filtres.jadulte != 'undefined' && filtres.jadulte == 'true') {
			ageCategories.push('jad');
		}
		if (filtres.adulte != 'undefined' && filtres.adulte == 'true') {
			ageCategories.push('adu');
		}
		if (filtres.senior != 'undefined' && filtres.senior == 'true') {
			ageCategories.push('sen');
		}
		if (ageCategories.length > 0) {
			filtre.ageCategory = { $in: ageCategories };
		}

		if (filtres.genreF != 'undefined' && filtres.genreF == 'true') {
			filtre.gender = { $eq: 'F' };
		} else if (filtres.genreM != 'undefined' && filtres.genreM == 'true') {
			filtre.gender = { $eq: 'M' };
		}


		if (filtres.modaliteI != 'undefined' && filtres.modaliteI == 'true') {
			filtre.nbParticipants = { $eq: '1' };
		} else if (filtres.modaliteC != 'undefined' && filtres.modaliteC == 'true') {
			filtre.nbParticipants = { $gt: '1'  };
		}
		else if (filtres.modaliteB != 'undefined' && filtres.modaliteB == 'true') {
			filtre.nbParticipants = { $eq: '2'  };
		}
		else if (filtres.modaliteT != 'undefined' && filtres.modaliteT == 'true') {
			filtre.nbParticipants = { $eq: '3'  };
		}
		if (filtres.idContext !== 'undefined' && filtres.idContext !== "" && filtres.idContext !== undefined)
		{
			filtre.idContext = {$eq: filtres.idContext};
		}

		if (filtres.idTeam != 'undefined' && filtres.idTeam == 'true' && filtres.modaliteC != 'undefined' && filtres.modaliteC == 'true')
		{
			filtre.idTeam = true
		}

		/*if (filtres.datDeb != 'undefined' && filtres.datDeb != '') {
			if (filtres.datFin != 'undefined' && filtres.datFin != '') {
				filtre.date_yyyymmdd = { $gte: filtres.datDeb, $lte: filtres.datFin };
			} else {
				filtre.date_yyyymmdd = { $gte: filtres.datDeb };
			}
		} else if (filtres.datFin != 'undefined' && filtres.datFin != '') {
			filtre.date_yyyymmdd = { $lte: filtres.datFin };
		}*/

		return filtre;
	}

	this.listParticipations = function(filtre, ordre, logger, userId, callback) {
		logger.info("listParticipations(" + JSON.stringify(filtre)+ ", " +
			JSON.stringify(ordre) + ") - " + userId);
		var Video = require(__dirname + '/../model/Video.js');
		var Utils = require(__dirname + '/../model/Utils.js');
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			var participations = [];

			//to made the $match request
			var request = [];

			if (filtre ['ageMin'] != null)
			{
				request.push({'participants.age' : filtre['ageMin']})
			}
			if (filtre ['ageMax'] != null)
			{
				request.push({'participants.age' : filtre['ageMax']})
			}
			if (filtre ['ageCategory'] != null)
			{
				request.push({'participants.ageCategory' : filtre['ageCategory']})
			}
			if (filtre ['gender'] != null)
			{
				request.push({'participants.gender' : filtre['gender']})
			}
			if (filtre ['nbParticipants'] != null)
			{
				request.push( {'nbParticipants' : filtre['nbParticipants']})
			}
			if (filtre ['idContext'] != null)
			{
				request.push({'idContext' : filtre['idContext']})
			}
			if (filtre['idTeam'] === true && filtre['nbParticipants'] !=  { $eq: '1' } )
			{
				var sort = {"idTeam" : 1}
			} else {
				var sort = {"participants.idParticipant" : 1}
			}

			if(Object.keys(filtre).length !== 0 )
			{
				var trueColl = coll.aggregate([
					{ $unwind: "$participants"},
					{$match: {$and : request}},
					{$sort : sort}
				])
			} else
			{
				var trueColl = coll.aggregate([
					{ $unwind: "$participants"},
					{$sort: { "idVideo" : 1 }}
				])
			}
			trueColl.forEach(function (v) {
				date = "";
				if (v.date_aaaammjj !== undefined &&
					v.date_aaaammjj != "" &&
					v.date_aaaammjj.length == 8) {
					date = v.date_aaaammjj.substr(6, 2) + "-" +
						v.date_aaaammjj.substr(4, 2) + "-" +
						v.date_aaaammjj.substr(0, 4);
				}
				v.dateJMA = date;
				participations.push(v);
			})
				.then(function() {
					callback(participations);
				});
		});
	};

	this.selectParticipationById = function(id, logger, userId, callback) {
		logger.info("selectParticipationById(" + id + ") - " + userId);
		var Video = require(__dirname + '/../model/Video.js');
		var participation = {};
		if (id == '') {
			// TODO possible de faire mieux pour la création d'un objet vide ?
			callback(participation);
		} else {
			var collection = this.dbProperties.videosCollection;
			mongo.connect (this.dbProperties.url, function (error, client) {
				if (error) throw error ;
				const db = client.db(dbName);
				const coll = db.collection(collection);
				coll.aggregate( [
					{ $unwind: "$participants" },
					{ $match: { "participants.idParticipant": id } },
				] )
					.forEach(function (v) {
						participation = v;
//						participation.dateJMA = utils.getDateJMA(participation.date_aaaammjj);
						date = "";
						if (participation.date_aaaammjj !== undefined &&
							participation.date_aaaammjj != "" &&
							participation.date_aaaammjj.length == 8) {
							date = participation.date_aaaammjj.substr(6, 2) + "-" +
								participation.date_aaaammjj.substr(4, 2) + "-" +
								participation.date_aaaammjj.substr(0, 4);
						}
						participation.dateJMA = date;
					})
					.then(function() {
						callback(participation);
					});
			});
		}
	};


	this.findTeam = function (idVideo, idTeam, idParticipant, logger, userId, callback){
		logger.info("find Binomial of(" + idParticipant + ") - " + userId);
		var Video = require(__dirname + '/../model/Video.js');
		var participationTeam = [];
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error;
			const db = client.db(dbName);
			const coll = db.collection(collection);

			coll.aggregate([
				{$unwind: "$participants"},
				{ $match: {$and : [{"idVideo" : idVideo}, {"idTeam" : idTeam}, {"participants.idParticipant": { $ne : idParticipant}}]}}]
			)
				.forEach(function (v) {
					date = "";
					if (v.date_aaaammjj !== undefined &&
						v.date_aaaammjj != "" &&
						v.date_aaaammjj.length == 8) {
						date = v.date_aaaammjj.substr(6, 2) + "-" +
							v.date_aaaammjj.substr(4, 2) + "-" +
							v.date_aaaammjj.substr(0, 4);
					}
					v.dateJMA = date;
					participationTeam.push(v);
				})
				.then(function() {
					callback(participationTeam);
				});

		})

	}


	this.saveClicks = function(idVideo, idParticipant, clicks, logger, userId, callback) {
		logger.info("saveClicks(" + idVideo + ", " + idParticipant + " - " +
			JSON.stringify(clicks) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.updateOne(
				{ "idVideo": idVideo, "participants.idParticipant": idParticipant },
				{ $set: { "participants.$.clicks": clicks } }
			).then(function(wr) {
				logger.info("saveClicks(" + idVideo + " - " + idParticipant + " - " +
					JSON.stringify(clicks) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(clicks);
			}).catch(function(error) {
				logger.error("saveClicks(" + idParticipant + " - " +
					JSON.stringify(clicks) + ") => Erreur - " +
					userId + "\n" + error);
			});
		});
	};

	this.saveParticipation = function(idVideo, participation, logger, userId, callback) {
		logger.info("saveParticipation(" + JSON.stringify(participation) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.updateOne(
				{
					"idVideo": idVideo,
					"participants.idParticipant": participation.idParticipant
				},
				{ $set: { "participants.$": participation } }
			).then(function(wr) {
				logger.info("saveParticipation(" + JSON.stringify(participation) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(wr.result);
			}).catch(function(error) {
				logger.error("saveParticipation(" + JSON.stringify(participation) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.checkAvailableId = function(typeId, id, logger, userId, callback) {
		logger.info("checkAvailableId(" + typeId + ", " + id + ") - " + userId);
		var Video = require(__dirname + '/../model/Video.js');
		var collection = this.dbProperties.videosCollection;
		mongo.connect (url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			let dispo = true;
			let filtre = {};
			switch(typeId) {
				case "v":
					filtre = { "idVideo": id };
					break;
				case "bin":
					filtre = { "idTeam": id };
					break;
				case "tri":
					filtre = { "idTeam": id };
					break;
				case "p":
					filtre = { "participants.idParticipant": id };
					break;
			}
			coll.find(filtre).count( { limit: 1 } ).then(function(c) {
				if (c > 0) {
					dispo = false;
				}
				callback(dispo);
			});
		});
	}

	this.addVideo = function(idVideo, video, logger, userId, callback) {
		logger.info("add(" + JSON.stringify(video) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.insertOne(video).then(function(wr) {
				logger.info("addVideo(" + JSON.stringify(video) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				// TODO renvoyer videon pour réaffichage
				callback(wr.result);
			}).catch(function(error) {
				logger.error("addVideo(" + JSON.stringify(video) +
					") => Error - " + userId + "\n" + error);
			});
		});
	};

	this.saveLastId = function(LastId,logger, userId, callback) {
		logger.info("saveLastId(" + JSON.stringify(LastId) + ") - " + userId);
		var collection = this.dbProperties.lastIdCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.updateOne(
				{ "nameId": LastId.nameId },
				{ $set: {"id": LastId.id} }
			).then(function(wr) {
				logger.info("saveLastId(" + JSON.stringify(LastId) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(wr.result);
			}).catch(function(error) {
				logger.error("saveLastId(" + JSON.stringify(LastId) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.saveOK = function(idVideo, OKType, newValue,logger, userId) {
		logger.info("saveOK(" + JSON.stringify(idVideo) + JSON.stringify(OKType) + JSON.stringify(newValue) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			let request = {}
			switch (OKType) {
				case "videoOK": request = {"videoOK": (newValue === "true" )}
					break;
				case "activity1": request = {"activity1": (newValue === "true" )}
					break;
				case "activity2": request = {"activity2": (newValue === "true" )}
					break;
				case "activity3": request = {"activity3": (newValue === "true" )}
					break;
			}
			coll.updateOne(
				{ "idVideo": idVideo },
				{ $set: request }
			).then(function(wr) {
				logger.info("saveOK(" + JSON.stringify(OKType) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
			}).catch(function(error) {
				logger.error("saveOK(" + JSON.stringify(idVideo) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};


	this.getLastIdVideo = function(idVideo,logger, userId, callback) {
		logger.info("getLastId(" + JSON.stringify(idVideo) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			let lastId = ""
			coll.find()
				.sort({"idVideo":-1})
				.limit(1)
				.forEach(function (video){
					lastId = video.idVideo
				})
				.then(function() {
					callback(lastId);
				}).catch(function(error) {
				logger.error("getLastId(" + JSON.stringify(idVideo) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.getLastIdTeam = function(nbParticipants,logger, userId, callback) {
		logger.info("getLastIdTeam(" + JSON.stringify(nbParticipants) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			let lastId = ""
			coll.find({"nbParticipants": nbParticipants.toString()})
				.sort({"idTeam":-1})
				.limit(1)
				.forEach(function (video){
					lastId = video.idTeam
				})
				.then(function() {
					callback(lastId);
				}).catch(function(error) {
				logger.error("getLastIdTeam(" + JSON.stringify(nbParticipants) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.getLastIdParticipant = function(idParticipants,logger, userId, callback) {
		logger.info("getLastId(" + JSON.stringify(idParticipants) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			let lastId = ""
			coll.aggregate([
				{ $unwind: "$participants"},
				{$sort: { "participants.idParticipant" : -1 }}
				])
				.limit(1)
				.forEach(function (video){
					lastId = video.participants.idParticipant
				})
				.then(function() {
					callback(lastId);
				}).catch(function(error) {
				logger.error("getLastId(" + JSON.stringify(idParticipants) +
					") => Erreur - " + userId + "\n" + error);
			});
		});
	};

	this.deleteVideo = function(idVideo, logger, userId, callback)
	{
		logger.info("deleteVideo(" + JSON.stringify(idVideo) + ") - " + userId);
		var collection = this.dbProperties.videosCollection;
		mongo.connect (this.dbProperties.url, function (error, client) {
			if (error) throw error ;
			const db = client.db(dbName);
			const coll = db.collection(collection);
			coll.deleteOne(
				{ "idVideo": idVideo }
			).then(function(wr) {
				logger.info("deleteVideo(" + JSON.stringify(idVideo) + ") => " +
					JSON.stringify(wr.result) + " - " + userId);
				callback(wr.result);
			}).catch(function(error) {

			});
		});
	}


};

module.exports = MongoDAO;