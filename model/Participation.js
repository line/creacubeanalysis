const ageCategory = {
	enf: "Enfant",
	ado: "Adolescent",
	jad: "Jeune adulte",
	adu: "Adulte",
	sen: "Senior"
};

const gender = {
	"F": "Female",
	"M": "Male"
};

const profileTech = {
	"0": "noKnow",
	"1": "notComfort",
	"2": "normal",
	"3": "webUser",
	"4": "techy",
	"5": "techExpert",
};

var metadata = {
	"FNN": [ "F01", "F02", "F03", "F04", "F05", "F06", "F07", "F08", "F09",
			 "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18" ],
	"FNNinnov":
		[ "F05", "F08", "F09", "F10", "F12", "F14", "F15", "F16", "F17", "F18" ],
	"AS0X": [ "AS00", "AS01", "AS02", "AS03" ],
	"PXX": [ "P01", "P02", "P03", "P04", "P05", "P06", "P07", "P08", "P09", "P10", "P11" ],
	"calculs":
		[ 
			{
				"idCalcul": "AF01-Wheels",
				"typeCalcul": "global",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "AF01" ]
			},
			{
				"idCalcul": "AF02-Magnets",
				"typeCalcul": "global",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "AF02" ]
			},
			{
				"idCalcul": "AF03-ButtonOnOff",
				"typeCalcul": "global",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "AF03" ]
			},
			{
				"idCalcul": "AF04-2eyes",
				"typeCalcul": "global",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "AF04" ]
			},
			{
				"idCalcul": "nbInstructionsPlayed",  
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "U00" ]
			},
			{
				"idCalcul": "listTPlayInstr-U00",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesTimes",
				"elemsCalcul": [],
				"clicksCalcul": [ "U00" ]
			},
			{
				"idCalcul": "listTStopInstr-U01",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesTimes",
				"elemsCalcul": [],
				"clicksCalcul": [ "U01" ]
			},
			{
				"idCalcul": "firstIntructionsEnd",
				"typeCalcul": "activite",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "U01" ]
			},
			{
				"idCalcul": "timeBeforeFirstManip",
				"typeCalcul": "activite",
				"methodeCalcul": "min",
				"elemsCalcul": [ "firstInstructionsEnd", "firstManip" ],
				"clicksCalcul": [],
			},
			{
				"idCalcul": "firstManip",
				"typeCalcul": "activite",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "U04", "U05", "U06", "U07" ]
			},
			{
				"idCalcul": "firstTest",
				"typeCalcul": "activite",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": "FNN"
			},
			{
				"idCalcul": "timeSucceed",
				"typeCalcul": "activite",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "T03" ]
			},
			{
				"idCalcul": "timeDropout",
				"typeCalcul": "activite",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "T02" ]
			},
			{
				"idCalcul": "noTest",
				"typeCalcul": "activite",
				"methodeCalcul": "firstOccurenceTime",
				"elemsCalcul": [],
				"clicksCalcul": [ "T01" ]
			},
			{
				"idCalcul": "activityDuration",
				"typeCalcul": "activite",
				"methodeCalcul": "timeDifference",
				"elemsCalcul": [ "timeSucceed", "firstInstructionsEnd" ],
				"clicksCalcul": [],
			},
			{
				"idCalcul": "listAssembling",
				"typeCalcul": "activite",
				"methodeCalcul": "clickOccurences",
				"elemsCalcul": [],
				"clicksCalcul": "AS0X"
			},
			{
				"idCalcul": "nbAssembling",
				"typeCalcul": "activite",
				"methodeCalcul": "clickAscendingTransitionsCount",
				"elemsCalcul": [ "listAssembling" ],
				"clicksCalcul": "AS0X"
			},
			{
				"idCalcul": "nbDissambling",
				"typeCalcul": "activite",
				"methodeCalcul": "clickDescendingTransitionsCount",
				"elemsCalcul": [ "listAssembling" ],
				"clicksCalcul": "AS0X"
			},
			{
				"idCalcul": "listPivoting",
				"typeCalcul": "activite",
				"methodeCalcul": "clickOccurences",
				"elemsCalcul": [],
				"clicksCalcul": [ "FL01" ]
			},
			{
				"idCalcul": "nbPivoting",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "FL01" ]
			},
			{
				"idCalcul": "listRePosition",
				"typeCalcul": "activite",
				"methodeCalcul": "clickOccurences",
				"elemsCalcul": [],
				"clicksCalcul": [ "FL02" ]
			},
			{
				"idCalcul": "nbRePosition",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "FL02" ]
			},
			{
				"idCalcul": "mouvIntra",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "FL01", "FL02" ]
			},
			{
				"idCalcul": "listFigures",
				"typeCalcul": "activite",
				"methodeCalcul": "clickOccurences",
				"elemsCalcul": [],
				"clicksCalcul": "FNN"
			},
			{
				"idCalcul": "nbTotalFigures",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": "FNN"
			},
			{
				"idCalcul": "nbDiffFigures",
				"typeCalcul": "activite",
				"methodeCalcul": "clickDiffOccurences",
				"elemsCalcul": [],
				"clicksCalcul": "FNN"
			},
			{
				"idCalcul": "nbInnovFigures",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": "FNNinnov"
			},
			{
				"idCalcul": "nbF01",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F01" ]
			},
			{
				"idCalcul": "nbF02",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F02" ]
			},
			{
				"idCalcul": "nbF03",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F03" ]
			},
			{
				"idCalcul": "nbF04",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F04" ]
			},
			{
				"idCalcul": "nbF05",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F05" ]
			},
			{
				"idCalcul": "nbF06",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F06" ]
			},
			{
				"idCalcul": "nbF07",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F07" ]
			},
			{
				"idCalcul": "nbF08",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F08" ]
			},
			{
				"idCalcul": "nbF09",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F09" ]
			},
			{
				"idCalcul": "nbF10",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F10" ]
			},
			{
				"idCalcul": "nbF11",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F11" ]
			},
			{
				"idCalcul": "nbF12",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F12" ]
			},
			{
				"idCalcul": "nbF13",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F13" ]
			},
			{
				"idCalcul": "nbF14",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F14" ]
			},
			{
				"idCalcul": "nbF15",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F15" ]
			},
			{
				"idCalcul": "nbF16",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F16" ]
			},
			{
				"idCalcul": "nbF17",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F17" ]
			},
			{
				"idCalcul": "nbF18",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "F18" ]
			},
			{
				"idCalcul": "nbFXX",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "FXX" ]
			},
/*
			{
				"idCalcul": "DescriptionFXX",
				"typeCalcul": "activite",
				"methodeCalcul": "",
				"elemsCalcul": [],
				"clicksCalcul": ""
			},
*/
			{
				"idCalcul": "listProblems",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": "PXX"
			},
			{
				"idCalcul": "P01-unbalance",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P01" ]
			},
			{
				"idCalcul": "P02-rotation",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P02" ]
			},
			{
				"idCalcul": "P03-reverse-left2right",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P03" ]
			},
			{
				"idCalcul": "P04-reverse-outward",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P04" ]
			},
			{
				"idCalcul": "P05-reverse-toParticipant",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P05" ]
			},
			{
				"idCalcul": "P06-color-association",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P06" ]
			},
			{
				"idCalcul": "P07-connection",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P07" ]
			},
			{
				"idCalcul": "P08doesn'tMove(wheels)",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P08" ]
			},
			{
				"idCalcul": "P09doesn'tMove(on/off)",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P09" ]
			},
			{
				"idCalcul": "P10doesn'tMove(capteur-involontaire)",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P10" ]
			},
			{
				"idCalcul": "P11doesn'tMove(inverseur)",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "P11" ]
			},
			{
				"idCalcul": "B01-trial-error",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B01" ]
			},
			{
				"idCalcul": "B02-analytical-systemic",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B02" ]
			},
			{
				"idCalcul": "B03-hypothesizing",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B03" ]
			},
			{
				"idCalcul": "B04-ego-preservation",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B04" ]
			},
			{
				"idCalcul": "B05-complaining",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B05" ]
			},
			{
				"idCalcul": "nbB01-trial-error",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B01" ]
			},
			{
				"idCalcul": "nbB02-analytical-systemic",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B02" ]
			},
			{
				"idCalcul": "nbB03-hypothesizing",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B03" ]
			},
			{
				"idCalcul": "nbB04-ego-preservation",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B04" ]
			},
			{
				"idCalcul": "nbB05-complaining",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "B05" ]
			},
			{
				"idCalcul": "E01-Ecstasy-Joy-Serenity",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E01" ]
			},
			{
				"idCalcul": "E02-Admiration-Trust-Acceptance",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E02" ]
			},
			{
				"idCalcul": "E03-Error-Fear-Apprehension",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E03" ]
			},
			{
				"idCalcul": "E04-Amazement-Surprise-Distraction",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E04" ]
			},
			{
				"idCalcul": "E05-Grief-Sadness-Pensiveness",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E05" ]
			},
			{
				"idCalcul": "E06-Loathing-Disgust-Boredom",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E06" ]
			},
			{
				"idCalcul": "E07-Rage-Anger-Annoyance",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E07" ]
			},
			{
				"idCalcul": "E08-Vigilance-Anticipation-Interest",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E08" ]
			},
			{
				"idCalcul": "nbE01-Ecstasy-Joy-Serenity",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E01" ]
			},
			{
				"idCalcul": "nbE02-Admiration-Trust-Acceptance",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E02" ]
			},
			{
				"idCalcul": "nbE03-Error-Fear-Apprehension",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E03" ]
			},
			{
				"idCalcul": "nbE04-Amazement-Surprise-Distraction",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E04" ]
			},
			{
				"idCalcul": "nbE05-Grief-Sadness-Pensiveness",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E05" ]
			},
			{
				"idCalcul": "nbE06-Loathing-Disgust-Boredom",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E06" ]
			},
			{
				"idCalcul": "nbE07-Rage-Anger-Annoyance",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E07" ]
			},
			{
				"idCalcul": "nbE08-Vigilance-Anticipation-Interest",
				"typeCalcul": "activite",
				"methodeCalcul": "occurencesCount",
				"elemsCalcul": [],
				"clicksCalcul": [ "E08" ]
			}
		]
};

/*
Propriétés: {
	clicks: "array"
	dataFilename: "string"
	idParticipant: "string"
	nameParticipant: "string"
	idContext: "string"
	idTeam: "String"
	idTeamHistory: "String"
	idTeamCloserness: "String"
	age: "int"
	ageCategory: enum: ["enf", "ado", "jad", "adu", "sen"]
	date_yyyymmdd: "date"
	nbParticipants: "int"
	gender: enum: ["M", "F"]
	profession: "string"
	profileTech: 
		enum: ["0", // noKnow
			"1", // notComfort
			"2", // normal
			"3", // webUser
			"4", // techy
			"5"] // techExpert
	diversity: "string"
	leftHanded: "bool"
	cubeletsPriorKnowledge: "bool"
	videoUrl: "string"
	videoName: "string"
	participationOK: "bool"
	commentaire: "string"
required: ["idParticipant",
		"age",
		"date_yyyymmdd",
		"nbParticipants",
		"gender",
		"clicks"],
*/
var Participation = function(
		clicks,
		dataFilename, // TODO supprimer dans la version définitive
		idParticipant,
		nameParticipant,
		age,
		ageCategory, // redondant mais facilite les recherches
		gender,
		profession,
		profileTech,
		diversity,
		leftHanded,
		cubeletsPriorKnowledge,
		participationOK,
		commentaire
		) {
	this.clicks = clicks;
	this.dataFilename = dataFilename;
	this.idParticipant = idParticipant;
	this.nameParticipant = nameParticipant;
	this.age = age;
	this.ageCategory = ageCategory;
	this.gender = gender;
	this.profession = profession;
	this.profileTech = profileTech;
	this.diversity = diversity;
	this.leftHanded = leftHanded;
	this.cubeletsPriorKnowledge = cubeletsPriorKnowledge;
	this.participationOK = participationOK;
	this.commentaire = commentaire;
};

Participation.prototype.correct = function(p) {
	if (typeof p != Object) return false;
	if (p.idParticipant === undefined) return false;
	if (p.age === undefined) return false;
	// agecategory correct
	if (p.date_yyyymmdd === undefined) return false;
	// date bien formee
	if (p.nbParticipants === undefined) return false;
	// nbparticipants = 1, 2 ou 3
	if (p.gender === undefined) return false;
	// gender correct
	// profiletech correct
	if (p.clicks === undefined) return false;
	if (typeof p.clicks != Array) return false;
	return true;
};

Participation.prototype.getLibCategorie = function() {
	return ageCategory[this.ageCategory];
};

Participation.prototype.getLibGender = function() {
	return gender[this.gender];
};

Participation.prototype.getDonneesIndividuelles = function() {
	var results = getResults(this);
	var json = this;
	for (i = 0; i < fields.length; i++) {
		fieldName = [fields][i];
		json[fieldName] = results[fieldName];
	}
	return json;
};

Participation.prototype.getFields = function() {
	/** Defines which fields are displayed in the ../creacube/list.html table. */
	var fields = [];
	for (c in metadata.calculs) {
		if (metadata.calculs[c].typeCalcul == "global") {
			fields.push(metadata.calculs[c].idCalcul);
		}
	}
	for (c in metadata.calculs) {
		if (metadata.calculs[c].typeCalcul == "activite") {
			fields.push("A1-" + metadata.calculs[c].idCalcul);
		}
	}
	for (c in metadata.calculs) {
		if (metadata.calculs[c].typeCalcul == "activite") {
			fields.push("A2-" + metadata.calculs[c].idCalcul);
		}
	}
	return fields;
};

Participation.prototype.toString = function() {
	return JSON.stringify(this, null, 2);
};

/**
 * Defines the data result analysis functionnalities.
 * 
 * @class
 */
Participation.prototype.getResults = function() {
	/**
	 * Implements the data calculated fields processing. - Implements the
	 * specifications given in http://tinyurl.com/y4kvqymy - Times is given in
	 * seconds, with respect to the file creation time.
	*/

	var results = {};

	var clicks = applatissement(this.clicks);

	results._warning = [];

	// Calculates first and last times
	results._first_time = 999999;
	results._last_time = 0;
	clicks_count = clicks.length;
	if(results._clicks_count == 0) {
		results._warning.push("NO click");
	} else {
		for(t in clicks) {
			if (+clicks[t].time < results._first_time) {
				results._first_time = clicks[t].time;
			}
			if (+clicks[t].time > results._last_time) {
				results._last_time = clicks[t].time
			}
		}

		var when = getWhen(clicks);

		// Adds warning if something strange happens
		if (when.length == 0) {
			results._warning.push("START never pressed");
			return results;
		}
		if (when.length == 1) {
			results._warning.push("START pressed only one  time");
		}
		if (when.length > 2) {
			results._warning.push("START pressed more than 2 times");
		}
		if (when[0] != results._first_time) {
			results._warning.push("START pressed after 1st click");
		}

		when[when.length] = results._last_time;
	
		results.when = when;
	}

	for (c in metadata.calculs) {
		metadata.calculs[c].globalCalcule = false;
		metadata.calculs[c].activitesCalculees = false;
	}

	var calculTermine = false;
	while (!calculTermine) {
		calculTermine = true;
		for (c in metadata.calculs) {
			var calcul = metadata.calculs[c];
			if (calcul.typeCalcul == "global" && calcul.globalCalcule) {
				continue;
			}
			if (calcul.typeCalcul == "activite" && calcul.activitesCalculees) {
				continue;
			}

			var startStop = [];
			if (metadata.calculs[c].typeCalcul == "global") {
				startStop.push([ when[0], when[when.length - 1] ]);
			}
			if (metadata.calculs[c].typeCalcul == "activite") {
				startStop.push([ when[0], when[1] ]);
				if (when.length > 2) {
					startStop.push([ when[1], when[2] ]);
				}
			}

			var ids = [];
			if (calcul.typeCalcul == "global") {
				ids.push(calcul.idCalcul);
			}
			if (calcul.typeCalcul == "activite") {
				ids.push("A1-" + calcul.idCalcul);
				if (when.length > 2) {
					ids.push("A2-" + calcul.idCalcul);
				}
			}

			var clicksRecherches = calcul.clicksCalcul;
			if (typeof clicksRecherches === "string") {
				clicksRecherches = metadata[clicksRecherches];
				if (clicksRecherches === 'undefined') {
					console.error("metadonnees manquantes pour " + calcul.clicksCalcul);
					clicksRecherches = [];
				}
			}

			var elems = calcul.elemsCalcul;
			if (elems !== 'undefined' && elems.length > 0) {
				if (!elemsCalcules(elems)) {
					continue;
				}
			}

			var resultat = [];
			var calculable = true;
			for (i = 0; i < ids.length; i++) {
				switch (calcul.methodeCalcul) {
					case "clickAscendingTransitionsCount":
						if (elems.length == 1) {
							resultat.push(
									getClickTransitionCount(
										elems, clicksRecherches, startStop[i][0], startStop[i][1],
										"ascending"));
						} else {
							calculable = false;
						}
						break;
					case "clickDescendingTransitionsCount":
						if (elems.length == 1) {
							resultat.push(
									getClickTransitionCount(
										elems, clicksRecherches, startStop[i][0], startStop[i][1],
										"descending"));
						} else {
							calculable = false;
						}
						break;
					case "clickDiffOccurences":
						resultat.push(
							getClickDiffOccurences(
								clicks, clicksRecherches, startStop[i][0], startStop[i][1]));
						break;
					case "clickOccurences":
						resultat.push(
							getClickOccurences(
								clicks, clicksRecherches, startStop[i][0], startStop[i][1]));
						break;
					case "firstOccurenceTime":
						resultat.push(
							getFirstOccurenceTime(
								clicks, clicksRecherches, startStop[i][0], startStop[i][1]));
						break;
					case "min":
						if (elemsCalcules(elems)) {
							var min = elems[0];
							for (k = 1; k < elems.length; k++) {
								min = Math.min(min, e[k]);
							}
							resultat.push(min);
						} else {
							calculable = false;
						}
						break;
					case "occurencesCount":
						resultat.push(
							getOccurenceCount(
								clicks, clicksRecherches, startStop[i][0], startStop[i][1]));
						break;
					case "occurencesTimes":
						resultat.push(
							getOccurenceTimes(
								clicks, clicksRecherches, startStop[i][0], startStop[i][1]));
						break;
					case "timeDifference":
						if (elems.length == 2) {
							resultat.push(getTimeDifference(elems[0], elems[1]));
						} else {
							calculable = false;
						}
						break;
					default:
						console.error("methode de calcul non gérée : " + c.methodeCalcul);
						break;
				}
			}

			if (calculable) {
				if (calcul.typeCalcul == "global" && resultat.length == 1) {
					results[calcul.idCalcul] = resultat[0];
					calcul.globalCalcule = true;
				}
				if (calcul.typeCalcul == "activite" && resultat.length >= 1) {
					results["A1-" + calcul.idCalcul] = resultat[0];
					if (resultat.length >= 2) {
						results["A2-" + calcul.idCalcul] = resultat[1];
					}
					calcul.activitesCalculees = true;
				}
			} else {
				calculTermine = false;
			}
		}
	}

	return results;
};

var applatissement = function(clicks) {
	var clicksApplatis = [];
	for (c in clicks) {
		for (t in clicks[c].tclicks) {
			clicksApplatis.push( {
				"time": clicks[c].time,
				"click": clicks[c].tclicks[t]
			} );
		}

	}
	return clicksApplatis;
}

// Defines the activity 1st and 2nd periods when = [ 1st-start-time,
// 2nd-start-time, end-time ];
var getWhen = function(clicks) {
	var when = [], start = 0;
	for(var t in clicks) {
		if (clicks[t].click == "START") {
			when[start++] = clicks[t].time;
		}
	}
	return when;
};

// Vérifie que les éléments entrant dans un calcul ont été calculés
var elemsCalcules = function(elems) {
	var calcules = true;
	for (e in elems) {
		var elemCalcule = false;
		for (c in metadata.calculs) {
			if (metadata.calculs[c].idCalcul == e) {
				if (metadata.calculs[c].typeCalcul == "global" &&
						metadata.calculs[c].globalCalcule) {
					elemCalcule = true;
					break;
				}
				if (metadata.calculs[c].typeCalcul == "activite" &&
						metadata.calculs[c].activitesCalculees) {
					elemCalcule = true;
					break;
				}
			}
		}
		if (!elemCalcule) {
			calcules = false;
			break;
		}
	}
	return calcules;
}

/** Returns the list of different click occurence, between two times. */
var getClickDiffOccurences  = function(clicks, clicksRecherches, start, stop) {
	var values = [];
	for(var t in clicks) {
		if (start <= +clicks[t].time &&
				+clicks[t].time <= stop &&
				clicksRecherches.includes(clicks[t].click) &&
				!values.includes(clicks[t].click)) {
			values.push(clicks[t].click);
		}
	}
	return values;
};

/** Returns the list of click occurence, between two times. */
var getClickOccurences  = function(clicks, clicksRecherches, start, stop) {
	var values = [];
	for(var t in clicks) {
		if (start <= +clicks[t].time &&
				+clicks[t].time <= stop &&
				clicksRecherches.includes(clicks[t].click)) {
			values.push(clicks[t].click);
		}
	}
	return values;
};

/**
 * Counts the number of transitions in a click list, between two times,
 * couting ascending or descending.
 */
var getClickTransitionCount = function(list, clicksRecherches, start, stop, mode) {
	var count = 0;								   
/*
	for(var i = 1; i < list.length; i++) {
		var transition = clicks.indexOf(list[i]) - clicks.indexOf(list[i-1]);
		if ((mode == "ascending" && transition > 0) ||
				(mode == "descending" && transition < 0)) {
			count++;
		}
	}
*/
	return count;
};

/**
 * Detects the 1st occurence of clicks, between two times, returns the time
 * in second.
 */
var getFirstOccurenceTime = function(clicks, clicksRecherches, start, stop) {
	var times = getOccurenceTimes(clicks, clicksRecherches, start, stop);
	return times.length > 0 ? times[0] : "never";
};

/** Counts the number of occurence of clicks, between two times. */
var getOccurenceCount = function(clicks, clicksRecherches, start, stop) {
	var count = 0;
	for(var t in clicks) {
		if (start <= +clicks[t].time &&
				+clicks[t].time <= stop &&
				clicksRecherches.includes(clicks[t].click)) {
			count++;
		}
	}
	return count;
};

/**
 * Detects the occurences of clicks, between two times, returns the time in
 * second.
 */
var getOccurenceTimes = function(clicks, clicksRecherches, start, stop) {
	var times = [];
	for(var t in clicks) {
		if (start <= +clicks[t].time &&
				+clicks[t].time <= stop &&
				clicksRecherches.includes(clicks[t].click)) {
			times.push(Math.round(+clicks[t].time - start));
		}
	}
	return times;
};

/** Calculates a duration between two times. */
var getTimeDifference = function(startTime, stopTime) {
	return (startTime instanceof Number && stopTime instanceof Number) 
			? parseInt(stopTime) - parseInt(startTime) 
			: "unknown";
};

module.exports = Participation;

