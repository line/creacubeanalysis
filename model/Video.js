/*
Propriétés: {
	idVideo: "string"
	nbParticipants: "int"
	idTeam: "string"
	idTeamHistory: "string"
	idTeamCloserness: "string"
	idContext: "string"
	date_yyyymmdd: "date"
	videoName: "string"
	videoOK: "bool"
	commentaire: "string"
	participants: "array"
required: ["idVideo",
		"nbParticipants",
		"videoName"],
*/
var Video = function(
		idVideo,
		nbParticipants,
		idTeam,
		idTeamHistory,
		idTeamCloserness,
		idContext,
		date_yyyymmdd,
		videoUrl, // TODO supprimer dans la version définitive
		videoName,
		videoOK,
		activity1,
		activity2,
		activity3,
		commentaire,
		participants
		) {
	this.idVideo = idVideo;
	this.nbParticipants = nbParticipants;
	this.idTeam = idTeam;
	this.idTeamHistory = idTeamHistory;
	this.idTeamCloserness = idTeamCloserness;
	this.idContext = idContext;
	this.date_yyyymmdd = date_yyyymmdd;
	this.videoUrl = videoUrl;
	this.videoName = videoName;
	this.videoOK = videoOK;
	this.activity1 = activity1;
	this.activity2 = activity2;
	this.activity3 = activity3;
	this.commentaire = commentaire;
	this.participants = participants;
};

Video.prototype.correct = function(v) {
	if (typeof v != Object) return false;
	if (v.date_yyyymmdd === undefined) return false;
	// TODO date bien formee
	if (v.nbParticipants === undefined) return false;
	// TODO nbparticipants = 1, 2 ou 3
	return true;
};

Video.prototype.getInfos = function() {
	var infos = [];
	for (var i in this) {
		if (i != "participants") {
			infos[i] = this[i];
		}
	}
	return infos;
};

Video.prototype.getDateJMA = function() {
	date = "";
	if (this.date_yyyymmdd !== undefined && this.date_yyyymmdd != "") {
		date = this.date_yyyymmdd.substr(6, 2) + "-" + 
			this.date_yyyymmdd.substr(4, 2) + "-" + 
			this.date_yyyymmdd.substr(0, 4);
	}
	return date;
};

Video.prototype.toString = function() {
	return JSON.stringify(this, null, 2);
};

module.exports = Video;

