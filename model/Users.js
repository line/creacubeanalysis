var User= function(
    userId,
    name,
    firstName,
    admin
) {
    this.userId = userId;
    this.name = name;
    this.firstName = firstName;
    this.admin = admin;
};

User.prototype.toString = function() {
    return JSON.stringify(this, null, 2);
};

module.exports = User;