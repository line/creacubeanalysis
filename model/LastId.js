var LastId= function(
    nameId,
    id
) {
    this.nameId = nameId;
    this.id = id;
};

LastId.prototype.toString = function() {
    return JSON.stringify(this, null, 2);
};

module.exports = LastId;