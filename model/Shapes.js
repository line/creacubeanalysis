var Shape= function(
    family,
    combinations
) {
    this.family = family;
    this.combinations = combinations;
};

Shape.prototype.toString = function() {
    return JSON.stringify(this, null, 2);
};

module.exports = Shape;