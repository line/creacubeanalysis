

## CreaMaker V2

New data model with shape's combinations

New observables table with new shapes

Personalized feedback in PDF generator

New fields for a Video : activity 1 and activity 2

Fluidity, flexibility, originality and collaboration individual in global and per activity calculated in a chart

Customizable graphic (axe's size, displayable fields, displayable idParticipant)

Median, first quartile, third quartile, min value and max vale of fluidity, flexibility and originality calculated 
for a group in a box plot chart.

Users management (add, delete)

Search bar for idContext

Add LastId (updatable)

Display last ids (idVideo, idTeam and idParticipant) in the DB

Filter participant works and add new filters

Debug : addVideo and archive

## Develop mode

/app/index.js in the initialization of the variable cas :

is_dev_mode: true

/model/properties.js see the comments

## Deployment mode

/app/index.js in the initialization of the variable cas :

is_dev_mode: false

/model/properties.js see the comments


## Technologies

Google Charts

jspdf

Select2


## New versions:

2.1 : 

add new combinations without img 

2.2 :

debug users management

2.3 : 

draggable observable table

excel creation with fluidity, flexibility, innovation, time in global and per activity. Also with the list of the
original shapes and median, average, min, max and quartiles for the group

converter for old coding shape


2.4:

a new start in the observables table with start activity 1 and start activity 2 (already considered in the entire code normally)

a new filter with age min and age max

convert string in int for the participant's age in the db

2.5:

new filters to export the excel

the possibility to put only the shape without the combination and with possibility to put "true" or "false"

min and max age filter for the participant